/*Table structure for table `boutique` */

CREATE TABLE `boutique` (
  `id` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `nom` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `cat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `comment` */

CREATE TABLE `comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `newsid` int(11) unsigned NOT NULL,
  `publisher` int(11) unsigned NOT NULL,
  `content` text NOT NULL,
  `timestamp` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `logboutique` */

CREATE TABLE `logboutique` (
  `id` int(11) unsigned NOT NULL,
  `message` text NOT NULL,
  `iditem` int(255) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `logvote` */

CREATE TABLE `logvote` (
  `id` int(11) unsigned NOT NULL,
  `message` text NOT NULL,
  `site` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `news` */

CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `publisher` int(11) unsigned NOT NULL,
  `timestamp` int(255) NOT NULL,
  `publish` int(2) NOT NULL,
  `title` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `points` */

CREATE TABLE `points` (
  `id` int(11) unsigned NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `privatemsg` */

CREATE TABLE `privatemsg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `objet` varchar(255) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `lus` int(1) NOT NULL,
  `timestamp` bigint(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `privatemsgdial` */

CREATE TABLE `privatemsgdial` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idobjet` int(11) unsigned NOT NULL,
  `content` text NOT NULL,
  `sender` int(11) unsigned NOT NULL,
  `receiver` int(11) unsigned NOT NULL,
  `lus` int(2) unsigned NOT NULL,
  `timestamp` bigint(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `session` */

CREATE TABLE `session` (
  `sess_id` char(40) NOT NULL,
  `sess_datas` text NOT NULL,
  `sess_expire` bigint(20) NOT NULL,
  PRIMARY KEY (`sess_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `tracker` */

CREATE TABLE `tracker` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `idfix` int(11) NOT NULL DEFAULT '0',
  `state` int(1) NOT NULL DEFAULT '0',
  `priority` int(1) NOT NULL DEFAULT '0',
  `categorie` int(1) unsigned NOT NULL,
  `firstdate` int(255) unsigned NOT NULL,
  `lastdate` int(255) NOT NULL DEFAULT '0',
  `lienwowhead` text NOT NULL,
  `desc_offi` text NOT NULL,
  `desc_serv` text NOT NULL,
  `publisher` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `trackercomment` */

CREATE TABLE `trackercomment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bugid` int(11) unsigned NOT NULL,
  `publisher` int(11) unsigned NOT NULL,
  `content` text NOT NULL,
  `timestamp` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `login` varchar(255) NOT NULL,
  `login_crypt` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_login` varchar(255) NOT NULL DEFAULT '0.0.0.0',
  `ip_register` varchar(255) NOT NULL DEFAULT '0.0.0.0',
  `last_date_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `level` int(2) NOT NULL DEFAULT '0',
  `visible_name` varchar(255) NOT NULL,
  `sess_id` char(40) DEFAULT NULL,
  `date_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `vote` */

CREATE TABLE `vote` (
  `id` int(11) unsigned NOT NULL,
  `total` int(11) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `voting` */

CREATE TABLE `voting` (
  `id` int(11) unsigned NOT NULL,
  `accountid` int(11) unsigned NOT NULL,
  `date` int(255) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
