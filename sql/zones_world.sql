DROP TABLE IF EXISTS `zones`;
CREATE TABLE `zones` (
                         `id` smallint(5) unsigned NOT NULL,
                         `categorie` varchar(255) NOT NULL,
						 `zone` varchar(255) NOT NULL,
                         PRIMARY KEY (`id`)
                       ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4742,'Norfendre', 'Accostage de Hrothgar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3928,'Raids', 'Ahn\'Qiraj');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4378,'Ar�nes', 'Arene de Dalaran');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3698,'Norfendre', 'Arene de Nagrand');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3702,'Norfendre', 'Arene des Tranchantes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3790,'Donjons', 'Auchindoun: Cryptes Auchenai');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3789,'Donjons', 'Auchindoun: Labyrinthe des Ombres');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3791,'Donjons', 'Auchindoun: Les salles des Sethekk');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3792,'Donjons', 'Auchindoun: Tombes-mana');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4494,'Donjons', 'Azjol-Nerub: Ahn\'kahet : l\'Ancien royaume');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4277,'Donjons', 'Azjol-Nerub: Azjol-Nerub');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (16,'Kalimdor', 'Azshara');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3358,'Champs de bataille', 'Bassin Arathi');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3711,'Norfendre', 'Bassin de Sholazar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (618,'Kalimdor', 'Berceau-de-l\'Hiver');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (10,'Royaumes de l\'Est', 'Bois de la Penombre');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3430,'Royaumes de l\'Est', 'Bois des Chants eternels');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (5287,'Royaumes de l\'Est', 'Cap Strangleronce');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4603,'Raids', 'Caveau d\'Archavon');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (718,'Donjons', 'Cavernes des Lamentations');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4812,'Raids', 'Citadelle de la Couronne de glace');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4813,'Donjons', 'Citadelle de la Couronne de glace: Fosse de Saron');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4809,'Donjons', 'Citadelle de la Couronne de glace: La Forge des Ames');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4820,'Donjons', 'Citadelle de la Couronne de glace: Salles des Reflets');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3713,'Donjons', 'Citadelle des Flammes infernales: La Fournaise du sang');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3836,'Raids', 'Citadelle des Flammes infernales: Le repaire de Magtheridon');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3714,'Donjons', 'Citadelle des Flammes infernales: Les salles Brisees');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3562,'Donjons', 'Citadelle des Flammes infernales: Remparts des Flammes infernales');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (85,'Royaumes de l\'Est', 'Clairieres de Tirisfal');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2717,'Raids', 'Coeur du Magma');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (267,'	Royaumes de l\'Est', 'Contreforts de Hautebrande');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (490,'Kalimdor', 'Cratere d\'Un\'Goro');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4395,'Norfendre', 'Dalaran');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1657,'Kalimdor', 'Darnassus');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (41,'	Royaumes de l\'Est', 'Defile de Deuillevent');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (405,'Kalimdor', 'Desolace');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (65,'Norfendre', 'Desolation des dragons');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4196,'Donjons', 'Donjon de Drak\'Tharon');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3845,'Raids', 'Donjon de la Tempete: Donjon de la Tempete');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3847,'Donjons', 'Donjon de la Tempete: La Botanica');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3848,'Donjons', 'Donjon de la Tempete: L\'Arcatraz');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3849,'Donjons', 'Donjon de la Tempete: Le Mechanar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (209,'Donjons', 'Donjon d\'Ombrecroc');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1196,'Donjons', 'Donjon d\'Utgarde: Cime d\'Utgarde');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (206,'Donjons', 'Donjon d\'Utgarde: Donjon d\'Utgarde');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1,'Royaumes de l\'Est', 'Dun Morogh');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (14,'Kalimdor', 'Durotar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (357,'	Kalimdor', 'Fjord Hurlant');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (495,'Norfendre', 'Citadelle de la Couronne de glace: Salles des Reflets');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3519,'Outreterre', 'Foret de Terokkar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (12,'Royaumes de l\'Est', 'Foret d\'Elwynn');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (130,'Royaumes de l\'Est', 'Foret des Pins-Argentes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2817,'Norfendre', 'Foret du Chant de cristal');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1537,'Royaumes de l\'Est', 'Forgefer');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1497,'Royaumes de l\'Est', 'Fossoyeuse');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (361,'Kalimdor', 'Gangrebois');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (721,'Donjons', 'Gnomeregan');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (51,'Royaumes de l\'Est', 'Gorge des Vents brulants');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2437,'Donjons', 'Gouffre de Ragefeu');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3277,'Champs de bataille', 'Goulet des Chanteguerres');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2367,'Donjons', 'Grottes du Temps: Contreforts de Hautebrande d\'antan');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2366,'Donjons', 'Grottes du Temps: Le Noir marecage');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4100,'Donjons', 'Grottes du Temps: L\'Epuration de Stratholme');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3606,'Raids', 'Grottes du Temps: Sommet d\'Hyjal');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4416,'Donjons', 'Gundrak');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2557,'Donjons', 'Hache-Tripes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (45,'Royaumes de l\'Est', 'Hautes-terres Arathies');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1519,'Royaumes de l\'Est', 'Hurlevent');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3524,'Kalimdor', 'Ile de Brume-Azur');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3525,'Kalimdor', 'Ile de Brume-Sang');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4080,'Royaumes de l\'Est', 'Ile de Quel\'Danas');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4710,'Champs de bataille', 'Ile des Conquerants');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4197,'Norfendre', 'Joug-d\'hiver');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3457,'Raids', 'Karazhan');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (491,'Donjons', 'Kraal de Tranchebauge');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (210,'Norfendre', 'La Couronne de glace');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (457,'Kalimdor', 'La mer Voilee');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (717,'Donjons', 'La Prison');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4406,'Arenes', 'L\'arene des Valeureux');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4415,'Donjons', 'Le fort Pourpre');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4265,'Donjons', 'Le Nexus: Le Nexus');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4228,'Donjons', 'Le Nexus: L\'Oculus');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4500,'Raids', 'Le Nexus: L\'Oeil de l\'eternit�');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4723,'Donjons', 'L\'epreuve du champion');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4722,'Raids', 'L\'epreuve du crois�');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (44,'	Royaumes de l\'Est', 'Les Carmines');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (394,'	Norfendre', 'Les Grisonnes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (47,'Royaumes de l\'Est', 'Les Hinterlands');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1581,'Donjons', 'Les Mortemines');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (11,'Royaumes de l\'Est', 'Les Paluns');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (67,'Norfendre', 'Les pics Foudroyes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1638,'Kalimdor', 'Les Pitons-du-Tonnerre');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (406,'Kalimdor', 'Les Serres-Rocheuses');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3433,'Royaumes de l\'Est', 'Les terres Fantomes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3522,'Outreterre', 'Les Tranchantes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3557,'Kalimdor', 'L\'Exodar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (38,'	Royaumes de l\'Est', 'Loch Modan');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3820,'Champs de bataille', 'L\'Oeil du cyclone');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3487,'Royaumes de l\'Est', 'Lune-d\'argent');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4298,'Royaumes de l\'Est', 'Maleterres : l\'enclave Ecarlate');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (139,'Royaumes de l\'Est', 'Maleterres de l\'Est');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (28,'Royaumes de l\'Est', 'Maleterres de l\'Ouest');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (8,'Royaumes de l\'Est', 'Marais des Chagrins');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2100,'Donjons', 'Maraudon');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (40,'Royaumes de l\'Est', 'Marche de l\'Ouest');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (15,'Kalimdor', 'Marecage d\'Aprefange');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3521,'Outreterre', 'Marecage de Zangar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (400,'Kalimdor', 'Mille pointes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (796,'Donjons', 'Monast�re Ecarlate');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (25,'Royaumes de l\'Est', 'Mont Rochenoire');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (215,'Kalimdor', 'Mulgore');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3518,'Outreterre', 'Nagrand');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3456,'Raids', 'Naxxramas');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1637,'Kalimdor', 'Orgrimmar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (331,'Kalimdor', 'Orneval');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3483,'Outreterre', 'Peninsule des Flammes infernales');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1583,'Donjons', 'Pic Rochenoire');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4075,'Raids', 'Plateau du Puits de soleil');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (719,'Donjons', 'Profondeurs de Brassenoire');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1584,'Donjons', 'Profondeurs de Rochenoire');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3523,'Outreterre', 'Raz-de-Neant');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (493,'Kalimdor', 'Reflet-de-Lune');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3923,'Raids', 'Repaire de Gruul');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2677,'Raids', 'Repaire de l\'Aile noire');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2159,'Raids', 'Repaire d\'Onyxia');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3607,'Raids', 'Reservoir de Glissecroc: Caverne du sanctuaire du Serpent');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3716,'Donjons', 'Reservoir de Glissecroc: La Basse-tourbiere');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3715,'Donjons', 'Reservoir de Glissecroc: Le caveau de la Vapeur');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3717,'Donjons', 'Reservoir de Glissecroc: Les enclos aux esclaves');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4384,'Champs de bataille', 'Rivage des Anciens');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3429,'Raids', 'Ruines d\'Ahn\'Qiraj');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3968,'Arenes', 'Ruines de Lordaeron');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2057,'Donjons', 'Scholomance');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3703,'Outreterre', 'Shattrath');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1377,'Kalimdor', 'Silithus');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (148,'Kalimdor', 'Sombrivage');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (722,'Donjons', 'Souilles de Tranchebauge');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (46,'Royaumes de l\'Est', 'Steppes Ardentes');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (33,'Royaumes de l\'Est', 'Strangleronce septentrionale');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2017,'Donjons', 'Stratholme');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (440,'Kalimdor', 'Tanaris');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (17,'Kalimdor', 'Tarides du Nord');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4709,'Kalimdor', 'Tarides du Sud');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (141,'Kalimdor', 'Teldrassil');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4493,'Raids', 'Temple du Repos du ver: Le sanctum Obsidien');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4987,'Raids', 'Temple du Repos du ver: Le sanctum Rubis');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1477,'Donjons', 'Temple englouti');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3959,'Raids', 'Temple Noir');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4131,'Donjons', 'Terrasse des Magisteres');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4,'Royaumes de l\'Est', 'Terres Foudroyees');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3,'Royaumes de l\'Est', 'Terres Ingrates');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3537,'Norfendre', 'Toundra Boreenne');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2257,'Royaumes de l\'Est', 'Tram des profondeurs');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1337,'Donjons', 'Uldaman');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4273,'Raids', 'Ulduar');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4272,'Donjons', 'Ulduar: Les salles de Foudre');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (4264,'Donjons', 'Ulduar: Les salles de Pierre');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (2597,'Champs de bataille', 'Vallee d\'Alterac');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (5339,'Royaumes de l\'Est', 'Vallee de Strangleronce');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (3520,'Outreterre', 'Vallee d\'Ombrelune');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (66,'Norfendre', 'Zul\'Drak');
INSERT INTO `zones`(`id`,`categorie`, `zone`) VALUES (1176,'Donjons', 'Zul\'Farrak');
