<?php

if(empty($_GET['page']))
{
	$_GET['page'] = "home";
	$model = 'model/home.php';
	$view = 'home.php';
}
else
{
	switch($_GET['page'])
	{
		case "home":
			$model = 'model/home.php';
			$view = 'home.php';
			break;
		case "news":
			$model = 'model/news.php';
			$view = 'news.php';
			break;
		case "inscription":
			$model = 'model/inscription.php';
			$view = 'inscription.php';
			break;
		case "checkcode":
			$model = 'model/checkcode.php';
			$view = 'checkcode.php';
			break;
		case "pass":
			$model = 'model/pass.php';
			$view = 'pass.php';
			break;
		case "arenes":
			$model = 'model/arenes.php';
			$view = 'arenes.php';
			break;
		case "honneur":
			$model = 'model/honneur.php';
			$view = 'honneur.php';
			break;
		case "statistiques":
			$model = 'model/statistiques.php';
			$view = 'statistiques.php';
			break;
		case "bugstracker":
			$model = 'model/bugstracker.php';
			$view = 'bugstracker.php';
			break;
		case "addbug":
		    $model = 'model/addbug.php';
			$view = 'addbug.php';
			break;
		case "bug":
			$model = 'model/bug.php';
			$view = 'bug.php';
			break;
		case "newsall":
			$model = 'model/newsall.php';
			$view = 'newsall.php';
			break;
		case "armurerie":
			$model = 'model/armurerie.php';
			$view = 'armurerie.php';
			break;
		case "personnage":
			$model = 'model/personnage.php';
			$view = 'personnage.php';
			break;
		case "guild":
			$model = 'model/guild.php';
			$view = 'guild.php';
			break;
		case "arene":
			$model = 'model/arene.php';
			$view = 'arene.php';
			break;
		case "profil":
			$model = 'model/profil.php';
			$view = 'profil.php';
			break;
		case "online":
			$model = 'model/online.php';
			$view = 'online.php';
			break;
		case "equipe":
			$model = 'model/equipe.php';
			$view = 'equipe.php';
			break;
		case "boutique":
			$model = 'model/boutique.php';
			$view = 'boutique.php';
			break;
		case "vote":
			$model = 'model/vote.php';
			$view = 'vote.php';
			break;
		case "commentall":
			$model = 'model/commentall.php';
			$view = 'commentall.php';
			break;
		case "bugsall":
			$model = 'model/bugsall.php';
			$view = 'bugsall.php';
			break;
		case "bugscommentall":
			$model = 'model/bugscommentall.php';
			$view = 'bugscommentall.php';
			break;
		case "allprofils":
			$model = 'model/allprofils.php';
			$view = 'allprofils.php';
			break;
        case "achat":
            $model = 'model/achat.php';
            $view = 'achat.php';
            break;
		case "compte":
		case "privatesend":
		case "modpass":
		case "modmail":
		case "modvisiblename":
		case "changerace":
		case "renommer":
        case "customize":
        case "faction":
			$model = 'model/compte.php';
			$view = 'compte.php';
			break;
		default:
		    $model = 'model/home.php';
			$view = 'home.php';
			break;
	}
}
?>