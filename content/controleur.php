<?php
require_once "configuration.php";									// Fichier de configuration

require_once "classes/class.mysql.php";								// Classe mysql
require_once "classes/class.site.php";    							// Classe site
require_once "classes/class.user.php";   							// Classe user
require_once "classes/class.royaume.php";   						// Classe royaume
require_once "classes/class.soap.php";   					    	// Classe soap
require_once "classes/class.session.php";   					    // Classe session

require_once "pages.php";    					   					// Liste des pages du site

//on d�finit l'utilisation des sessions en personnel
ini_set('session.save_handler', 'user');

//on d�clare la classe
$session = new Session($array_db['host'], $array_db['user'], $array_db['pass'], $array_site['db_site']);

//on pr�cise les m�thodes � employer pour les sessions
session_set_save_handler(array($session, 'open'),
                         array($session, 'close'),
                         array($session, 'read'),
                         array($session, 'write'),
                         array($session, 'destroy'),
                         array($session, 'gc'));

//on d�marre la session
session_start();

// Cr�ation de la base site
$sql = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_site['db_site']);
// Cr�ation de la base auth
$dbauth = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_auth']);
// Cr�ation de la base characters
$dbcharacters = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_characters']);
// Cr�ation de la base world
$dbworld = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_world']);
// Cr�ation de l'objet site
$site = new site($sql, $array_site['nom'], $array_site['url']);
// Cr�ation de l'objet user
$user = new user($sql, $dbauth, $dbcharacters);
// Cr�ation de l'objet royaume
$royaume = new royaume($dbauth, $dbcharacters, $dbworld);

if ($royaume->isonline())
{
	$soap = new TCSoap(array('soap_user' => $array_soap['soap_user'], 'soap_pass' => $array_soap['soap_pass'], 'soap_port' => $array_soap['soap_port'],'addr' => $array_soap['addr']), $array_soap['debug']);
}

function format_post($post)
{
    return htmlentities($post, ENT_QUOTES, 'UTF-8');
}

if(count($_POST) < 100)
{
	array_map('format_post', $_POST);
}

$injection_detect = false;
foreach($_POST as $result)
{
	if ($site->sqlDetect($result))
	{
		$injection_detect = true;
	}
}
if ($injection_detect == true)
{
	$site->redirect('home',0);
}

if (!empty($_SESSION['id']))
{
	if ($user->set_id($_SESSION['id']))
	{
		$user->load_account();
	}
}
else
{
	if (!empty($_COOKIE["conection1"]) AND !empty($_COOKIE["conection2"]))
	{
		$Conn = $sql->query("SELECT id, level FROM `users` WHERE password = '".$_COOKIE["conection1"]."' AND login_crypt = '".$_COOKIE["conection2"]."'");
		$val = $Conn->fetch_array();
		$id = $val['id'];
		$level = $val['level'];
		if ($user->set_id($id))
		{
			$_SESSION['id'] = $id;
			$_SESSION['level'] = $level;
			$user->load_account();
			$sql->query("UPDATE users SET ip_login = '".$user->ipaddr."', last_date_login = NOW(), sess_id = '".$_COOKIE["PHPSESSID"]."' WHERE id = '".$id."'");
		}
	}
}

if(empty($user->sess_id))
{
	if (!empty($_POST['Connexion']) && $_POST['Connexion'] == 'ok')
	{
		if($_POST['login_connexion'] != '' && $_POST['pass_connexion'] != '')
		{
			// On initialise $existence_ft � 0
			$existence_ft = 0;
			// On initialise $tentatives � 0
			$tentatives = 0;
			// Si le fichier existe, on le lit
			if (file_exists('content/session_cache/'.$_POST['login_connexion'].'.tmp'))
			{
				// On ouvre le fichier
				$fichier = fopen('content/session_cache/'.$_POST['login_connexion'].'.tmp', 'r+');

				if (!empty($fichier))
				{
					// On r�cup�re le contenu de la ligne
					$contenu= fgets($fichier, 4096);
					if (!empty($contenu))
					{
						// On d�coupe le contenu du fichier pour r�cup�rer les informations
						$contenu_array = explode(';', $contenu);

						// Si la date du fichier est celle d'aujourd'hui, on r�cup�re le nombre de tentatives
						if($contenu_array[0] == date('d/m/Y'))
						{
							$tentatives = $contenu_array[1];
							$existence_ft = 2;
						}
						else
						{
							$existence_ft = 1;
						}
					}
					else
					{
						$existence_ft = 1;
					}
				}
			}
			// S'il y a eu moins de 5 identifications rat�es dans la journ�e, on laisse passer
			if($tentatives < 5)
			{
				$result = "";
				$login = strtolower($_POST['login_connexion']);
				$password = $site->sha_password($login, $_POST['pass_connexion']);
				$Conn = $sql->query("SELECT id, login, password, level FROM users WHERE login = '".$login."'");
				$ligne = $Conn->fetch_array();
				$loginsql = $ligne['login'];
				$passsql = $ligne['password'];
				$id = $ligne['id'];
				if($login == $loginsql)
				{
					if($password == $passsql)
					{
						$result = 'good';
						if ($user->set_id($id))
						{
							$_SESSION['id'] = $id;
							$_SESSION['level'] = $ligne['level'];
							$user->load_account();
							if (!empty($_POST['souvenir']))
							{
								$login_crypt = $site->sha_password($_POST['pass_connexion'], $login);
								SetCookie("conection1", $password, time() + 3600*48);
								SetCookie("conection2", $login_crypt, time() + 3600*48);
							}
							$sql->query("UPDATE users SET ip_login = '".$user->ipaddr."', last_date_login = NOW(), sess_id = '".$_COOKIE["PHPSESSID"]."' WHERE id = '".$id."'");
						}
					}
					else
					{
						$result = 'wrong_password';
					}
				}
				else
				{
					$result = 'wrong_login';
				}
				if ($result == 'good')
				{	
					if ($existence_ft > 0)
					{
						unlink('content/session_cache/'.$_POST['login_connexion'].'.tmp');
					}
				}
				else if ($result == 'wrong_password')
				{
					// Si le fichier n'existe pas encore, on le cr��
					if($existence_ft == 0)
					{
						// On cr�� le fichier puis on l'ouvre
						$fichier = fopen('content/session_cache/'.$_POST['login_connexion'].'.tmp', 'a+');
						if (!empty($fichier))
						{
							fputs($fichier, date('d/m/Y').';1');
						}
					}
					elseif($existence_ft == 1)
					{
						// On remet le curseur au d�but du fichier
						rewind($fichier);
						ftruncate($fichier,0);
						fputs($fichier, date('d/m/Y').';1');
					}
					elseif($existence_ft == 2)
					{
						// On remet le curseur au d�but du fichier
						rewind($fichier);
						ftruncate($fichier,0);
						$tentatives = $tentatives + 1;
						fputs($fichier, date('d/m/Y').';'.$tentatives.'');
						if($tentatives > 4)
						{
							$site->block_account($_POST['login_connexion'], $user->ipaddr);
						}
					}
				}
			}
			else
			{
				$result = 'too_many_connections';
			}
			// Si on a ouvert un fichier, on le referme (eh oui, il ne faut pas l'oublier)
			if(!empty($fichier))
			{
				fclose($fichier);
			}
		}
	}
}
else if (!empty($user->sess_id))
{
	if (!empty($_POST['Deconnexion']) && $_POST['Deconnexion'] == 'ok')
	{
		$sql->query("UPDATE users SET sess_id = '' WHERE id = '".$user->sess_id."'");
		$_SESSION = array();
		setcookie("conection1", "", time() - 3600);
		setcookie("conection2", "", time() - 3600);
		session_destroy();
		$user->unload_account();
	}
}

if (!empty($model))
{
	require_once $model;												// Appel du modele de la page
}

require_once "model/view.php";

ob_start();
require_once "view/view.php";											// Appel de la vue de la page
ob_end_flush();

$_SESSION['error'] = '';
?>