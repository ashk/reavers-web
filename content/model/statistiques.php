<?php
$site->mustBeLogin(MODO);

$realmlist = $royaume->getrealmlist();
$alliance = $royaume->getplayers(1);
$horde = $royaume->getplayers(2);
$human = 0;
$nain = 0;
$elfe = 0;
$gnome = 0;
$draenei = 0;
$orc = 0;
$mortvivant = 0;
$tauren = 0;
$troll = 0;
$elfesang = 0;
$guerrier = 0;
$paladin = 0;
$chasseur = 0;
$voleur = 0;
$pretre = 0;
$chevaliermort = 0;
$chaman = 0;
$mage = 0;
$demoniste = 0;
$druide = 0;
foreach($alliance as $player)
{
	if ($player['race'] == 1)
	{
		$human = $human + 1;
	}
	elseif ($player['race'] == 3)
	{
		$nain = $nain + 1;
	}
	elseif ($player['race'] == 4)
	{
		$elfe = $elfe + 1;
	}
	elseif ($player['race'] == 7)
	{
		$gnome = $gnome + 1;
	}
	elseif ($player['race'] == 11)
	{
		$draenei = $draenei + 1;
	}
	if ($player['class'] == 1)
	{
		$guerrier = $guerrier + 1;
	}
	elseif ($player['class'] == 2)
	{
		$paladin = $paladin + 1;
	}
	elseif ($player['class'] == 3)
	{
		$chasseur = $chasseur + 1;
	}
	elseif ($player['class'] == 4)
	{
		$voleur = $voleur + 1;
	}
	elseif ($player['class'] == 5)
	{
		$pretre = $pretre + 1;
	}
	elseif ($player['class'] == 6)
	{
		$chevaliermort = $chevaliermort + 1;
	}
	elseif ($player['class'] == 7)
	{
		$chaman = $chaman + 1;
	}
	elseif ($player['class'] == 8)
	{
		$mage = $mage + 1;
	}
	elseif ($player['class'] == 9)
	{
		$demoniste = $demoniste + 1;
	}
	elseif ($player['class'] == 11)
	{
		$druide = $druide + 1;
	}
}
foreach($horde as $player)
{
	if ($player['race'] == 2)
	{
		$orc = $orc + 1;
	}
	elseif ($player['race'] == 5)
	{
		$mortvivant = $mortvivant + 1;
	}
	elseif ($player['race'] == 6)
	{
		$tauren = $tauren + 1;
	}
	elseif ($player['race'] == 8)
	{
		$troll = $troll + 1;
	}
	elseif ($player['race'] == 10)
	{
		$elfesang = $elfesang + 1;
	}
	if ($player['class'] == 1)
	{
		$guerrier = $guerrier + 1;
	}
	elseif ($player['class'] == 2)
	{
		$paladin = $paladin + 1;
	}
	elseif ($player['class'] == 3)
	{
		$chasseur = $chasseur + 1;
	}
	elseif ($player['class'] == 4)
	{
		$voleur = $voleur + 1;
	}
	elseif ($player['class'] == 5)
	{
		$pretre = $pretre + 1;
	}
	elseif ($player['class'] == 6)
	{
		$chevaliermort = $chevaliermort + 1;
	}
	elseif ($player['class'] == 7)
	{
		$chaman = $chaman + 1;
	}
	elseif ($player['class'] == 8)
	{
		$mage = $mage + 1;
	}
	elseif ($player['class'] == 9)
	{
		$demoniste = $demoniste + 1;
	}
	elseif ($player['class'] == 11)
	{
		$druide = $druide + 1;
	}
}
$accounts = $royaume->getaccount();

$uptimemax = $royaume->getuptimemax();
$uptime = $royaume->getuptime();

$joursmax = floor($uptimemax['uptime']/86400);
$reste = $uptimemax['uptime']%86400;
$heuresmax = floor($reste/3600);
$reste = $reste%3600;
$minutesmax = floor($reste/60);

$jours = floor($uptime['uptime']/86400);
$reste = $uptime['uptime']%86400;
$heures = floor($reste/3600);
$reste = $reste%3600;
$minutes = floor($reste/60);
?>
