<?php
if (empty($_POST['choix']))
	$choix = 1;
else
	$choix = $_POST['choix'];

if (isset($_POST['name']) && !empty($_POST['name']))
{
	$name = $_POST['name'];
	if ($choix == 1)
	{
		$players = array();
		$playerslist = $royaume->getplayers(0);

		if (!empty($playerslist))
		{
			foreach ($playerslist as $player)
			{
				$name1 = $player['name'];
				if (preg_match("/$name/i", "$name1"))
					$players[] = $player;
			}
		}
		$nbrPersos = sizeof($players);
	}
	else if ($choix == 2)
	{
		$guilds = array();
		$guildslist = $royaume->getguilds();

		if (!empty($guildslist))
		{
			foreach ($guildslist as $guild)
			{
				$name1 = $guild['name'];
				if (preg_match("/$name/i", "$name1"))
					$guilds[] = $guild;
			}
		}
		$nbrGuilds = sizeof($guilds);
	}
	else if ($choix == 3)
	{
		$arenes = array();
		if (empty($_POST['type']))
			$type = 2;
		else
			$type = $_POST['type'];
		$areneslist = $royaume->loadarenes(0,$type);

		if (!empty($areneslist))
		{
			foreach ($areneslist as $arene)
			{
				$name1 = $arene['name'];
				if (preg_match("/$name/i", "$name1"))
					$arenes[] = $arene;
			}
		}
		$nbrTeams = sizeof($arenes);
	}
}
else
{
	if ($choix == 1)
	{
		$nbrPersos = $royaume->getNbrPlayers();
		$nbrPages = ceil($nbrPersos / 20);
	}
	else if ($choix == 2)
	{
		$nbrGuilds = $royaume->getNbrGuilds();
		$nbrPages = ceil($nbrGuilds / 20);
	}
	else if ($choix == 3)
	{
		if (empty($_POST['type']))
			$type = 2;
		else
			$type = $_POST['type'];

		$nbrTeams = $royaume->getNbrTeams($type);
		$nbrPages = ceil($nbrTeams / 20);
	}
}
?>
