<?php
if (!empty($_GET['variable1']) && is_numeric($_GET['variable1']))
{
	$guildid = $_GET['variable1'];
	$guild = $royaume->getguild_by_guildid($guildid);
	$guildmembers = $royaume->getguildmember_by_guildid($guildid);
	foreach($guildmembers as $key => $guildmember)
	{
		$character = $royaume->get_character_guid($guildmember['guid']);
		$guildmembers[$key]['name'] = $character['name'];
		$guildmembers[$key]['level'] = $character['level'];
		$guildmembers[$key]['race'] = $character['race'];
		$guildmembers[$key]['gender'] = $character['gender'];
		$guildmembers[$key]['class'] = $character['class'];
		$guildmembers[$key]['online'] = $character['online'];
	}
}
?>