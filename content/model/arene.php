<?php
if (!empty($_GET['variable1']) && is_numeric($_GET['variable1']))
{
	$arenaTeamId = $_GET['variable1'];
	$arenaTeam = $royaume->getarenateam_by_arenaTeamId($arenaTeamId);
	$arenaTeam['weekLoose'] = $arenaTeam['weekGames'] - $arenaTeam['weekWins'];
	$arenaTeam['weekPercentWin'] = 0;
	if (!empty($arenaTeam['weekGames']))
		$arenaTeam['weekPercentWin'] = ($arenaTeam['weekWins'] * 100) / $arenaTeam['weekGames'];
	$arenaTeam['seasonLoose'] = $arenaTeam['seasonGames'] - $arenaTeam['seasonWins'];
	$arenaTeam['seasonPercentWin'] = 0;
	if (!empty($arenaTeam['seasonGames']))
		$arenaTeam['seasonPercentWin'] = ($arenaTeam['seasonWins'] * 100) / $arenaTeam['seasonGames'];
	$arenaTeamMembers = $royaume->getstatmember_by_arenaTeamId($arenaTeamId);
	foreach($arenaTeamMembers as $key => $arenaTeamMember)
	{
		$arenaTeamMembers[$key]['weekLoose'] = $arenaTeamMember['weekGames'] - $arenaTeamMember['weekWins'];
		$arenaTeamMembers[$key]['weekPercentWin'] = 0;
		if (!empty($arenaTeamMember['weekGames']))
			$arenaTeamMembers[$key]['weekPercentWin'] = ($arenaTeamMember['weekWins'] * 100) / $arenaTeamMember['weekGames'];
		$arenaTeamMembers[$key]['weekPercentGames'] = 0;
		if (!empty($arenaTeam['weekGames']))
			$arenaTeamMembers[$key]['weekPercentGames'] = ($arenaTeamMember['weekGames'] * 100) / $arenaTeam['weekGames'];
		$arenaTeamMembers[$key]['seasonLoose'] = $arenaTeamMember['seasonGames'] - $arenaTeamMember['seasonWins'];
		$arenaTeamMembers[$key]['seasonPercentWin'] = 0;
		if (!empty($arenaTeamMember['seasonGames']))
			$arenaTeamMembers[$key]['seasonPercentWin'] = ($arenaTeamMember['seasonWins'] * 100) / $arenaTeamMember['seasonGames'];
		$arenaTeamMembers[$key]['seasonPercentGames'] = 0;
		if (!empty($arenaTeam['seasonGames']))
			$arenaTeamMembers[$key]['seasonPercentGames'] = ($arenaTeamMember['seasonGames'] * 100) / $arenaTeam['seasonGames'];
	}
}
?>
