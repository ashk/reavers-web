<?php

require_once('content/classes/class.captcha.php');

$site->mustBeLogin(INSCRIT);
$captchaCheck = new Captcha(SECRET);

if (!empty($_POST['site']) && is_numeric($_POST['site']))
{
	$date = time();
	$Conn = $sql->query("SELECT * FROM voting WHERE accountid = '".$user->sess_id."' AND id = '".$_POST['site']."'");
    if ($captchaCheck->checkCode($_POST['g-recaptcha-response'])) {
        if ($Conn->num_rows > 0) {
            $sitevote = $Conn->fetch_array();
            $datevote = $sitevote['date'] + $tab_sites[$_POST['site']][2];
            if ($date >= $datevote) {
                $Conn = $sql->query("UPDATE `vote` SET total = (total + 1) WHERE id = '" . $user->sess_id . "'");
                $Conn = $sql->query("UPDATE `voting` SET date = '" . $date . "' WHERE id = '" . $_POST['site'] . "' AND accountid = '" . $user->sess_id . "'");
                $Conn = $sql->query("UPDATE `points` SET points = (points + 1) WHERE id = '" . $user->sess_id . "'");
                $sql->query("INSERT INTO `logvote` (id, message, site) VALUES (" . $user->sess_id . ", 'Ce compte a été utilisé pour voter', '" . $tab_sites[$_POST['site']][0] . "')");
                $user->load_account();
                $result = 1;
            } else {
                $delay = $datevote - $date;
                $jours = floor($delay / 86400);
                $reste = $delay % 86400;
                $heures = floor($reste / 3600);
                $reste = $reste % 3600;
                $minutes = floor($reste / 60);
                $sql->query("INSERT INTO `logvote` (id, message, site) VALUES (" . $user->sess_id . ", 'Ce compte a été utilisé pour une tentative de vote', '" . $tab_sites[$_POST['site']][0] . "')");
                $result = -1;
            }
        } else {
            $sql->query("INSERT INTO `voting` (id,accountid,date) VALUES (" . $_POST['site'] . "," . $user->sess_id . "," . $date . ")");
            $Conn = $sql->query("UPDATE `vote` SET total = (total + 1) WHERE id = '" . $user->sess_id . "'");
            $Conn = $sql->query("UPDATE `points` SET points = (points + 1) WHERE id = '" . $user->sess_id . "'");
            $sql->query("INSERT INTO `logvote` (id, message, site) VALUES (" . $user->sess_id . ", 'Ce compte a été utilisé pour voter', '" . $tab_sites[$_POST['site']][0] . "')");
            $user->load_account();
            $result = 1;
        }
    } else {
        $result['error_captcha'] = true;
    }
}
else
{
	if (!empty($tab_sites))
	{
		$date = time();
		foreach($tab_sites as $key => $vote)
		{
			$Conn = $sql->query("SELECT * FROM voting WHERE accountid = '".$user->sess_id."' AND id = '".$key."'");
			if($Conn->num_rows > 0)
			{
				$sitevote = $Conn->fetch_array();
				$datevote = $sitevote['date'] + $tab_sites[$key][2];
				if ($date >= $datevote)
				{
					$result[$key] = 1;
				}
				else
				{
					$delay[$key] = $datevote - $date;
					$result[$key] = 0;
				}
			}
			else
			{
				$result[$key] = 1;
			}
		}
		if (!empty($delay))
		{
			sort($delay);
			$jours = floor($delay[0]/86400);
			$reste = $delay[0]%86400;
			$heures = floor($reste/3600);
			$reste = $reste%3600;
			$minutes = floor($reste/60);
		}
	}
}
?>