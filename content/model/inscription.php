<?php

require_once('content/classes/class.captcha.php');

$captchaCheck = new Captcha(SECRET);

$site->mustBeNotLogin();

if(!empty($_POST['inscription']))
{
	/* On recup les variables ! */
	$login = trim(strip_tags($_POST['login']));
	$pass = trim(strip_tags($_POST['pass']));
	$passconf = trim(strip_tags($_POST['passconf']));
	$mail = trim(strip_tags($_POST['mail']));
	$mailconf = trim(strip_tags($_POST['mailconf']));
    $captcha = trim(strip_tags($_POST['g-recaptcha-response']));


    // Vérification captcha
    $result = $captchaCheck->checkCode($captcha);
    if ($result === false) {
        $inscription['error_capacha'] = 1;
    }

	$result = $site->checkmdp($pass, $passconf);
	if ($result == 'tooshort')
	{
		$inscription['pass_tooshort'] = 1;
	}
	else if ($result == 'toolong')
	{
		$inscription['pass_toolong'] = 1;
	}
	else if ($result == 'nofigure')
	{
		$inscription['pass_nofigure'] = 1;
	}
	else if ($result == 'noupcap')
	{
		$inscription['pass_noupcap'] = 1;
	}
	else if ($result == 'format')
	{
		$inscription['pass_format'] = 1;
	}
	else if ($result == 'noconf')
	{
		$inscription['pass_noconf'] = 1;
	}
	else if ($result == 'empty')
	{
		$inscription['empty'] = 1;
	}

	$result = $site->checkmail($mail, $mailconf);
	if ($result == 'noconf')
	{
		$inscription['mail_noconf'] = 1;
	}
	else if ($result == 'format')
	{
		$inscription['mail_format'] = 1;
	}
	else if ($result == 'exist')
	{
		$inscription['mail_exist'] = 1;
	}
	else if ($result == 'empty')
	{
		$inscription['empty'] = 1;
	}

	$result = $site->checkpseudo($login);
	if ($result == 'tooshort')
	{
		$inscription['login_tooshort'] = 1;
	}
	else if ($result == 'toolong')
	{
		$inscription['login_toolong'] = 1;
	}
	else if ($result == 'format')
	{
		$inscription['login_format'] = 1;
	}
	else if ($result == 'exist')
	{
		$inscription['login_exist'] = 1;
	}
	else if ($result == 'empty')
	{
		$inscription['empty'] = 1;
	}



	if (empty($inscription))
	{
		$login = strtolower($login);
		$pass_sha1 = $site->sha_password($login, $pass);
		$login_crypt = $site->sha_password($pass, $login);
		$addr_ip_vis = $_SERVER["REMOTE_ADDR"];

		$dbauth->query("INSERT INTO account (username,sha_pass_hash,email) VALUES ('".$login."','".$pass_sha1."','".$mail."')");
		$resid = $dbauth->Get_insert_Id();
		$sql->query("INSERT INTO users (id, login, login_crypt, password, email, ip_register, level, visible_name) VALUES ('".$resid."', '".$login."', '".$login_crypt."', '".$pass_sha1."', '".$mail."', '".$addr_ip_vis."', '0', '".$login."')");
		$sql->query("INSERT INTO points VALUES ('".$resid."', '0')");
		$sql->query("INSERT INTO vote VALUES ('".$resid."', '0')");
	}
}
?>
