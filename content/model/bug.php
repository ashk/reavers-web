<?php

$site->mustBeLogin(INSCRIT);

if (!empty($_GET['variable1']) && is_numeric($_GET['variable1']))
{
	$bugid = $_GET['variable1'];

	if (!$user->is_banned_by_ip() && !$user->is_banned_by_account() && empty($user->array_account['locked']))
	{
		if (isset($_POST['ajouter']) && $_POST['ajouter'] == 'ajouter')
		{
			if (!empty($_POST['content']))
				$user->savebugcomment($bugid, $_POST['content']);
		}
	}

	$bug = $site->get_bug_by_id($bugid);

	$nbrComments = $site->loadNbrComByBugId($bugid);
	$nbrPages = ceil($nbrComments / 3);
}
?>
