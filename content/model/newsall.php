<?php
if (!empty($_POST['remove']))
{
	if (!empty($_POST['newsId']) && is_numeric($_POST['newsId']))
	{
		if ($user->isLogged())
		{
			if ($user->checknews($_POST['newsId']))
			{
				$site->delnews_by_id($_POST['newsId']);
				$site->delcomment_by_newsid($_POST['newsId']);
			}
		}
	}
}

$nbrNews = $site->loadNbrNews();
$nbrPages = ceil($nbrNews / 3);
?>