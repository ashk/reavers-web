<?php

$site->mustBeLogin(INSCRIT);

switch ($_GET['page'])
{
	case 'privatesend':
        if (!empty($_GET['variable1']) && is_numeric($_GET['variable1']))
		{
			$idObject = $_GET['variable1'];
            if ($user->isprivateuser($idObject))
            {
                $user->setlus($idObject);
                if (isset($_POST['Envoyer']) && $_POST['Envoyer'] == 'Envoyer')
				{
                    if (!empty($_POST['content']))
                        $user->saveprivatemsg($idObject, $_POST['content']);
                }

				$privateobjet = $site->loadObjectByObjectId($idObject);
				$nbrMessages = $site->loadNbrMessages($idObject);
				$nbrPages = ceil($nbrMessages / 10);
			}
        }
        else if (empty($_GET['variable1']))
        {
            if (!empty($_POST['Envoyer']) && $_POST['Envoyer'] == 'Envoyer')
			{
                if (!empty($_POST['title']) && !empty($_POST['content']) && !empty($_POST['receiver']) && is_numeric($_POST['receiver']))
                {
                    $resid = $user->saveobjetmsg($_POST['receiver'], $_POST['title']);
                    if (is_numeric($resid))
                        $user->saveprivatemsg($resid, $_POST['content']);
                }
            }
			$nbrObjectSend = $user->loadNbrObjectSend();
			$nbrObjectReceive = $user->loadNbrObjectReceive();
			$nbrSendPages = ceil($nbrObjectSend / 10);
			$nbrReceivePages = ceil($nbrObjectReceive / 10);
        }
		break;
	case 'modpass':
		if (isset($_POST['modpassvalid']))
		{
			if (!empty($_POST['oldpass']) && !empty($_POST['pass']) && !empty($_POST['confirmpass']))
			{
                if ($user->ispassword($_POST['oldpass']))
                {
					if ($_POST['pass'] == $_POST['confirmpass'])
					{
                        $user->changepassword($_POST['pass']);
					}
				}
			}
		}
		break;
    case 'modmail':
        if (isset($_POST['modmail']))
		{
			if (!empty($_POST['oldmail']) && !empty($_POST['mail']) && !empty($_POST['confirmmail']))
			{
                if ($user->ismail($_POST['oldmail']))
                {
					if ($_POST['mail'] == $_POST['confirmmail'])
					{
                        $user->changemail($_POST['mail']);
					}
				}
			}
		}
        break;
    case 'modvisiblename':
        if (isset($_POST['modvisiblename']))
		{
			if (!empty($_POST['name']))
			{
                $user->changevisiblename($_POST['name']);
			}
		}
        break;
    /*case 'changerace':
        if (isset($_POST['personnage']) && !empty($_POST['personnage']))
        {
            $result = $user->changerace($_POST['personnage']);
        }
        else
        {
            $characters = $user->get_characters();
        }
        break;
    case 'renommer':
        if (isset($_POST['personnage']) && !empty($_POST['personnage']))
        {
            $result = $user->renommer($_POST['personnage']);
        }
        else
        {
            $characters = $user->get_characters();
        }
        break;
    case 'customize':
        if (isset($_POST['personnage']) && !empty($_POST['personnage']))
        {
            $result = $user->customize($_POST['personnage']);
        }
        else
        {
            $characters = $user->get_characters();
        }
        break;
    case 'faction':
        if (isset($_POST['personnage']) && !empty($_POST['personnage']))
        {
            $result = $user->faction($_POST['personnage']);
        }
        else
        {
            $characters = $user->get_characters();
        }
        break;
	case 'compte':
		$idban = $user->is_banned_by_account();
		$ipban = $user->is_banned_by_ip();
		break;*/
}
?>