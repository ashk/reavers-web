<?php
if (!empty($_GET['variable1']) && is_numeric($_GET['variable1']))
{
	$guid = $_GET['variable1'];
	$arenastat2c2 = $royaume->getstatmember_by_guid(2, $guid);
	$arenastat3c3 = $royaume->getstatmember_by_guid(3, $guid);
	$arenastat5c5 = $royaume->getstatmember_by_guid(5, $guid);
	if (!empty($arenastat2c2))
	{
		$arenateam2c2 = $royaume->getarenateam_by_arenaTeamId($arenastat2c2['arenaTeamId']);
		$arenastat2c2['weekLoose'] = $arenastat2c2['weekGames'] - $arenastat2c2['weekWins'];
		$arenastat2c2['weekPercentWin'] = 0;
		if (!empty($arenastat2c2['weekGames']))
			$arenastat2c2['weekPercentWin'] = ($arenastat2c2['weekWins'] * 100) / $arenastat2c2['weekGames'];
		$arenastat2c2['seasonLoose'] = $arenastat2c2['seasonGames'] - $arenastat2c2['seasonWins'];
		$arenastat2c2['seasonPercentWin'] = 0;
		if (!empty($arenastat2c2['seasonGames']))
			$arenastat2c2['seasonPercentWin'] = ($arenastat2c2['seasonWins'] * 100) / $arenastat2c2['seasonGames'];
	}
	if (!empty($arenastat3c3))
	{
		$arenateam3c3 = $royaume->getarenateam_by_arenaTeamId($arenastat3c3['arenaTeamId']);
		$arenastat3c3['weekLoose'] = $arenastat3c3['weekGames'] - $arenastat3c3['weekWins'];
		$arenastat3c3['weekPercentWin'] = 0;
		if (!empty($arenastat3c3['weekGames']))
			$arenastat3c3['weekPercentWin'] = ($arenastat3c3['weekWins'] * 100) / $arenastat3c3['weekGames'];
		$arenastat3c3['seasonLoose'] = $arenastat3c3['seasonGames'] - $arenastat3c3['seasonWins'];
		$arenastat3c3['seasonPercentWin'] = 0;
		if (!empty($arenastat3c3['seasonGames']))
			$arenastat3c3['seasonPercentWin'] = ($arenastat3c3['seasonWins'] * 100) / $arenastat3c3['seasonGames'];
	}
	if (!empty($arenastat5c5))
	{
		$arenateam5c5 = $royaume->getarenateam_by_arenaTeamId($arenastat5c5['arenaTeamId']);
		$arenastat5c5['weekLoose'] = $arenastat5c5['weekGames'] - $arenastat5c5['weekWins'];
		$arenastat5c5['weekPercentWin'] = 0;
		if (!empty($arenastat5c5['weekGames']))
			$arenastat5c5['weekPercentWin'] = ($arenastat5c5['weekWins'] * 100) / $arenastat5c5['weekGames'];
		$arenastat5c5['seasonLoose'] = $arenastat5c5['seasonGames'] - $arenastat5c5['seasonWins'];
		$arenastat5c5['seasonPercentWin'] = 0;
		if (!empty($arenastat5c5['seasonGames']))
			$arenastat5c5['seasonPercentWin'] = ($arenastat5c5['seasonWins'] * 100) / $arenastat5c5['seasonGames'];
	}
	$row_characters_guid = $royaume->get_character_guid($guid);
	$row_item = $royaume->get_itemcharacter_by_guid($guid);
	$guild = $royaume->getguild_by_guidcharacter($guid);
	if (!empty($guild))
	{
		$guildmember = $royaume->getguildmember_by_memberguid($guild['guildid'], $guid);
		$rankname = $royaume->getnamerank_by_rankid($guild['guildid'], $guildmember['rank']);
	}
	$row_stats = $royaume->get_stats_by_guidcharacter($guid);
	$or = floor($row_characters_guid['money']/10000);
	$reste = $row_characters_guid['money']%10000;
	$argent = floor($reste/100);
	$cuivre = $reste%100;
	$jours = floor($row_characters_guid['totaltime']/86400);
	$reste = $row_characters_guid['totaltime']%86400;
	$heures = floor($reste/3600);
	$reste = $reste%3600;
	$minutes = floor($reste/60);
	foreach($row_item as $item)
	{
		if (empty($item['guid']))
			$row_icon[] = array();
		else
			$row_icon[] = $royaume->get_icon_by_itemguid($item['guid']);
	}
	$character_model = "";
	if (!empty($row_item[0]))
		$character_model = "1,".$row_item[0]['displayid']."";
	if (!empty($row_item[2]))
		$character_model = "".$character_model.",3,".$row_item[2]['displayid']."";
	if (!empty($row_item[3]))
		$character_model = "".$character_model.",4,".$row_item[3]['displayid']."";
	if (!empty($row_item[4]))
		$character_model = "".$character_model.",5,".$row_item[4]['displayid']."";
	if (!empty($row_item[5]))
		$character_model = "".$character_model.",6,".$row_item[5]['displayid']."";
	if (!empty($row_item[6]))
		$character_model = "".$character_model.",7,".$row_item[6]['displayid']."";
	if (!empty($row_item[7]))
		$character_model = "".$character_model.",8,".$row_item[7]['displayid']."";
	if (!empty($row_item[8]))
		$character_model = "".$character_model.",9,".$row_item[8]['displayid']."";
	if (!empty($row_item[9]))
		$character_model = "".$character_model.",10,".$row_item[9]['displayid']."";
	if (!empty($row_item[13]))
		$character_model = "".$character_model.",14,".$row_item[13]['displayid']."";
	if (!empty($row_item[14]))
		$character_model = "".$character_model.",16,".$row_item[14]['displayid']."";
	if (!empty($row_item[15]))
		$character_model = "".$character_model.",13,".$row_item[15]['displayid']."";
	if (!empty($row_item[16]))
		$character_model = "".$character_model.",22,".$row_item[16]['displayid']."";
}
?>