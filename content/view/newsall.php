<div id='chemin'>
	<a href='home'>Accueil</a> > Toutes les news
</div>
<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Toutes les news</center></div>
<div class="cadre">
	<select name="limit" onchange="newsRequest(this.value, <?php echo $user->isLogged() ?>);">
		<?php
		for ($i = 0; $i < $nbrPages; $i++)
		{
			$index = $i + 1;
			?>
			<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 3) + 1) ?> - <?php echo (($i + 1) * 3) ?></option>
			<?php
		}
		echo '
	</select>
	<br />
	<br />
	';
	?>
	<span id="allNews"></span>
</div>

<script type="text/javascript" src="js/news.js"></script>
<script type="text/javascript">
	newsRequest(1, <?php echo $user->isLogged() ?>);
</script>