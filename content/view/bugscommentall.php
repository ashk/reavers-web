<?php
if (!empty($user->sess_id))
{
	if (!empty($profil))
	{
		if (!empty($comments))
		{
			echo '
			<div id="chemin">
				<a href="home">Accueil</a> > <a href="profil-'.$id.'">Profil de '.$profil['visible_name'].'</a> > Commentaires
			</div>
			<div style="width:650px;margin-left:15px;margin-bottom:5px;margin-top:15px;font-size:40px;"><center>Tout les commentaires</center></div>
			<div class="cadre2">
				';
				foreach($comments as $comment)
				{
					$bug = $site->get_bug_by_id($comment['bugid']);
					echo '
					<div class="newscontent">
						<div id="listbug">
							Discussion : <a href="bug-'.$bug['id'].'">'.$bug['title'].'</a>
							<div style="margin-left:10px;">
								Message : '.$site->tronquer($comment['content'], 50).'
							</div>
							<br />
							<div style="float:right;position:relative;bottom:10px;">
								Publi� le '.$comment['date'].' � '.$comment['heure'].'
							</div>
						</div>
					</div>
					<br />
					';
				}
				echo '
			</div>
			';
		}
	}
}
?>