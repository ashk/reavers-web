<div id='chemin'>
	<a href='home'>Accueil</a> > <a href='bugstracker-<?php echo $bug['categorie']; ?>'>Bugstracker</a> > Bug
</div>
<div style='margin-left:18px;margin-bottom:5px;font-size:40px;'>Bug : <?php echo $bug['title']; ?></div>
<div class='cadre2'>
	<div id='cadrepriority'>
		<div style='padding:10px;text-align:center;'>
			<font color="#2E006C">Priorit� :</font>
			<div style='font-size:15px;'>
				<?php echo $site->get_priority_by_priorityid($bug['priority']); ?>
			</div>
		</div>
	</div>
	<div id='cadrestatus'>
		<div style='padding:10px;text-align:center;'>
			<font color="#2E006C">Status :</font>
			<div style='font-size:15px;'>
				<?php echo $site->get_state_by_stateid($bug['state']); ?>
			</div>
		</div>
	</div>
	<div class='newscontent'>
		<div style='margin-left:30px;margin-right:10px;margin-bottom:10px;margin-top:10px;font-size:16px;'>
			<li><font color="#2E006C">Date de soumission : </font><span>le <?php echo date("d-m-Y � H:i:s", $bug['firstdate']); ?></span></li>
			<li><font color="#2E006C">Comportement sur le serveur : </font><br/><span><?php echo $bug['desc_serv']; ?></span></li>
			<li><font color="#2E006C">Comportement normal : </font><br/><span><?php echo $bug['desc_offi']; ?></span></li>
			<li id='listbug'><font color="#2E006C">Liens WowHead : </font><a href="<?php echo $bug['lienwowhead']; ?>" target="_blank"><?php echo $bug['lienwowhead']; ?></a></li>
			<?php
			if ($bug['state'] == 5)
			{
				?>
				<li><font color="#2E006C">Date de fermeture : </font><span>le <?php echo date("d-m-Y � H:i:s", $bug['lastdate']); ?></span></li>
				<?php
			}
			if ($bug['idfix'] > 0)
			{
				?>
				<li><font color="#2E006C">D�veloppeur  : </font><span><?php echo $site->get_name_by_id($bug['idfix']); ?></span></li>
				<?php
			}
			?>
		</div>
	</div>
    <div style="position:relative;left:495px;top:3px;">
        <a href="#" data-rel="popup_name" class="poplight" data-width="500"><button type='submit' id="button">Ajouter un commentaire</button></a>
	</div>
</div>
<br />
<br />
<div id="commentnews">Commentaires</div>
<?php
if ($nbrComments > 0)
{
	?>
	<div class="cadre">
		<select name="limit" onchange="commentRequestByBugId(<?php echo $bugid ?>, this.value, <?php echo $user->isLogged() ?>);">
			<?php
			for ($i = 0; $i < $nbrPages; $i++)
			{
				$index = $i + 1;
				?>
				<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 3) + 1) ?> - <?php echo (($i + 1) * 3) ?></option>
				<?php
			}
			?>
		</select>
		<br />
		<br />
		<span id="block-comment"></span>
	</div>
	<?php
}

if ($nbrComments <= 0)
{
    echo '
	<div class="cadre">
		<div class="newscontent">
			<div id="messagecomment1">
				Soyez le premier � ajouter un commentaire !
			</div>
		</div>
    </div>
	';
}
echo '
<div id="popup_name" class="popup">
    <form method="post" action="bug-'.$bugid.'" id="general">
	    <div id="messagecomment3">
            <label for="content">Contenu du message: </label><br />
            <textarea cols="55" rows="10" name="content" id="comment"></textarea><br />
		</div>
	    <div id="messagecomment4">
	        <input type="submit" name="ajouter" value="ajouter" />
		</div>
	</form>
</div>
';
?>

<script type="text/JavaScript" src="js/modale.js"></script>
<script type="text/JavaScript" src="js/bugs.js"></script>
<script type="text/javascript">
	commentRequestByBugId(<?php echo $bugid ?>, 1, <?php echo $user->isLogged() ?>);
</script>
