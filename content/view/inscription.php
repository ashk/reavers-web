<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Inscription au serveur de jeu</center></div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class='cadre2'>
	<div class='newscontent'>
		<?php
		if(!empty($_POST['inscription']))
		{
			/* on fait les v�rifications d'usages */
			if (!empty($inscription))
			{
				echo "<font color='#FF0000'>";
				if(!empty($inscription['empty']))
				{
					echo "- Champs vides.<br>";
				}
				if (!empty($inscription['pass_tooshort']))
				{
					echo "- Mot de passe trop court (min. 6 caract�res).<br>";
				}
				if (!empty($inscription['pass_toolong']))
				{
					echo "- Mot de passe trop long (max. 32 caract�res).<br>";
				}
				if (!empty($inscription['pass_nofigure']))
				{
					echo "- Votre mot de passe doit contenir au moins un chiffre.<br>";
				}
				if (!empty($inscription['pass_noupcap']))
				{
					echo "- Votre mot de passe doit contenir au moins une majuscule.<br>";
				}
				if (!empty($inscription['pass_format']))
				{
					echo "- Mot de passe non conforme ([a-z][A-Z][0-9]).<br>";
				}
				if (!empty($inscription['pass_noconf']))
				{
					echo "- Votre mot de passe et votre confirmation diff�rent.<br>";
				}

				// doit avoir la forme d'un mail !
				if(!empty($inscription['mail_format']))
				{
					echo "- Adresse mail non conforme.<br>";
				}

				// Mail et confirmation diff�rent
				if(!empty($inscription['mail_noconf']))
				{
					echo "- Votre mail et votre confirmation diff�rent.<br>";
				}
				if(!empty($inscription['login_tooshort']))
				{
					echo "- Pseudo trop court (min. 4 caract�res).<br>";
				}
				if(!empty($inscription['login_toolong']))
				{
					echo "- Pseudo trop long (max. 32 caract�res).<br>";
				}
				if(!empty($inscription['login_format']))
				{
					echo "- Pseudo non conforme ([a-z][A-Z][0-9]).<br>";
				}
				if(!empty($inscription['login_exist']))
				{
					echo "- Pseudo d�j� utilis�.<br>";
				}

				if(!empty($inscription['mail_exist']))
				{
					echo "- Adresse mail d�j� utilis�.<br>";
				}

                // Vérification captcha

                if(!empty($inscription['error_capacha'])) {
                    echo "- Erreur captcha";
                }

				echo "</font>";

			}
			else
			{
				echo "
				<p>
					Votre inscription a �t� effectu�e avec succ�s ! Vous pouvez d�sormais vous connecter et utiliser nos services.
				</p>";
			}
		}
		else
		{
			?>
			<form id='myForm' method='post' action='inscription'>
                <br />
                <br />
                <label class='form_col' for='login'>Pseudo :</label>
                <input type='text' name='login' id='login' tabindex='40' value='' size='30' maxlength='64'/>
                <span class='tooltip' id='pseudo1'>Pseudo trop court (min 6 caract�res)</span>
                <span class='tooltip' id='pseudo2'>Pseudo trop long (max 32 caract�res)</span>
                <span class='tooltip' id='pseudo3'>Pseudo non conforme ([a-z][A-Z][0-9])</span>
                <br />
                <label class='form_col' for='pass'>Mot de passe :</label>
                <input type='password' name='pass' id='pass' tabindex='40' value='' size='30' maxlength='64'/>
                <span class='tooltip' id='pass1'>Mot de passe trop court (min 6 caract�res)</span>
                <span class='tooltip' id='pass2'>Mot de passe trop long (max 32 caract�res)</span>
                <span class='tooltip' id='pass3'>Mot de passe non conforme ([a-z][A-Z][0-9])</span>
                <br />
                <label class='form_col' for='passconf'>Confirmez le mot de passe :</label>
                <input type='password' name='passconf' id='passconf' tabindex='40' value='' size='30' maxlength='64'/>
                <span class='tooltip' id='passconf1'>Le mot de passe de confirmation doit �tre identique � celui d'origine</span>
                <br />
                <label class='form_col' for='mail'>Email :</label>
                <input type='text' name='mail' id='mail' tabindex='40' value='' size='30' maxlength='64'/>
                <span class='tooltip' id='email1'>Adresse mail non conforme</span>
                <br />
                <label class='form_col' for='mailconf'>Confirmez votre email :</label>
                <input type='text' name='mailconf' id='mailconf' tabindex='40' value='' size='30' maxlength='64'/>
                <span class='tooltip' id='emailconf1'>Le mail de confirmation doit �tre identique � celui d'origine</span>
                <br/><br/>
                <div class="g-recaptcha" data-sitekey="6LcMpQITAAAAAOeadQ2_0Omu5RlIi1oQgcZAz9cF" style="margin-left: 25%"></div>
                <br/><br/>
                <input type='submit' name="inscription" value='Valider'> <input type='reset' />
			</form>
			<?php
		}
		?>
	</div>
</div>
<script type="text/JavaScript" src="js/inscription.js"></script>
