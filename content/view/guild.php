<?php
if (!empty($guild))
{
	echo '
	<div id="chemin">
		<a href="home">Accueil</a> > <a href="armurerie">Armurerie</a> > '.$guild['name'].'
	</div>
	<div class="cadre2">
		<div class="newscontent5">
			<div style="font-size:35px;font-weight:bold;color:#960018;">
				'.$guild['name'].'
			</div>
			<div style="float:left;">
				';
				echo '
				<font color="darkred">Maitre de guilde:</font> <a href="armurerie-1-'.$guild['leaderguid'].'">'.$royaume->get_name_by_guid($guild['leaderguid']).'</a>
				';
				echo '
			</div>
			<br />
			<div style="float:left;">
				<font color="darkred">Date de cr�ation:</font> '.date('j-m-Y',$guild['createdate']).'
			</div>
			<div style="float:right;">
				';
				echo count($guildmembers).' Membres
			</div>
		</div>
	</div>
	<div id="commentnews">Membres</div>
	<div class="cadre2">
		<div class="newscontent5">
			<div style="width:605px; margin-left:15px; margin-top:10px;">
				<table border="1" width="100%">
					<tr>
						<td align="center"><div style="font-weight:bold;">Nom du personnage</div></td>
						<td align="center"><div style="font-weight:bold;">Niveau</div></td>
						<td align="center"><div style="font-weight:bold;">Rang</div></td>
						<td align="center"><div style="font-weight:bold;">Race</div></td>
						<td align="center"><div style="font-weight:bold;">Classe</div></td>
						<td align="center"><div style="font-weight:bold;">Etat</div></td>
					</tr>
					';
					if (!empty($guildmembers))
					{
						foreach ($guildmembers as $guildmember)
						{
							echo '
							<tr>
								<td>
									<a href="personnage-'.$guildmember['guid'].'">
										'.$guildmember['name'].'
									</a>
								</td>
								<td align="center"><font color="darkred">'.$guildmember['level'].'</font></td>
								<td align="center">'.$royaume->getnamerank_by_rankid($guildid, $guildmember['rank']).'</td>
								<td align="center"><img src="img/icons/race/'.$guildmember['race'].'-'.$guildmember['gender'].'.gif"/></td>
								<td align="center"><img src="img/icons/class/'.$guildmember['class'].'.gif"/></td>
								<td align="center">
									';
									if ($guildmember['online'])
									{
										echo '
										<div style="color:#34C924;">
											En ligne
										</div>
										';
									}
									else
									{
										echo '
										<div style="color:#ED0000;">
											Hors ligne
										</div>
										';
									}
									echo '
								</td>
							</tr>
							';
						}
					}
					else
					{
						echo '
						<tr align="center">
							<td colspan="6">
								<strong><font color="red">Il n\'y a pas de personnages !</font></strong>
							</td>
						</tr>
						';
					}
					echo '
				</table>
			</div>
		</div>
	</div>
	';
}
?>
