<div id='chemin'>
	<a href='home'>Accueil</a> > Boutique
</div>
<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Boutique</center></div>
<?php
if (!empty($user->sess_id))
{
	?>
	<div class='cadre2'>
		<br />
        <p>La boutique sur le site est en construction, la boutique est disponible en Jeu</p>
		<!--<table class="table" border="0" cellspacing="0" cellpadding="3" width="100%">
			<tr class="row2">
				<td class="iconCol"><a href="boutique-1" class="sort-link"><input type="image" src="img/boutique/1.png"/><center>Armes</center></a></td>
				<td class="iconCol"><a href="boutique-2" class="sort-link"><input type="image" src="img/boutique/2.png"/><center>Armures</center></a></td>
				<td class="iconCol"><a href="boutique-3" class="sort-link"><input type="image" src="img/boutique/3.png"/><center>Bijoux</center></a></td>
				<td class="iconCol"><a href="boutique-4" class="sort-link"><input type="image" src="img/boutique/4.png"/><center>Boucliers</center></a></td>
				<td class="iconCol"><a href="boutique-5" class="sort-link"><input type="image" src="img/boutique/5.png"/><center>Compagnons</center></a></td>
			</tr>
			<tr class="row2">
				<td class="iconCol"><a href="boutique-6" class="sort-link"><input type="image" src="img/boutique/6.png"/><center>Compos</center></a></td>
				<td class="iconCol"><a href="boutique-7" class="sort-link"><input type="image" src="img/boutique/7.png"/><center>Monnaies</center></a></td>
				<td class="iconCol"><a href="boutique-8" class="sort-link"><input type="image" src="img/boutique/8.png"/><center>H�ritage</center></a></td>
				<td class="iconCol"><a href="boutique-9" class="sort-link"><input type="image" src="img/boutique/9.png"/><center>Montures</center></a></td>
				<td class="iconCol"><a href="boutique-10" class="sort-link"><input type="image" src="img/boutique/10.png"/><center>Sacs</center></a></td>
			</tr>
		</table>
		<br />-->
	</div>
	<?php
	if (!empty($_GET['variable1']) && is_numeric($_GET['variable1']))
	{
		echo '
		<div style="width:660px; margin-left:30px; margin-top:30px;">
			<table border="0" cellspacing="0" cellpadding="3" width="100%">
				<tr>
					<td width="470px">Nom de l\'objet</td>
					<td width="100px">Prix</td>
					<td width="90px" align="center">Acheter</td>
				</tr>
				';
				if (!empty($objet_array))
				{
					foreach($objet_array as $objet)
					{																			
						echo '
						<tr class="row2">
							<td width="470px"><a href="http://fr.wowhead.com/item='.$objet['id'].'"><font color="darkgreen"><b>'.utf8_decode($objet['nom']).'</font></a></td>
							<td width="100px"><font color="darkred">'.$objet['prix'].' points</font></td>
							<td width="90px" align="center"><a href="boutique-'.$_GET['variable1'].'-'.$objet['id'].'"><img src="img/admin/acheter.png"/></a></td>
						</tr>
						';
					}
				}
				else
				{
					echo '
					<tr class="row2" align="center">
						<td colspan="3">
							<strong><font color="red">Il n\'y a pas d\'objets en vente dans cette cat�gorie !</font></strong>
						</td>
					</tr>
					';
				}
				echo '	
			</table>
		</div>
		';
		if (!$user->is_banned_by_ip() && !$user->is_banned_by_account() && empty($user->array_account['locked']))
		{
			if (!empty($_GET['variable2']) && is_numeric($_GET['variable2']))
			{
				if (empty($_POST['character']))
				{
					if (!empty($characters))
					{
						echo '
						<div style="margin-left:30px; margin-top:30px;">
							<form method="post" action="boutique-'.$_GET['variable1'].'-'.$_GET['variable2'].'">
								<label>
									Choisissez votre personnage
									<select name="character">
										<optgroup label="Mes personnages">
										';
										foreach($characters as $character)
										{
											echo '
											<option value="'.$character['guid'].'">'.$character['name'].'</option>
											';
										}
										echo '
									</select>
								</label>
								';
								?>
								<input type="submit" value="Valider" onclick="return(confirm('Etes-vous s�r de vouloir acheter cet objet?'));">
							</form>
						</div>
						<?php
					}
					else
					{
						echo '
						<div align="center" style="margin-top:30px;">
							<strong><font color="red">Aucuns personnages !</font></strong>
						</div>';
					}
				}
				else
				{
					if ($result == 1)
					{
						echo '
						<div align="center" style="margin-top:30px;">
							<strong><font color="green">L\'objet achet� a �t� envoy� dans votre boite aux lettres.<br />Bon jeu sur '.$site->nom_site.' !</font></strong>
						</div>';
					}
					else if ($result == -1)
					{
						echo '
						<div align="center" style="margin-top:30px;">
							<strong><font color="red">Vous n\'avez pas assez de points pour acheter cet objet !</font></strong>
						</div>';
					}
				}
			}
		}
	}
}
else
{
	?>
	<div class='cadre2'>
		<br />
		<div style="color:#FF0000;">
			<center>Vous devez �tre connect� pour acc�der � cette page !</center>
		</div>
		<br />
	</div>
	<?php
}
?>