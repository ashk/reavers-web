<?php
if (!empty($row_characters_guid))
{
	echo '
	<div id="chemin">
		<a href="home">Accueil</a> > <a href="armurerie">Armurerie</a> > '.$row_characters_guid['name'].'
	</div>
	<div class="cadre5">
		<div class="newscontent2">
			<div style="font-size:35px;font-weight:bold;color:#960018;">
				'.$row_characters_guid['name'].'
			</div>
			<div class="rank">
				';
				if (!empty($rankname))
				{
					echo '
					<a href="armurerie-2-'.$guild['guildid'].'">< '.$rankname.' de <b>'.$guild['name'].'</b> ></a>
					';
				}
				else
				{
					echo '
						< Aucune guilde >
					';
				}
				echo '
			</div>
			';
			if ($royaume->isonline_by_character($guid))
			{
				echo '
				<div style="float:right;color:#34C924;">
					En ligne
				</div>
				';
			}
			else
			{
				echo '
				<div style="float:right;color:#ED0000;">
					Hors ligne
				</div>
				';
			}
			echo '
			<br />
			<div style="float:left;font-weight:bold;">
				'.$royaume->getracestringfr($row_characters_guid['race']).' '.$royaume->getclassstringfr($row_characters_guid['class']).' de niveau <strong>'.$row_characters_guid['level'].'</strong>
			</div>
			<div style="float:right;font-weight:bold;">
				Honneur : '.$row_characters_guid['totalHonorPoints'].' , Points d\'ar�ne : '.$row_characters_guid['arenaPoints'].' , Tu�s : '.$row_characters_guid['totalKills'].'
			</div>
			<br />
			<div style="float:left;font-weight:bold;">
				'.$or.'<img src="img/icons/money/or.gif"> '.$argent.'<img src="img/icons/money/argent.gif"> '.$cuivre.'<img src="img/icons/money/cuivre.gif">
			</div>
			<div style="float:right;font-weight:bold;">
				Temps de jeu : '.$jours.' Jours, '.$heures.' Heures, '.$minutes.' Minutes
			</div>
		</div>
	</div>
	<div id="summary-inventory" class="summary-inventory summary-inventory-simple">
		<div class="summary-health-resource">
			<ul>
				<li class="health" id="summary-health"><span class="name">Sant&eacute;</span><span class="value">'.$row_characters_guid['health'].'/'.round($row_stats['maxhealth'], 2).'</span></li>';
				if($row_characters_guid['class'] == 1)
				{
					echo '<li class="resource-1" id="summary-power"><span class="name">Rage</span><span class="value">'.$row_characters_guid['power2'].'/'.round($row_stats['maxpower2'], 2).'</span></li>';
				}
				if($row_characters_guid['class'] == 3)
				{
					echo '<li class="resource-1" id="summary-power"><span class="name">Rage</span><span class="value">'.$row_characters_guid['power3'].'/'.round($row_stats['maxpower3'], 2).'</span></li>';
				}
				elseif($row_characters_guid['class'] == 4)
				{
					echo '<li class="resource-3" id="summary-power"><span class="name">&Eacute;nergie</span><span class="value">'.$row_characters_guid['power4'].'/'.round($row_stats['maxpower4'], 2).'</span></li>';
				}
				elseif($row_characters_guid['class'] == 6)
				{
					echo '<li class="resource-6" id="summary-power"><span class="name">Runique</span><span class="value">'.$row_characters_guid['power7'].'/'.round($row_stats['maxpower7'], 2).'</span></li>';
				}
				elseif(($row_characters_guid['class'] == 2) ||($row_characters_guid['class'] == 5) || ($row_characters_guid['class'] == 7) || ($row_characters_guid['class'] == 8) || ($row_characters_guid['class'] == 9) || ($row_characters_guid['class'] == 11))
				{
					echo '<li class="resource-0" id="summary-power"><span class="name">Mana</span><span class="value">'.$row_characters_guid['power1'].'/'.round($row_stats['maxpower1'], 2).'</span></li>';
				}
				echo '
			</ul>
		</div>
		<div style="position:relative; left:80px; top:-10px;">
			<object data="http://static.wowhead.com/modelviewer/ModelView.swf" width="290px" height="380px" type="application/x-shockwave-flash">
				<param name="quality" value="high"> 
				<param name="allowscriptaccess" value="always">  
				<param name="allowfullscreen" value="false"> 
				<param name="menu" value="false">
				<param name="wmode" value="transparent"> 
				<param name="flashvars" value="model='.$royaume->getracestring($row_characters_guid['race']).$royaume->getgenderstring($row_characters_guid['gender']).'&amp;modelType=16&amp;contentPath=http://static.wowhead.com/modelviewer/&amp;blur=1&amp;equipList='.$character_model.'">
				<param name="movie" value="http://static.wowhead.com/modelviewer/ModelView.swf">
			</object>
		</div>
		';
		if(!empty($row_item[0]))
		{
			echo '
			<div class="slot slot-1 item-quality-'.$row_item[0]['Quality'].'" style=" left: 0px; top: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[0]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[0]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-1" style=" left: 0px; top: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[1]))
		{
			echo '
			<div class="slot slot-2 item-quality-'.$row_item[1]['Quality'].'" style=" left: 0px; top: 58px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[1]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[1]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-2" style=" left: 0px; top: 58px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[2]))
		{
			echo '
			<div class="slot slot-3 item-quality-'.$row_item[2]['Quality'].'" style=" left: 0px; top: 116px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[2]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[2]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-3" style=" left: 0px; top: 116px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[14]))
		{
			echo '
			<div class="slot slot-16 item-quality-'.$row_item[14]['Quality'].'" style=" left: 0px; top: 174px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[14]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[14]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-16" style=" left: 0px; top: 174px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[4]))
		{
			echo '
			<div class="slot slot-5 item-quality-'.$row_item[4]['Quality'].'" style=" left: 0px; top: 232px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[4]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[4]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-5" style=" left: 0px; top: 232px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[3]))
		{
			echo '
			<div class="slot slot-4 item-quality-'.$row_item[3]['Quality'].'" style=" left: 0px; top: 290px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[3]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[3]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-4" style=" left: 0px; top: 290px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[18]))
		{
			echo '
			<div class="slot slot-19 item-quality-'.$row_item[18]['Quality'].'" style=" left: 0px; top: 348px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[18]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[18]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-19" style=" left: 0px; top: 348px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[8]))
		{
			echo '
			<div class="slot slot-9 item-quality-'.$row_item[8]['Quality'].'" style=" left: 0px; top: 406px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[8]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[8]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-9" style=" left: 0px; top: 406px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[9]))
		{
			echo '
			<div class="slot slot-10 slot-align-right item-quality-'.$row_item[9]['Quality'].'" style=" right: 0px; top: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[9]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[9]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-10 slot-align-right" style=" right: 0px; top: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[5]))
		{
			echo '
			<div class="slot slot-6 slot-align-right item-quality-'.$row_item[5]['Quality'].'" style=" right: 0px; top: 58px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[5]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[5]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-6 slot-align-right" style=" right: 0px; top: 58px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[6]))
		{
			echo '
			<div class="slot slot-7 slot-align-right item-quality-'.$row_item[6]['Quality'].'" style=" right: 0px; top: 116px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[6]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[6]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-7 slot-align-right" style=" right: 0px; top: 116px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[7]))
		{
			echo '
			<div class="slot slot-8 slot-align-right item-quality-'.$row_item[7]['Quality'].'" style=" right: 0px; top: 174px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[7]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[7]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-8 slot-align-right" style=" right: 0px; top: 174px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[10]))
		{
			echo '
			<div class="slot slot-11 slot-align-right item-quality-'.$row_item[10]['Quality'].'" style=" right: 0px; top: 232px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[10]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[10]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-11 slot-align-right" style=" right: 0px; top: 232px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[11]))
		{
			echo '
			<div class="slot slot-11 slot-align-right item-quality-'.$$row_item[11]['Quality'].'" style=" right: 0px; top: 290px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$$row_item[11]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[11]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-11 slot-align-right" style=" right: 0px; top: 290px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[12]))
		{
			echo '
			<div class="slot slot-12 slot-align-right item-quality-'.$row_item[12]['Quality'].'" style=" right: 0px; top: 348px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[12]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[12]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-12 slot-align-right" style=" right: 0px; top: 348px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[13]))
		{
			echo '
			<div class="slot slot-12 slot-align-right item-quality-'.$row_item[13]['Quality'].'" style=" right: 0px; top: 406px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[13]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[13]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-12 slot-align-right" style=" right: 0px; top: 406px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[15]))
		{
			echo '
			<div class="slot slot-21 slot-align-right item-quality-'.$row_item[15]['Quality'].'" style=" left: 135px; bottom: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[15]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[15]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-21 slot-align-right" style=" left: 135px; bottom: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[16]))
		{
			echo '
			<div class="slot slot-22 item-quality-'.$row_item[16]['Quality'].'" style=" left: 200px; bottom: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[16]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[16]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-22" style=" left: 200px; bottom: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		if(!empty($row_item[17]))
		{
			echo '
			<div class="slot slot-15 item-quality-'.$row_item[17]['Quality'].'" style=" left: 265px; bottom: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="http://fr.wowhead.com/item='.$row_item[17]['itemEntry'].'" class="item"><img src="http://wow.zamimg.com/images/wow/icons/large/'.$row_icon[17]['icon'].'.jpg" alt="" /><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		else
		{
			echo '	
			<div class="slot slot-15" style=" left: 265px; bottom: 0px;">
				<div class="slot-inner">
					<div class="slot-contents">
						<a href="javascript:;" class="empty"><span class="frame"></span></a>
					</div>
				</div>
			</div>';
		}
		echo '
	</div>
	<div class="summary-stats" id="summary-stats">
		<div id="summary-stats-simple" class="summary-stats-simple">
			<div class="summary-stats-simple-base">
				<div class="summary-stats-column">
					<form method="post" action="">
						';
						?>
						<select name="carac1" onChange="afficher(this.value, 'col1')">
							<option value="1" selected="selected">Caract�ristiques</option>
							<option value="2">En m�l�e </option>
							<option value="3">A distance </option>
							<option value="4">Sortil�ges </option>
							<option value="5">D�fenses </option>     
						</select>   
					</form>
					<?php
					echo '
					<span id = "col1">
						<ul>
							<li><span>Force :</span> '.round($row_stats['strength'], 2).'</li>
							<li><span>Agilit� :</span> '.round($row_stats['agility'], 2).'</li>
							<li><span>Endurance :</span> '.round($row_stats['stamina'], 2).'</li>
							<li><span>Intelligence :</span> '.round($row_stats['intellect'], 2).'</li>
							<li><span>Esprit :</span> '.round($row_stats['spirit'], 2).'</li>
							<li><span>Armure :</span> '.round($row_stats['armor'], 2).'</li>
						</ul>
					</span>
				</div>
				<br />
			</div>
			<div class="summary-stats-simple-other">
				<div class="summary-stats-column">
					<form method="post" action="">
						';
						?>
						<select name="carac2" onChange="afficher(this.value, 'col2')">
							<option value="1">Caract�ristiques </option>
							<option value="2" selected="selected">En m�l�e </option>
							<option value="3">A distance </option>
							<option value="4">Sortil�ges </option>
							<option value="5">D�fenses </option>     
						</select>   
					</form>
					<?php
					echo '
					<span id = "col2">
						<ul>
							<li><span>Puissance :</span> '.round($row_stats['attackPower'], 2).'</li>
							<li><span>Critique :</span> '.round($row_stats['critPct'], 2).'%</li>
						</ul>
					</span>
				</div>
			</div>
			<div class="summary-stats-end"></div>
		</div>
	</div>
	<div style="font-size:25px; margin-top:100px; margin-left:300px;">
		Honneur
	</div>
	<div class="cadre6">
		<table width="100%" BORDER="0">
			<tr>
				<td align="center">
				</td>
				<td align="center">
					Aujourd\'hui
				</td>
				<td align="center">
					Hier
				</td>
				<td align="center">
					� vie
				</td>
			</tr>
			<tr>
				<td align="center">
					Tu�s
				</td>
				<td align="center">
					'.$row_characters_guid['todayKills'].'
				</td>
				<td align="center">
					'.$row_characters_guid['yesterdayKills'].'
				</td>
				<td align="center">
					'.$row_characters_guid['totalKills'].'
				</td>
			</tr>
			<tr>
				<td align="center">
					Honneur
				</td>
				<td align="center">
					'.$row_characters_guid['todayHonorPoints'].'
				</td>
				<td align="center">
					'.$row_characters_guid['yesterdayHonorPoints'].'
				</td>
				<td align="center">
					'.$row_characters_guid['totalHonorPoints'].'
				</td>
			</tr>
		</table>
	</div>
	<div style="font-size:30px; margin-top:30px; margin-left:305px;">
		Ar�nes
	</div>
	<div class="cadre6">
		';
		if (!empty($arenastat2c2))
		{
			echo '
			<div style="font-size:25px; float:left;">
				<div class="rank">
					<a href="armurerie-3-'.$arenateam2c2['arenaTeamId'].'">" '.$arenateam2c2['name'].' "</a>
				</div>
			</div>
			<div style="float:right;">
				Cote personnelle <font color="darkred">'.$arenastat2c2['personalRating'].'</font>
			</div>
			<table width="100%" BORDER="0">
				<tr>
					<td align="center">
					</td>
					<td align="center">
						Jou�s
					</td>
					<td align="center">
						Gagn�s - Perdus
					</td>
					<td align="center">
						% gagn�s
					</td>
				</tr>
				<tr>
					<td align="center">
						Semaine
					</td>
					<td align="center">
						'.$arenastat2c2['weekGames'].'
					</td>
					<td align="center">
						'.$arenastat2c2['weekWins'].' - '.$arenastat2c2['weekLoose'].'
					</td>
					<td align="center">
						'.$arenastat2c2['weekPercentWin'].'%
					</td>
				</tr>
				<tr>
					<td align="center">
						Saison
					</td>
					<td align="center">
						'.$arenastat2c2['seasonGames'].'
					</td>
					<td align="center">
						'.$arenastat2c2['seasonWins'].' - '.$arenastat2c2['seasonLoose'].'
					</td>
					<td align="center">
						'.$arenastat2c2['seasonPercentWin'].'%
					</td>
				</tr>
			</table>
			';
		}
		else
		{
			echo '
			<div style="font-size:35px; padding-top:35px; padding-left:135px;">
				2C2
			</div>
			';
		}
		echo '
	</div>
	<div class="cadre6">
		';
		if (!empty($arenastat3c3))
		{
			echo '
			<div style="font-size:25px; float:left;">
				<div class="rank">
					<a href="armurerie-3-'.$arenateam3c3['arenaTeamId'].'">" '.$arenateam3c3['name'].' "</a>
				</div>
			</div>
			<div style="float:right;">
				Cote personnelle <font color="darkred">'.$arenastat3c3['personalRating'].'</font>
			</div>
			<table width="100%" BORDER="0">
				<tr>
					<td align="center">
					</td>
					<td align="center">
						Jou�s
					</td>
					<td align="center">
						Gagn�s - Perdus
					</td>
					<td align="center">
						% gagn�s
					</td>
				</tr>
				<tr>
					<td align="center">
						Semaine
					</td>
					<td align="center">
						'.$arenastat3c3['weekGames'].'
					</td>
					<td align="center">
						'.$arenastat3c3['weekWins'].' - '.$arenastat3c3['weekLoose'].'
					</td>
					<td align="center">
						'.$arenastat3c3['weekPercentWin'].'%
					</td>
				</tr>
				<tr>
					<td align="center">
						Saison
					</td>
					<td align="center">
						'.$arenastat3c3['seasonGames'].'
					</td>
					<td align="center">
						'.$arenastat3c3['seasonWins'].' - '.$arenastat3c3['seasonLoose'].'
					</td>
					<td align="center">
						'.$arenastat3c3['seasonPercentWin'].'%
					</td>
				</tr>
			</table>
			';
		}
		else
		{
			echo '
			<div style="font-size:35px; padding-top:35px; padding-left:135px;">
				3C3
			</div>
			';
		}
		echo '
	</div>
	<div class="cadre6">
		';
		if (!empty($arenastat5c5))
		{
			echo '
			<div style="font-size:25px; float:left;">
				<div class="rank">
					<a href="armurerie-3-'.$arenateam5c5['arenaTeamId'].'">" '.$arenateam5c5['name'].' "</a>
				</div>
			</div>
			<div style="float:right;">
				Cote personnelle <font color="darkred">'.$arenastat5c5['personalRating'].'</font>
			</div>
			<table width="100%" BORDER="0">
				<tr>
					<td align="center">
					</td>
					<td align="center">
						Jou�s
					</td>
					<td align="center">
						Gagn�s - Perdus
					</td>
					<td align="center">
						% gagn�s
					</td>
				</tr>
				<tr>
					<td align="center">
						Semaine
					</td>
					<td align="center">
						'.$arenastat5c5['weekGames'].'
					</td>
					<td align="center">
						'.$arenastat5c5['weekWins'].' - '.$arenastat5c5['weekLoose'].'
					</td>
					<td align="center">
						'.$arenastat5c5['weekPercentWin'].'%
					</td>
				</tr>
				<tr>
					<td align="center">
						Saison
					</td>
					<td align="center">
						'.$arenastat5c5['seasonGames'].'
					</td>
					<td align="center">
						'.$arenastat5c5['seasonWins'].' - '.$arenastat5c5['seasonLoose'].'
					</td>
					<td align="center">
						'.$arenastat5c5['seasonPercentWin'].'%
					</td>
				</tr>
			</table>
			';
		}
		else
		{
			echo '
			<div style="font-size:35px; padding-top:35px; padding-left:135px;">
				5C5
			</div>
			';
		}
		echo '
	</div>
	';
}
?>
<script language="JavaScript" type="text/javascript">
function afficher(value, id)
{
	var ecrire = document.getElementById(id);
	<?php
	if (!empty($row_stats))
	{
		echo("strength = ".round($row_stats['strength'], 2).";");
		echo("agility = ".round($row_stats['agility'], 2).";");
		echo("stamina = ".round($row_stats['stamina'], 2).";");
		echo("intellect = ".round($row_stats['intellect'], 2).";");
		echo("spirit = ".round($row_stats['spirit'], 2).";");
		echo("armor = ".round($row_stats['armor'], 2).";");
		echo("attackPower = ".round($row_stats['attackPower'], 2).";");
		echo("critPct = ".round($row_stats['critPct'], 2).";");
		echo("rangedAttackPower = ".round($row_stats['rangedAttackPower'], 2).";");
		echo("rangedCritPct = ".round($row_stats['rangedCritPct'], 2).";");
		echo("spellPower = ".round($row_stats['spellPower'], 2).";");
		echo("spellCritPct = ".round($row_stats['spellCritPct'], 2).";");
		echo("resilience = ".round($row_stats['resilience'], 2).";");
		echo("dodgePct = ".round($row_stats['dodgePct'], 2).";");
		echo("parryPct = ".round($row_stats['parryPct'], 2).";");
		echo("blockPct = ".round($row_stats['blockPct'], 2).";");
	}
	else
	{
		echo("strength = 0;");
		echo("agility = 0;");
		echo("stamina = 0;");
		echo("intellect = 0;");
		echo("spirit = 0;");
		echo("armor = 0;");
		echo("attackPower = 0;");
		echo("critPct = 0;");
		echo("rangedAttackPower = 0;");
		echo("rangedCritPct = 0;");
		echo("spellPower = 0;");
		echo("spellCritPct = 0;");
		echo("resilience = 0;");
		echo("dodgePct = 0;");
		echo("parryPct = 0;");
		echo("blockPct = 0;");
	}
	?>
	var contenu1 = "<ul><li><span>Force :</span> " + strength + "</li><li><span>Agilit� :</span> " + agility + "</li><li><span>Endurance :</span> " + stamina + "</li><li><span>Intelligence :</span> " + intellect + "</li><li><span>Esprit :</span> " + spirit + "</li><li><span>Armure :</span> " + armor + "</li></ul>";
	var contenu2 = "<ul><li><span>Puissance :</span> " + attackPower + "</li><li><span>Critique :</span> " + critPct + "%</li></ul>";
	var contenu3 = "<ul><li><span>Puissance :</span> " + rangedAttackPower + "</li><li><span>Critique :</span> " + rangedCritPct + "%</li></ul>";
	var contenu4 = "<ul><li><span>Puissance :</span> " + spellPower + "</li><li><span>Critique :</span> " + spellCritPct + "%</li></ul>";
	var contenu5 = "<ul><li><span>Armure :</span> " + armor + "</li><li><span>R�silience :</span> " + resilience + "</li><li><span>Esquive :</span> " + dodgePct + "%</li><li><span>Parade :</span> " + parryPct + "%</li><li><span>Blocage :</span> " + blockPct + "%</li></ul>";
	switch (value) 
	{
		case "1": // Caract�ristiques
			ecrire.innerHTML = contenu1;
			break;
		case "2": // En m�l�e
			ecrire.innerHTML=contenu2;
			break;
		case "3": // A distance 
			ecrire.innerHTML=contenu3;
			break;
		case "4": // Sortil�ges
			ecrire.innerHTML=contenu4;
			break;
		case "5": // D�fenses
			ecrire.innerHTML=contenu5;
			break;
	}
}
</script>
