<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Classement</center></div>
<div class="cadre4">
	<div id='titlearenes'>Ar�nes</div>
	<div id='titlehonor'>Honneur</div>
	<div id="arenes">
		<div id='typearenes'>2c2</div>
		<div style='font-size:12px;'>
			<?php
			if (!empty($arenne2c2))
			{
				echo '
				<table width="100%" style="margin-left:1px;">
				';
				foreach($arenne2c2 as $team)
				{
					echo "
					<tr>
						<td align='left'><a href='arene-".$team['arenaTeamId']."'>".$team['name']."</a></td>
						<td align='right'>".$team['rating']."</td>
					</tr>
					";
				}
				?>	
				</table>
				<div style="margin-left:35px;"><a href="arenes-2">[ Voir la suite ]</a></div>
				<?php
			}
			?>
		</div>
	</div>
	<div id="arenes">
		<div id='typearenes'>3c3</div>
		<div style='font-size:12px;'>
			<?php
			if (!empty($arenne3c3))
			{
				echo '
				<table width="100%" style="margin-left:1px;">
				';
				foreach($arenne3c3 as $team)
				{
					echo "
					<tr>
						<td align='left'><a href='arene-".$team['arenaTeamId']."'>".$team['name']."</a></td>
						<td align='right'>".$team['rating']."</td>
					</tr>
					";
				}
				?>	
				</table>
				<div style="margin-left:35px;"><a href="arenes-3">[ Voir la suite ]</a></div>
				<?php
			}
			?>
		</div>
	</div>
	<div id="arenes">
		<div id='typearenes'>5c5</div>
		<div style='font-size:12px;'>
				<?php
			if (!empty($arenne5c5))
			{
				echo '
				<table width="100%" style="margin-left:1px;">
				';
				foreach($arenne5c5 as $team)
				{
					echo "
					<tr>
						<td align='left'><a href='arene-".$team['arenaTeamId']."'>".$team['name']."</a></td>
						<td align='right'>".$team['rating']."</td>
					</tr>
					";
				}
				?>	
				</table>
				<div style="margin-left:35px;"><a href="arenes-5">[ Voir la suite ]</a></div>
				<?php
			}
			?>
		</div>
	</div>
	<div id="honneur">
		<div style='margin-left:45px;'>Tu�s � vie</div>
		<div style='font-size:12px;'>
				<?php
			if (!empty($honneur))
			{
				echo '
				<table width="100%" style="margin-left:1px;">
				';
				foreach($honneur as $player)
				{
					echo "
					<tr>
						<td align='left'><a href='personnage-".$player['guid']."'>".$player['name']."</a></td>
						<td align='right'>".$player['totalKills']."</td>
					</tr>
					";
				}
				?>	
				</table>
				<div style="margin-left:45px;"><a href="honneur">[ Voir la suite ]</a></div>
				<?php
			}
			?>
		</div>
	</div>
</div>
<div style='width:650px;margin-left:15px;margin-bottom:5px;margin-top:15px;font-size:40px;'><center>Actualit�s</center></div>
<div class="cadre">
	<?php
	if (!empty($news))
	{
		echo '<br />';
		foreach($news as $new)
		{
			echo'
			<div class="newscontent">
				<div id="newspics">
					<h1>'.$new['title'].'</h1>
				</div>
				<div id="newstext">
					'.$new['content'].'
				</div>
				<div id="newsdate">
					';
					if (!empty($user->sess_id))
					{
						echo '
						Publi� par <a href="profil-'.$new['publisher'].'">'.$site->get_name_by_id($new['publisher']).'</a> le '.$new['date'].' � '.$new['heure'].'
						';
					}
					else
					{
						echo '
						Publi� par '.$new['publisherName'].' le '.$new['date'].' � '.$new['heure'].'
						';
					}
					echo '
				</div>
				';
				if (!empty($user->sess_id))
				{
					echo '
					<div id="newscom">
						<a href="news-'.$new['id'].'">Voir les commentaires('.$new['comment'].')</a>
					</div>
					';
				}
				else
				{
					echo '
					<div id="newscom">
						Impossible de voir les commentaires!
					</div>
					';
				}
				echo '
			</div>
			<br />
			';
		}
		?>
		<div style="margin-left:520px;">
			<a href='newsall'>Toutes les news...</a>
		</div>
		<?php
	}
	else
	{
		?>
		<div style='margin-top:60px;font-size:30px;'>
			<center>Il n'y a aucune news!</center>
		</div>
		<?php
	}
?>
</div>
