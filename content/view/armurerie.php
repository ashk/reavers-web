<div id='chemin'>
	<a href='home'>Accueil</a> > Armurerie
</div>
<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Armurerie</center></div>
<div class='cadre2'>
	<div class='newscontent'>
		<form method="post" id="general" action="">
			<?php
			if ($choix == 1)
			{
				?>
				<select name="choix" onchange="this.form.submit();">
					<option value="1" selected="selected">Nom du personnage</option>
					<option value="2">Nom de la guilde</option>
					<option value="3">Nom de l'equipe d'arenes</option>
				</select>
				<br />
				<br />
				<label class='form_col' for="content">Nom du personnage: </label>
				<input type="text" name="name" id="name" size="30"/>
				<input type='submit' value='Rechercher'>
				<?php
			}
			else if ($choix == 2)
			{
				?>
				<select name="choix" onchange="this.form.submit();">
					<option value="1">Nom du personnage</option>
					<option value="2" selected="selected">Nom de la guilde</option>
					<option value="3">Nom de l'equipe d'arenes</option>
				</select>
				<br />
				<br />
				<label class='form_col' for="content">Nom de la guilde: </label>
				<input type="text" name="name" id="name" size="30"/>
				<input type='submit' value='Rechercher'>
				<?php
			}
			else if ($choix == 3)
			{
				?>
				<select name="choix" onchange="this.form.submit();">
					<option value="1">Nom du personnage</option>
					<option value="2">Nom de la guilde</option>
					<option value="3" selected="selected">Nom de l'equipe d'arenes</option>
				</select>
				<br />
				<br />
				<label class='form_col' for="content">Nom de l'equipe d'arenes </label>
				<input type="text" name="name" id="name" size="30"/>
				<input type='submit' value='Rechercher'>
				<br />
				<br />
				<div style="float:left;">
					<?php
					if ($type == '2c2')
					{
						echo '
						<label>
							Type
							<select name="type" onchange="this.form.submit();">
								<option value="2" selected="selected">2C2</option>
								<option value="3" >3C3</option>
								<option value="5" >5C5</option>
							</select>
						</label>
						';
					}
					else if ($type == '3c3')
					{
						echo '
						<label>
							Type
							<select name="type" onchange="this.form.submit();">
								<option value="2" >2C2</option>
								<option value="3" selected="selected">3C3</option>
								<option value="5" >5C5</option>
							</select>
						</label>
						';
					}
					else if ($type == '5c5')
					{
						echo '
						<label>
							Type
							<select name="type" onchange="this.form.submit();">
								<option value="2" >2C2</option>
								<option value="3" >3C3</option>
								<option value="5" selected="selected">5C5</option>
							</select>
						</label>
						';
					}
					?>
				</div>
				<?php
			}
			?>
		</form>
		<br />
		<div id='spacewhite2'></div>
		<?php
		if ($choix == 1)
		{
			if ((!isset($_POST['name']) || empty($_POST['name'])) && $nbrPersos > 0)
			{
				?>
				<span id='select_perso'></span>
				<select name="limit" onchange="personnageRequest(this.value);">
					<?php
					for ($i = 0; $i < $nbrPages; $i++)
					{
						$index = $i + 1;
						?>
						<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 20) + 1) ?> - <?php echo (($i + 1) * 20) ?></option>
						<?php
					}
					?>
				</select>
				<br />
				<?php
			}

			?>
			<table border='1' width="100%">
				<tr>
					<td align='center'><div style='font-weight:bold;'>Nom du personnage</div></td>
					<td align='center'><div style='font-weight:bold;'>Niveau</div></td>
					<td align='center'><div style='font-weight:bold;'>Race</div></td>
					<td align='center'><div style='font-weight:bold;'>Classe</div></td>
					<td align='center'><div style='font-weight:bold;'>Etat</div></td>
				</tr>
				<?php
				if ($nbrPersos > 0 && (!isset($_POST['name']) || empty($_POST['name'])))
				{
					?>
                    
					<tr id="tablepersos" ></tr></table>
					<?php
				}
				else if ($nbrPersos > 0 && isset($_POST['name']) && !empty($_POST['name']))
				{
                        foreach ($players as $player)
                        {
                            echo '
                            <tr>
                                <td>
                                    <a href="personnage-'.$player['guid'].'">
                                        '.$player['name'].'
                                    </a>
                                </td>
                                <td align="center"><font color="darkred">'.$player['level'].'</font></td>
                                <td align="center"><img src="img/icons/race/'.$player['race'].'-'.$player['gender'].'.gif"/></td>
                                <td align="center"><img src="img/icons/class/'.$player['class'].'.gif"/></td>
                                <td align="center">
                                    ';
                                    if ($character['online'])
                                    {
                                        echo '
                                        <div style="color:#34C924;">
                                            En ligne
                                        </div>
                                        ';
                                    }
                                    else
                                    {
                                        echo '
                                        <div style="color:#ED0000;">
                                            Hors ligne
                                        </div>
                                        ';
                                    }
                                    echo '
                                </td>
                            </tr>
                            ';
                        }
                        echo '
                    </table>
                    ';
				}
				else
				{
					echo '
					<tr align="center">
						<td colspan="5">
							<strong><font color="red">Il n\'y a pas de personnages !</font></strong>
						</td>
					</tr>
					';
				}
				echo '
			</table>
			';
			if (!isset($_POST['name']) || empty($_POST['name']))
			{
				?>
				<script type="text/JavaScript" src="js/armurerie.js"></script>
				<script type="text/javascript">
					personnageRequest(1);
				</script>
				<?php
			}
		}
		else if ($choix == 2)
		{
			if ((!isset($_POST['name']) || empty($_POST['name'])) && $nbrGuilds > 0)
			{
				?>
				<span id='select_perso'></span>
				<select name="limit" onchange="guildRequest(this.value);">
					<?php
					for ($i = 0; $i < $nbrPages; $i++)
					{
						$index = $i + 1;
						?>
						<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 20) + 1) ?> - <?php echo (($i + 1) * 20) ?></option>
						<?php
					}
					?>
				</select>
				<br />
				<?php
			}

			?>
			<table border='1' width="100%">
				<tr>
					<td align='center'><div style='font-weight:bold;'>Nom de la guilde</div></td>
					<td align='center'><div style='font-weight:bold;'>Maitre de guilde</div></td>
					<td align='center'><div style='font-weight:bold;'>Date de cr�ation</div></td>
				</tr>
				<?php
				if ($nbrGuilds > 0 && (!isset($_POST['name']) || empty($_POST['name'])))
				{
					?>
					<tr id="tableguilds"></tr>
					<?php
				}
				else if ($nbrGuilds > 0 && isset($_POST['name']) && !empty($_POST['name']))
				{
					foreach ($guilds as $guild)
					{
						echo '
						<tr>
							<td>
								<a href="guild-'.$guild['guildid'].'">
										'.$guild['name'].'
								</a>
							</td>
							<td align="center"><a href="personnage-'.$guild['leaderguid'].'">'.$royaume->get_name_by_guid($guild['leaderguid']).'</a></td>
							<td align="center">'.date('j-m-Y',$guild['createdate']).'</td>
						</tr>
						';
					}
				}
				else
				{
					echo '
					<tr align="center">
						<td colspan="3">
							<strong><font color="red">Il n\'y a pas de guildes !</font></strong>
						</td>
					</tr>
					';
				}
				echo '
			</table>
			';
			if (!isset($_POST['name']) || empty($_POST['name']))
			{
				?>
				<script type="text/JavaScript" src="js/armurerie.js"></script>
				<script type="text/javascript">
					guildRequest(1);
				</script>
				<?php
			}
		}
		else if ($choix == 3)
		{
			if ((!isset($_POST['name']) || empty($_POST['name'])) && $nbrTeams > 0)
			{
				?>
				<span id='select_perso'></span>
				<select name="limit" onchange="teamRequest(this.value, <?php echo $type ?>);">
					<?php
					for ($i = 0; $i < $nbrPages; $i++)
					{
						$index = $i + 1;
						?>
						<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 20) + 1) ?> - <?php echo (($i + 1) * 20) ?></option>
						<?php
					}
					?>
				</select>
				<br />
				<?php
			}

			?>
			<table border='1' width="100%">
				<tr>
					<td align='center'><div style='font-weight:bold;'>Nom de l'equipe</div></td>
					<td align='center'><div style='font-weight:bold;'>Capitaine</div></td>
					<td align='center'><div style='font-weight:bold;'>Cote</div></td>
				</tr>
				<?php
				if ($nbrTeams > 0 && (!isset($_POST['name']) || empty($_POST['name'])))
				{
					?>
					<tr id="tableteams"></tr>
					<?php
				}
				else if ($nbrTeams > 0 && isset($_POST['name']) && !empty($_POST['name']))
				{
					foreach ($arenes as $arene)
					{
						echo '
						<tr>
							<td>
								<a href="arene-'.$arene['arenaTeamId'].'">
										'.$arene['name'].'
								</a>
							</td>
							<td align="center"><a href="personnage-'.$arene['captainGuid'].'">'.$royaume->get_name_by_guid($arene['captainGuid']).'</a></td>
							<td align="center">'.$arene['rating'].'</td>
						</tr>
						';
					}
				}
				else
				{
					echo '
					<tr align="center">
						<td colspan="3">
							<strong><font color="red">Il n\'y a pas d\'equipes !</font></strong>
						</td>
					</tr>
					';
				}
				echo '
			</table>
			';
			if (!isset($_POST['name']) || empty($_POST['name']))
			{
				?>
				<script type="text/JavaScript" src="js/armurerie.js"></script>
				<script type="text/javascript">
					teamRequest(1, <?php echo $type ?>);
				</script>
				<?php
			}
		}
		echo '
	</div>
</div>
';
?>

<script type="text/JavaScript" src="js/general.js"></script>