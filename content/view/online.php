<div id='chemin'>
	<a href='home'>Accueil</a> > online
</div>
<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Joueurs en ligne</center></div>
<div class='cadre2'>
	<?php
	if ($nbrOnline > 0)
	{
		?>
		<select name="limit" onchange="onlineRequest(this.value);">
			<?php
			for ($i = 0; $i < $nbrPages; $i++)
			{
				$index = $i + 1;
				?>
				<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 20) + 1) ?> - <?php echo (($i + 1) * 20) ?></option>
				<?php
			}
			?>
		</select>
		<br />
		<?php
	}
	?>
	<div class='newscontent'>
		<table width="100%">
			<tr>
				<td>Nom</td>
				<td align="center">Niveau</td>
				<td align="center">Race</td>
				<td align="center">Classe</td>
				<td align="center">Zone</td>
			</tr>
			<?php
			if ($nbrOnline > 0)
			{
				?>
				<tr id="tableonline"></tr>
				<?php
			}
			else
			{
				echo '
				<tr align="center">
					<td colspan="5">
						<strong><font color="red">Aucun joueur en ligne !</font></strong>
					</td>
				</tr>
				';
			}
			?>
		</table>
	</div>
</div>

<script type="text/JavaScript" src="js/online.js"></script>
<script type="text/javascript">
	onlineRequest(1);
</script>