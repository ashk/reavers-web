<div id='chemin'>
	<a href='home'>Accueil</a> > Bugstracker
</div>
<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Liste des bugs</center></div>
<div class='cadre2'>
	<br />
	<div class='newscontent'>
		<div style='margin-left:15px;'>
			<form method='post'>
				<div style='float:left;margin-right:70px;'>
					<label>
						Status: 
						<select name="status" onchange="bugsRequest(<?php echo $cat ?>);">
							<option value="0" selected="selected">Choisissez un status</option>
							<option value="1">Nouveau</option>
							<option value="2">Invalide</option> 
							<option value="3">En cours</option> 
							<option value="4">Requiert tests</option>
							<option value="5">R�solu</option>
						</select>
					</label>
				</div>
				<div>
					<label>
						Priorit�: 
						<select name="priority" onchange="bugsRequest(<?php echo $cat ?>);">
							<option value="0" selected="selected">Choisissez une priorit�</option>
							<option value="1">Non d�finie</option>
							<option value="2">Faible</option> 
							<option value="3">Moyenne</option> 
							<option value="4">Haute</option>
							<option value="5">Tr�s haute</option>
						</select>
					</label>
				</div>
			</form>
		</div>
		<div id='spacewhite2'></div>
		<div id="menutracker">
			<table width='100%' BORDER="1">
				<tr>
					<td>
						<?php
						if (isset($_GET['page']) && $_GET['page'] == 'bugstracker' && $cat == 1)
						{
							echo '<a href="bugstracker-1"><dl id="menutrackerfocus">Qu�tes</dl></a>';
						}
						else
						{
							echo '<a href="bugstracker-1"><dl>Qu�tes</dl></a>';
						}
						echo '
					</td>
					<td>
						';
						if (isset($_GET['page']) && $_GET['page'] == 'bugstracker' && $cat == 2)
						{
							echo '<a href="bugstracker-2"><dl id="menutrackerfocus">Cr�atures/Pnjs</dl></a>';
						}
						else
						{
							echo '<a href="bugstracker-2"><dl>Cr�atures/Pnjs</dl></a>';
						}
						echo '
					</td>
					<td>
						';
						if (isset($_GET['page']) && $_GET['page'] == 'bugstracker' && $cat == 3)
						{
							echo '<a href="bugstracker-3"><dl id="menutrackerfocus">Sorts/Talents</dl></a>';
						}
						else
						{
							echo '<a href="bugstracker-3"><dl>Sorts/Talents</dl></a>';
						}
						echo '
					</td>
					<td>
						';
						if (isset($_GET['page']) && $_GET['page'] == 'bugstracker' && $cat == 4)
						{
							echo '<a href="bugstracker-4"><dl id="menutrackerfocus">Objets</dl></a>';
						}
						else
						{
							echo '<a href="bugstracker-4"><dl>Objets</dl></a>';
						}
						echo '
					</td>
					<td>
						';
						if (isset($_GET['page']) && $_GET['page'] == 'bugstracker' && $cat == 5)
						{
							echo '<a href="bugstracker-5"><dl id="menutrackerfocus">Instances</dl></a>';
						}
						else
						{
							echo '<a href="bugstracker-5"><dl>Instances</dl></a>';
						}
						echo '
					</td>
					<td>
						';
						if (isset($_GET['page']) && $_GET['page'] == 'bugstracker' && $cat == 6)
						{
							echo '<a href="bugstracker-6"><dl id="menutrackerfocus">Site</dl></a>';
						}
						else
						{
							echo '<a href="bugstracker-6"><dl>Site</dl></a>';
						}
						echo '
					</td>
					<td>
						';
						if (isset($_GET['page']) && $_GET['page'] == 'bugstracker' && $cat == 7)
						{
							echo '<a href="bugstracker-7"><dl id="menutrackerfocus">Autre</dl></a>';
						}
						else
						{
							echo '<a href="bugstracker-7"><dl>Autre</dl></a>';
						}
						echo '
					</td>
					';
					?>
				</tr>
			</table>
		</div>
		<div id='listbug'>
			<table width='100%' BORDER="1">
				<tr height=40>
					<td width='100px' align='center'>
						Post� le
					</td>
					<td align='center'>
						Titre
					</td>
					<td width='100px' align='center'>
						Priorit�
					</td>
					<td width='100px' align='center'>
						Status
					</td>
				</tr>
				<tr id="tablebugs"></tr>
			</table>
		</div>
	</div>
</div>

<script type="text/JavaScript" src="js/bugtracker.js"></script>
<script type="text/javascript">
	bugsRequest(<?php echo $cat ?>);
</script>
