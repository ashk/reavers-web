<?php
if (!empty($arenaTeam))
{
	echo '
	<div id="chemin">
		<a href="home">Accueil</a> > <a href="armurerie">Armurerie</a> > '.$arenaTeam['name'].'
	</div>
	<div class="cadre2">
		<div class="newscontent5">
			<div style="font-size:35px;font-weight:bold;color:#960018;">
				'.$arenaTeam['name'].'
			</div>
			<div style="float:left;">
				';
				if ($arenaTeam['type'] == 2)
				{
					echo '
					<font color="darkred">Classement 2c2 :</font> '.$arenaTeam['rank'].'
					';
				}
				else if ($arenaTeam['type'] == 3)
				{
					echo '
					<font color="darkred">Classement 3c3 :</font> '.$arenaTeam['rank'].'
					';
				}
				else if ($arenaTeam['type'] == 5)
				{
					echo '
					<font color="darkred">Classement 5c5 :</font> '.$arenaTeam['rank'].'
					';
				}
				echo '
			</div>
			<div style="float:right;">
				<font color="darkred">C�te �quipe :</font> '.$arenaTeam['rating'].'
			</div>
			<br />
			<div style="float:left;">
				<font color="darkred">Capitaine :</font> <a href="personnage-'.$arenaTeam['captainGuid'].'">'.$royaume->get_name_by_guid($arenaTeam['captainGuid']).'</a>
			</div>
			<div style="float:right;">
				'.count($arenaTeamMembers).' membres
			</div>
		</div>
	</div>
	<br />
	<div class="cadre2">
		<div style="margin-left:80px;font-weight:bold;font-size:25px;">
			Equipe
		</div>
		<div class="newscontent3">
			<table width="100%" BORDER="0">
				<tr>
					<td align="center">
					</td>
					<td align="center">
						Jou�s
					</td>
					<td align="center">
						Gagn�s - Perdus
					</td>
					<td align="center">
						% gagn�s
					</td>
				</tr>
				<tr>
					<td align="center">
						Semaine
					</td>
					<td align="center">
						'.$arenaTeam['weekGames'].'
					</td>
					<td align="center">
						'.$arenaTeam['weekWins'].' - '.$arenaTeam['weekLoose'].'
					</td>
					<td align="center">
						'.$arenaTeam['weekPercentWin'].'%
					</td>
				</tr>
				<tr>
					<td align="center">
						Saison
					</td>
					<td align="center">
						'.$arenaTeam['seasonGames'].'
					</td>
					<td align="center">
						'.$arenaTeam['seasonWins'].' - '.$arenaTeam['seasonLoose'].'
					</td>
					<td align="center">
						'.$arenaTeam['seasonPercentWin'].'%
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:80px;font-weight:bold;font-size:25px;margin-top:10px;">
			Membres
		</div>
		<div class="newscontent3">
			<div style="float:right;font-weight:bold;margin-top:-10px;margin-bottom:10px;">
				<font color="darkred">Semaine</font>
			</div>
			<table width="100%" BORDER="0">
				<tr>
					<td align="center">
					</td>
					<td align="center">
						Jou�s
					</td>
					<td align="center">
						% Jou�s
					</td>
					<td align="center">
						Gagn�s - Perdus
					</td>
					<td align="center">
						% gagn�s
					</td>
					<td align="center">
						C�te
					</td>
				</tr>
				';
				foreach($arenaTeamMembers as $arenaTeamMember)
				{
					echo '
					<tr>
						<td>
							<a href="personnage-'.$arenaTeamMember['guid'].'"><font color="darkred">'.$royaume->get_name_by_guid($arenaTeamMember['guid']).'</font></a>
						</td>
						<td align="center">
							'.$arenaTeamMember['weekGames'].'
						</td>
						<td align="center">
							'.$arenaTeamMember['weekPercentGames'].'%
						</td>
						<td align="center">
							'.$arenaTeamMember['weekWins'].' - '.$arenaTeamMember['weekLoose'].'
						</td>
						<td align="center">
							'.$arenaTeamMember['weekPercentWin'].'%
						</td>
						<td align="center">
							'.$arenaTeamMember['personalRating'].'
						</td>
					</tr>
					';
				}
				echo '
			</table>
		</div>
		<br />
		<div class="newscontent3">
			<div style="float:right;font-weight:bold;margin-top:-10px;margin-bottom:10px;">
				<font color="darkred">Saison</font>
			</div>
			<table width="100%" BORDER="0">
				<tr>
					<td align="center">
					</td>
					<td align="center">
						Jou�s
					</td>
					<td align="center">
						% Jou�s
					</td>
					<td align="center">
						Gagn�s - Perdus
					</td>
					<td align="center">
						% gagn�s
					</td>
					<td align="center">
						C�te
					</td>
				</tr>
				';
				foreach($arenaTeamMembers as $arenaTeamMember)
				{
					echo '
					<tr>
						<td>
							<a href="personnage-'.$arenaTeamMember['guid'].'"><font color="darkred">'.$royaume->get_name_by_guid($arenaTeamMember['guid']).'</font></a>
						</td>
						<td align="center">
							'.$arenaTeamMember['seasonGames'].'
						</td>
						<td align="center">
							'.$arenaTeamMember['seasonPercentGames'].'%
						</td>
						<td align="center">
							'.$arenaTeamMember['seasonWins'].' - '.$arenaTeamMember['seasonLoose'].'
						</td>
						<td align="center">
							'.$arenaTeamMember['seasonPercentWin'].'%
						</td>
						<td align="center">
							'.$arenaTeamMember['personalRating'].'
						</td>
					</tr>
					';
				}
				echo '
			</table>
		</div>
	</div>
	';
}
?>
