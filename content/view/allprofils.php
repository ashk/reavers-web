<?php
if (!empty($user->sess_id))
{
	?>
	<div id='chemin'>
		<a href='home'>Accueil</a> > Tout les membres
	</div>
	<div style='width:650px;margin-left:15px;margin-bottom:5px;font-size:40px;'><center>Liste des profils</center></div>
	<div class='cadre2'>
		<div class='newscontent'>
			<form method="post" id="general" action="">
				<label class='form_col' for="content">Nom du membre: </label>
				<input type="text" name="name" id="name" size="30"/>
				<input type='submit' value='Rechercher'>
			</form>
			<br />
			<div id='spacewhite2'></div>
			<table border='1' width="100%">
				<tr>
					<td align='center'><div style='font-weight:bold;'>Nom du membre</div></td>
					<td align='center'><div style='font-weight:bold;'>Date d'inscription</div></td>
					<td align='center'><div style='font-weight:bold;'>Derniere connection au site</div></td>
					<td align='center'><div style='font-weight:bold;'>Derniere connection en jeu</div></td>
				</tr>
				<?php
				if (!empty($members))
				{
					foreach ($members as $key => $member)
					{
						echo '
						<tr>
							<td>
								<a href="profil-'.$member['id'].'">
										'.$member['login'].'
								</a>
							</td>
							<td align="center">'.$member['date_register'].'</td>
							<td align="center">'.$member['last_date_login'].'</td>
							<td align="center">'.$accounts[$key]['last_login'].'</td>
						</tr>
						';
					}
				}
				else
				{
					echo '
					<tr align="center">
						<td colspan="4">
							<strong><font color="red">Il n\'y a pas de membres !</font></strong>
						</td>
					</tr>
					';
				}
				echo '
			</table>
		</div>
	</div>
	';
}
?>