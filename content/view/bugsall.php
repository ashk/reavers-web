<?php
if (!empty($user->sess_id))
{
	if (!empty($profil))
	{
		if (!empty($bugs))
		{
			echo '
			<div id="chemin">
				<a href="home">Accueil</a> > <a href="profil-'.$id.'">Profil de '.$profil['visible_name'].'</a> > Bugs
			</div>
			';
			?>
			<div style="width:650px;margin-left:15px;margin-bottom:5px;margin-top:15px;font-size:40px;"><center>Tout les bugs</center></div>
			<div class="cadre2">
				<div class='newscontent'>
					<div id='listbug'>
						<table width='100%' BORDER="1">
							<tr height=40>
								<td width='100px' align='center'>
									Post� le
								</td>
								<td align='center'>
									Titre
								</td>
								<td width='100px' align='center'>
									Priorit�
								</td>
								<td width='100px' align='center'>
									Status
								</td>
							</tr>
							<?php
							foreach($bugs as $key => $bug)
							{
								echo "
								<tr>
									<td width='100px' align='center'>
										".date("d-m-Y", $bug['firstdate'])."
									</td>
									<td align='center'>
										<a href='bug-".$bug['id']."'>".$bug['title']."</a>
									</td>
									<td width='100px' align='center'>
										".$site->get_priority_by_priorityid($bug['priority'])."
									</td>
									<td width='100px' align='center'>
										".$site->get_state_by_stateid($bug['state'])."
									</td>
								</tr>
								";
							}
							echo "
						</table>
					</div>
				</div>
			</div>
			";
		}
	}
}
?>