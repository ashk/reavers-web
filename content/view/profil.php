<?php
if (!empty($profil))
{
	?>
	<div id='chemin'>
		<a href='home'>Accueil</a> > Profil de <?php echo $profil['visible_name']; ?>
	</div>
	<div class='cadre2'>
		<div style="float:right;">
			<a href="allprofils">Voir tout les profils...</a>
		</div>
		<br />
		<div class='newscontent'>
			<div style="font-size:30px;font-weight:bold;color:#000000;">
				<?php echo ucfirst($profil['visible_name']); ?>
			</div>
			Date d'inscription : <?php echo $profil['date_register']; ?>
			<?php
			if ($site->is_online($id))
			{
				?>
				<div style="float:right;color:#34C924;">
					En ligne
				</div>
				<?php
			}
			else
			{
				?>
				<div style="float:right;color:#ED0000;">
					Hors ligne
				</div>
				<?php
			}
			?>
			<br />
			Derni�re visite : <?php echo $profil['last_date_login']; ?>
			<div style="float:right;color:#960018;">
				<?php echo $site->get_grade_by_level($profil['level']); ?>
			</div>
		</div>
        <?php
        if (!empty($user->array_user) && !$user->is_banned_by_ip() && !$user->is_banned_by_account() && empty($user->array_account['locked']))
		{
            echo '
		    <br />
		    <div style="float:right;position:relative;bottom:2px;top:-17px;">
                <a href="#" data-rel="popup_name" class="poplight" data-width="500"><button type="submit" id="button">Envoyer un message priv�e</button></a>
		    </div>
            ';
        }
        ?>
	</div>
	<?php
	if (!empty($comments))
	{
		?>
		<div style='width:650px;margin-left:15px;margin-bottom:5px;margin-top:25px;font-size:40px;'><center>Derniers commentaires</center></div>
		<div class='cadre2'>
			<?php
			foreach($comments as $comment)
			{
				$news = $site->getNewsById($comment['newsid']);
				echo '
				<div class="newscontent">
					<div id="listbug">
						Discussion : <a href="news-'.$news['id'].'">'.$news['title'].'</a>
						<div style="margin-left:10px;">
							Message : '.$site->tronquer($comment['content'], 50).'
						</div>
						<br />
						<div style="float:right;position:relative;bottom:10px;">
							Publi� le '.$comment['date'].' � '.$comment['heure'].'
						</div>
					</div>
				</div>
				<br />
				';
			}
			echo '
			<div style="float:right;position:relative;bottom:10px;">
				<a href="commentall-'.$id.'">Tout les commentaires...</a>
			</div>
		</div>
		';
	}
	if (!empty($bugs))
	{
		?>
		<div style='width:650px;margin-left:15px;margin-bottom:5px;margin-top:25px;font-size:40px;'><center>Derniers bugs recens�s</center></div>
		<div class='cadre2'>
			<div class='newscontent'>
				<div id='listbug'>
					<table width='100%' BORDER="1">
						<tr height=40>
							<td width='100px' align='center'>
								Post� le
							</td>
							<td align='center'>
								Titre
							</td>
							<td width='100px' align='center'>
								Priorit�
							</td>
							<td width='100px' align='center'>
								Status
							</td>
						</tr>
						<?php
						foreach($bugs as $key => $bug)
						{
							echo "
							<tr>
								<td width='100px' align='center'>
									".date("d-m-Y", $bug['firstdate'])."
								</td>
								<td align='center'>
									<a href='bug-".$bug['id']."'>".$bug['title']."</a>
								</td>
								<td width='100px' align='center'>
									".$site->get_priority_by_priorityid($bug['priority'])."
								</td>
								<td width='100px' align='center'>
									".$site->get_state_by_stateid($bug['state'])."
								</td>
							</tr>
							";
						}
						echo "
					</table>";
					echo '
				</div>
			</div>
			<br />
			<div style="float:right;position:relative;bottom:10px;">
				<a href="bugsall-'.$id.'">Tout les bugs recens�s...</a>
			</div>
		</div>
		';
	}
	if (!empty($bugscomments))
	{
		?>
		<div style='width:650px;margin-left:15px;margin-bottom:5px;margin-top:25px;font-size:40px;'><center>Derniers commentaires de bugs</center></div>
		<div class='cadre2'>
			<?php
			foreach($bugscomments as $bugscomment)
			{
				$bug = $site->get_bug_by_id($bugscomment['bugid']);
				echo '
				<div class="newscontent">
					<div id="listbug">
						Discussion : <a href="bug-'.$bug['id'].'">'.$bug['title'].'</a>
						<div style="margin-left:10px;">
							Message : '.$site->tronquer($bugscomment['content'], 50).'
						</div>
						<br />
						<div style="float:right;position:relative;bottom:10px;">
							Publi� le '.$bugscomment['date'].' � '.$bugscomment['heure'].'
						</div>
					</div>
				</div>
				<br />
				';
			}
			echo '
			<div style="float:right;position:relative;bottom:10px;">
				<a href="bugscommentall-'.$id.'">Tout les commentaires...</a>
			</div>
		</div>
		';
	}
	?>
	<div style='width:650px;margin-left:15px;margin-bottom:5px;margin-top:25px;font-size:40px;'><center>Compte en jeu</center></div>
	<div class='cadre2'>
		<div class='newscontent'>
			Derniere connexion : <?php echo $account['last_login']; ?>
			<?php
			if ($royaume->is_banned_by_ip($account['last_ip']) || $royaume->is_banned_by_id($id))
			{
				?>
				<div style="float:right;color:#ED0000;">
					Banni
				</div>
				<?php
			}
			else if (!empty($account['locked']))
			{
				?>
				<div style="float:right;color:#ED0000;">
					Bloqu�
				</div>
				<?php
			}
			else
			{
				?>
				<div style="float:right;color:#34C924;">
					Actif
				</div>
				<?php
			}
			?>
		</div>
		<br />
		<?php
		if (!empty($characters))
		{
			echo '
			<table border="1" width="100%">
				<tr>
					<td align="center"><div style="font-weight:bold;">Nom du personnage</div></td>
					<td align="center"><div style="font-weight:bold;">Niveau</div></td>
					<td align="center"><div style="font-weight:bold;">Race</div></td>
					<td align="center"><div style="font-weight:bold;">Classe</div></td>
					<td align="center"><div style="font-weight:bold;">Etat</div></td>
				</tr>
				';
				foreach ($characters as $character)
				{
					echo '
					<tr>
						<td>
							<a href="personnage-'.$character['guid'].'">
								'.$character['name'].'
							</a>
						</td>
						<td align="center"><font color="darkred">'.$character['level'].'</font></td>
						<td align="center"><img src="img/icons/race/'.$character['race'].'-'.$character['gender'].'.gif"/></td>
						<td align="center"><img src="img/icons/class/'.$character['class'].'.gif"/></td>
						<td align="center">
							';
							if ($character['online'])
							{
								echo '
								<div style="color:#34C924;">
									En ligne
								</div>
								';
							}
							else
							{
								echo '
								<div style="color:#ED0000;">
									Hors ligne
								</div>
								';
							}
							echo '
						</td>
					</tr>
					';
				}
				echo '
			</table>
			';
		}
		echo '
	</div>
	';
}
echo '
<div id="popup_name" class="popup">
    <form method="post" action="privatesend" id="general">
        <div id="messagecomment3">
            <label for="content">Titre du message: </label><br />
            <input type="text" name="title" id="name" size="74"/><br />
            <label for="content">Contenu du message: </label><br />
            <textarea cols="55" rows="10" name="content" id="comment"></textarea><br />
        </div>
        <div id="messagecomment4">
            <input type="submit" name="Envoyer" value="Envoyer" />
        </div>
        <input type="hidden" name="receiver" value='.$id.' />
    </form>
</div>
';
?>
<script type="text/JavaScript" src="js/modale.js"></script>
<script type="text/JavaScript" src="js/general.js"></script>