<?php
if (!empty($user->sess_id))
{
	?>
	<div id='chemin'>
		<a href='home'>Accueil</a> > Mon compte
	</div>
	<div id="left-column">
		<div style="font-size:12px;">
			<div class="menu-category"><img src="img/icon_House.png" class="menu-icon" alt="Menu1"><span class="catmenutitle">Mon compte</span></div>
			<?php
			if (!isset($_GET['page']) || $_GET['page'] == 'compte')
			{
				echo '<a href="compte" class="menuitem"><span><div id="menuitemhover">Acceuil</div></span></a>';
			}
			else
			{
				echo '<a href="compte" class="menuitem"><span>Acceuil</span></a>';
			}
			if (isset($_GET['page']) && $_GET['page'] == 'privatesend')
			{
                if (!empty($privnonlues))
				{
					echo "
					<a href='privatesend' class='menuitem'><span><div id='menuitemhover'>Messages priv�e (".count($privnonlues).")</div></span></a>
					";
				}
                else
                {
                    echo "
					<a href='privatesend' class='menuitem'><span><div id='menuitemhover'>Messages priv�e</div></span></a>
					";
                }
		    }
			else
			{
                if (!empty($privnonlues))
				{
					echo '<a href="privatesend" class="menuitem"><span>Messages priv�e ('.count($privnonlues).')</span></a>';
                }
                else
                {
                    echo '<a href="privatesend" class="menuitem"><span>Messages priv�e</span></a>';
                }
		    }
			if (isset($_GET['page']) && $_GET['page'] == 'modpass')
			{
				echo '<a href="modpass" class="menuitem"><span><div id="menuitemhover">Mot de passe</div></span></a>';
			}
			else
			{
				echo '<a href="modpass" class="menuitem"><span>Mot de passe</span></a>';
			}
			if (isset($_GET['page']) && $_GET['page'] == 'modmail')
			{
				echo '<a href="modmail" class="menuitem"><span><div id="menuitemhover">Adresse email</div></span></a>';
			}
			else
			{
				echo '<a href="modmail" class="menuitem"><span>Adresse email</span></a>';
			}
			if (isset($_GET['page']) && $_GET['page'] == 'modvisiblename')
			{
				echo '<a href="modvisiblename" class="menuitem"><span><div id="menuitemhover">Nom visible</div></span></a>';
			}
			else
			{
				echo '<a href="modvisiblename" class="menuitem"><span>Nom visible</span></a>';
			}
			?>
			<!-- <div class="menu-category"><img src="img/icon_community.png" class="menu-icon" alt="Menu3"><span class="catmenutitle">Mes personnages</span></div> -->
			<?php
			/*if (isset($_GET['page']) && $_GET['page'] == 'changerace')
			{
				echo '<a href="changerace" class="menuitem"><span><div id="menuitemhover">Changer de race</div></span></a>';
			}
			else
			{
				echo '<a href="changerace" class="menuitem"><span>Changer de race</span></a>';
			}
			if (isset($_GET['page']) && $_GET['page'] == 'renommer')
			{
				echo '<a href="renommer" class="menuitem"><span><div id="menuitemhover">Renommer personnage</div></span></a>';
			}
			else
			{
				echo '<a href="renommer" class="menuitem"><span>Renommer personnage</span></a>';
			}
            if (isset($_GET['page']) && $_GET['page'] == 'customize')
			{
				echo '<a href="customize" class="menuitem"><span><div id="menuitemhover">Changer d\'apparence</div></span></a>';
			}
			else
			{
				echo '<a href="customize" class="menuitem"><span>Changer d\'apparence</span></a>';
			}
            if (isset($_GET['page']) && $_GET['page'] == 'faction')
			{
				echo '<a href="faction" class="menuitem"><span><div id="menuitemhover">Changer de faction</div></span></a>';
			}
			else
			{
				echo '<a href="faction" class="menuitem"><span>Changer de faction</span></a>';
			}*/
			?>
		</div>
	</div>
    <?php
    if (!isset($_GET['page']) || $_GET['page'] == 'compte')
	{
		echo "
        <div class='cadre7'>
            <div class='newscontent4'>
				<label class='form_col2'>Nom de compte :</label>
				<font color='FF0000'>".$user->array_user['login']."</font>
				<br />
				<label class='form_col2'>Adresse email :</label>
				<font color='FF0000'>".$user->array_user['email']."</font>
				<label class='form_col2'>Nom visible :</label>
				<font color='FF0000'>".$user->array_user['visible_name']."</font>
				<br />
				<label class='form_col2'>Date d'inscription :</label>
				<font color='FF0000'>".$user->array_user['date_register']."</font>
				<br />
				<label class='form_col2'>Derniere connexion au site :</label>
				<font color='FF0000'>".$user->array_user['last_date_login']."</font>
				<br />
				<label class='form_col2'>Derniere connexion au serveur :</label>
				<font color='FF0000'>".$user->array_account['last_login']."</font>
				<br />
				<label class='form_col2'>Extension :</label>
				";
				if ($user->array_account['expansion'] == 0)
				{
					echo "
					<font color='#fff468'>Classique</font>
					";
				}
				else if ($user->array_account['expansion'] == 1)
				{
					echo "
					<font color='#aad372'>The Burning Crusade</font>
					";
				}
				else if ($user->array_account['expansion'] == 2)
				{
					echo "
					<font color='#68ccef'>Wrath of the Lich King</font>
					";
				}
				echo "
				<br />
				<label class='form_col2'>Grade :</label>
				<font color='FF0000'>".$user->getlevel()."</font>
				";
				?>
				<div id="spacewhite3"></div>
				<label class='form_col2'>Etat du compte :</label>
				<?php
				if (!empty($idban) || !empty($ipban))
				{
					echo "<font color='red'>Banni</font>";
				}
				else if (!empty($user->array_account['locked']))
				{
					echo "<font color='red'>Bloqu�</font>";
				}
				else
				{
					echo "<font color='34C924'>Actif</font>";
				}
				if (!empty($idban))
				{
					if ($idban['bandate'] > $idban['unbandate'])
					{
						echo '
						<br />
						<br />
						<center>
							Votre compte a �t� banni � vie du serveur de jeu.
							<br />
							<label class="form_col2">Banni par :</label>
							<font color="FF0000">'.$idban['bannedby'].'</font>
							<br />
							<label class="form_col2">Raison :</label>
							<font color="FF0000">'.$idban['banreason'].'</font>
						</center>
						';
					}
					else
					{
						echo '
						<br />
						<br />
						<center>
							Votre compte a �t� banni du <font color="FF0000">'.date("d-m-Y", $idban['bandate']).'</font> au <font color="BF3030">'.date("d-m-Y", $idban['unbandate']).'</font>
						</center>
						<label class="form_col2">Banni par :</label>
						<font color="FF0000">'.$idban['bannedby'].'</font>
						<br />
						<label class="form_col2">Raison :</label>
						<font color="FF0000">'.$idban['banreason'].'</font>
						';
					}
				}
				if (!empty($ipban))
				{
					if ($ipban['bandate'] > $ipban['unbandate'])
					{
						echo '
						<br />
						<br />
						<center>
							Votre adresse ip actuelle a �t� banni � vie du serveur de jeu.
							<br />
							<label class="form_col2">Banni par :</label>
							<font color="FF0000">'.$ipban['bannedby'].'</font>
							<br />
							<label class="form_col2">Raison :</label>
							<font color="FF0000">'.$ipban['banreason'].'</font>
						</center>
						';
					}
					else
					{
						echo '
						<br />
						<br />
						<center>
							Votre adresse ip actuelle a �t� banni du <font color="FF0000">'.date("d-m-Y", $ipban['bandate']).'</font> au <font color="BF3030">'.date("d-m-Y", $ipban['unbandate']).'</font>
						</center>
						<label class="form_col2">Banni par :</label>
						<font color="FF0000">'.$ipban['bannedby'].'</font>
						<br />
						<label class="form_col2">Raison :</label>
						<font color="FF0000">'.$ipban['banreason'].'</font>
						';
					}
				}
                echo '
            </div>
        </div>
        ';
	}
	else if (isset($_GET['page']) && $_GET['page'] == 'modpass')
	{
		echo "
        <div class='cadre7'>
            <div class='newscontent4'>
				<form id='compte' method='post' action='modpass'>
					<label class='form_col3'>Ancien mot de passe :</label>
					<input type='password' name='oldpass' id='oldpass' tabindex='40' size='30' maxlength='64'/>
					<br />
					<label class='form_col3'>Nouveau mot de passe :</label>
					<input type='password' name='pass' id='pass' tabindex='40' size='30' maxlength='64'/>
					<br />
					<label class='form_col3'>Confirmation du mot de passe :</label>
					<input type='password' name='confirmpass' id='confirmpass' tabindex='40' size='30' maxlength='64'/>
					<br />
					<div style='margin-left:335px;'>
						<input type='submit' name='modpassvalid' value='Modifier'> <input type='reset' />
					</div>
				</form>
            </div>
        </div>
		";
	}
	else if (isset($_GET['page']) && $_GET['page'] == 'modvisiblename')
	{
		echo "
        <div class='cadre7'>
            <div class='newscontent4'>
				<form id='compte' method='post' action='modvisiblename'>
					<label class='form_col4'>Nom visible :</label>
					<input type='text' name='name' id='name' tabindex='40' value ='".$user->array_user['visible_name']."' size='30' maxlength='64'/>
					<br />
					<div style='margin-left:275px;'>
						<input type='submit' name='modvisiblename' value='Modifier'> <input type='reset' />
					</div>
				</form>
            </div>
        </div>
	    ";
	}
	else if (isset($_GET['page']) && $_GET['page'] == 'modmail')
	{
		echo "
        <div class='cadre7'>
            <div class='newscontent4'>
				<form id='compte' method='post' action='modmail'>
					<label class='form_col3'>Ancienne adresse mail :</label>
					<input type='text' name='oldmail' id='oldmail' tabindex='40' value='".$user->array_user['email']."' size='30' maxlength='64'/>
					<span class='tooltip' id='oldmail1'>Adresse mail non conforme</span>
					<br />
					<label class='form_col3'>Nouvelle adresse mail :</label>
					<input type='text' name='mail' id='mail' tabindex='40' size='30' maxlength='64'/>
					<span class='tooltip' id='email1'>Adresse mail non conforme</span>
					<br />
					<label class='form_col3'>Confirmation de l'adresse mail :</label>
					<input type='text' name='confirmmail' id='confirmmail' tabindex='40' size='30' maxlength='64'/>
					<span class='tooltip' id='confirmmail1'>Le mail de confirmation doit �tre identique � celui d'origine</span>
					<br />
					<div style='margin-left:335px;'>
						<input type='submit' name='modmail' value='Modifier'> <input type='reset' />
					</div>
				</form>
            </div>
        </div>
		";
	}
	else if (isset($_GET['page']) && $_GET['page'] == 'privatesend')
	{
        if (!empty($_GET['variable1']) && is_numeric($_GET['variable1']))
		{
            echo "
            <div style='margin-left:175px;margin-bottom:-30px;'><h2>Objet : ".$privateobjet['objet']."</h1></div>
            <div class='cadre7'>
				";
				if ($nbrMessages > 0)
				{
					?>
					<select name="limit" onchange="messageRequest(<?php echo $idObject ?>, this.value);">
						<?php
						for ($i = 0; $i < $nbrPages; $i++)
						{
							$index = $i + 1;
							?>
							<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 10) + 1) ?> - <?php echo (($i + 1) * 10) ?></option>
							<?php
						}
						?>
					</select>
					<br />
					<?php
				}
				echo "
				<span id='private'></span>
                ";
				    foreach ($privatemsgs as $privatemsg)
			        {
                        echo '
                        <br />
                        <div class="newscontent4">
                            <div id="newstext">
                                '.$privatemsg['content'].'
                            </div>
                            <div id="newsdate">
                                Publi� le '.date("d-m-Y",$privatemsg['timestamp']).' � '.date("H:i",$privatemsg['timestamp']).' par <a href="profil-'.$privatemsg['sender'].'">'.$site->get_name_by_id($privatemsg['sender']).'</a>
                            </div>
                        </div>
                        ';
                    }
                echo '
				<a href="#" data-rel="popup_name" class="poplight" data-width="500">
					<button type="submit" id="button">R�pondre</button>
				</a>
            </div>
            <div id="popup_name" class="popup">
                <form method="post" action="privatesend-'.$_GET['variable1'].'" id="general">
                    <div id="messagecomment3">
                        <label for="content">Ajouter une r�ponse: </label><br />
                        <textarea cols="55" rows="10" name="content" id="comment"></textarea><br />
                    </div>
                    <div id="messagecomment4">
                        <input type="submit" name="Envoyer" value="Envoyer" />
                    </div>
                </form>
            </div>
            ';
			?>
			<script type="text/JavaScript" src="js/messages.js"></script>
			<script type="text/javascript">
				messageRequest(<?php echo $idObject ?>, 1);
			</script>
			<?php
        }
        else if (empty($_GET['variable1']))
        {
			echo "
            <div class='cadre7'>
                <div class='newscontent4'>
				    <div style='font-size:15px;font-weight:bold;color:#68ccef;'>
			            Messages recus
				    </div>
				    <table width='100%' BORDER='1'>
		                <tr>
						    <td align='center'>
						        Objet
						    </td>
						    <td align='center' width='100px'>
					            Date d'envoi
						    </td>
					        <td align='center' width='100px'>
					            Envoy� par
					        </td>
					    </tr>
					    ";
						if ($nbrObjectReceive > 0)
						{
							?>
							<tr id="tablereceive"></tr>
							<?php
						}
					    else
					    {
						   echo '
						   <tr align="center">
							   <td colspan="3">
                                    <strong>Aucun message !</strong>
                                </td>
                            </tr>
                            ';
                        }
                        echo "
                    </table>
					";
					if ($nbrObjectReceive > 0)
					{
						?>
						<select name="limit" onchange="objectSendRequest(<?php echo $user->sess_id ?>, this.value);">
							<?php
							for ($i = 0; $i < $nbrReceivePages; $i++)
							{
								$index = $i + 1;
								?>
								<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 10) + 1) ?> - <?php echo (($i + 1) * 10) ?></option>
								<?php
							}
							?>
						</select>
						<br />
						<?php
					}
					echo "
                    <div id='spacewhite3'></div>
                    <div style='font-size:15px;font-weight:bold;color:#68ccef;'>
                        Messages envoy�s
                    </div>
                    <table width='100%' BORDER='1'>
                        <tr>
                            <td align='center'>
                                Objet
                            </td>
                            <td align='center' width='100px'>
                                Date d'envoi
                            </td>
                            <td align='center' width='100px'>
                                Envoy� �
                            </td>
                        </tr>
                        ";
                        if ($nbrObjectSend > 0)
						{
							?>
							<tr id="tablesend"></tr>
							<?php
                        }
                        else
                        {
                            echo '
                            <tr align="center">
                                <td colspan="3">
                                    <strong>Aucun message !</strong>
                                </td>
                            </tr>
                            ';
                        }
                        echo "
                    </table>
					";
					if ($nbrObjectSend > 0)
					{
						?>
						<select name="limit" onchange="objectReceiveRequest(<?php echo $user->sess_id ?>, this.value);">
							<?php
							$index = 0;
							for ($i = 0; $i < $nbrSendPages; $i++)
							{
								$index = $i + 1;
								?>
								<option value="<?php echo $index ?>" <?php if ($i == 0) echo 'selected="selected"'; ?> > <?php echo (($i * 10) + 1) ?> - <?php echo (($i + 1) * 10) ?></option>
								<?php
							}
							?>
						</select>
						<br />
						<?php
					}
					echo "
                </div>
            </div>
            ";
			?>
			<script type="text/JavaScript" src="js/messageObject.js"></script>
			<script type="text/javascript">
				objectSendRequest(<?php echo $user->sess_id ?>, 1);
				objectReceiveRequest(<?php echo $user->sess_id ?>, 1);
			</script>
			<?php
        }
    }
    /*else if (isset($_GET['page']) && $_GET['page'] == 'changerace')
	{
        if (isset($_POST['personnage']))
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    A la prochaine connection l'option de changement de race sera disponible.
                </div>
            </div>
            ";
        }
        else
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    <form id='compte' method='post' action='changerace'>
                        <label>
                            Choisissez votre personnage
                            <select name='character'>
                                <optgroup label='Mes personnages'>
                                ";
                                foreach($characters as $character)
                                {
                                    echo '
                                    <option value="'.$character['guid'].'">'.$character['name'].'</option>
                                    ';
                                }
                                echo '
                            </select>
                        </label>
                        <input type="submit" name="personnage" value="Valider">
                    </form>
                </div>
            </div>
            ';
        }
    }
    else if (isset($_GET['page']) && $_GET['page'] == 'renommer')
	{
        if (isset($_POST['personnage']))
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    A la prochaine connection l'option de changement de nom sera disponible.
                </div>
            </div>
            ";
        }
        else
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    <form id='compte' method='post' action='renommer'>
                        <label>
                            Choisissez votre personnage
                            <select name='character'>
                                <optgroup label='Mes personnages'>
                                ";
                                foreach($characters as $character)
                                {
                                    echo '
                                    <option value="'.$character['guid'].'">'.$character['name'].'</option>
                                    ';
                                }
                                echo '
                            </select>
                        </label>
                        <input type="submit" name="personnage" value="Valider">
                    </form>
                </div>
            </div>
            ';
        }
    }
    else if (isset($_GET['page']) && $_GET['page'] == 'customize')
	{
        if (isset($_POST['personnage']))
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    A la prochaine connection l'option de changement d'apparence sera disponible.
                </div>
            </div>
            ";
        }
        else
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    <form id='compte' method='post' action='customize'>
                        <label>
                            Choisissez votre personnage
                            <select name='character'>
                                <optgroup label='Mes personnages'>
                                ";
                                foreach($characters as $character)
                                {
                                    echo '
                                    <option value="'.$character['guid'].'">'.$character['name'].'</option>
                                    ';
                                }
                                echo '
                            </select>
                        </label>
                        <input type="submit" name="personnage" value="Valider">
                    </form>
                </div>
            </div>
            ';
        }
    }
    else if (isset($_GET['page']) && $_GET['page'] == 'faction')
	{
        if (isset($_POST['personnage']))
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    A la prochaine connection l'option de changement de faction sera disponible.
                </div>
            </div>
            ";
        }
        else
        {
            echo "
            <div class='cadre7'>
                <div class='newscontent4'>
                    <form id='compte' method='post' action='faction'>
                        <label>
                            Choisissez votre personnage
                            <select name='character'>
                                <optgroup label='Mes personnages'>
                                ";
                                foreach($characters as $character)
                                {
                                    echo '
                                    <option value="'.$character['guid'].'">'.$character['name'].'</option>
                                    ';
                                }
                                echo '
                            </select>
                        </label>
                        <input type="submit" name="personnage" value="Valider">
                    </form>
                </div>
            </div>
            ';
        }
    }*/
}
?>
<script type="text/JavaScript" src="js/modale.js"></script>
<script type="text/JavaScript" src="js/compte.js"></script>