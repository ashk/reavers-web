    <!DOCTYPE html>
<html>
<head>
	<title><?php echo $site->nom_site; ?></title>
	<meta name='keywords' lang='fr' content='liste,de,mots,cles' >
	<meta name='description' lang='fr' content="Description de votre serveur." >
	<meta http-equiv="content-Type" content="text/html; charset=iso-8859-1" >
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" media="screen" type="text/css"  href="style/index.css" >
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="http://static.wowhead.com/widgets/power.js"></script>
	<script type="text/javascript" src="js/compteur.js"></script>
</head>
<body>
	<header>
		<div id="menu">
			<?php
			if (!isset($_GET['page']) || $_GET['page'] == 'home')
			{
				echo '<a href="home"><dl id="menufocusleft">Accueil<div id="separator"></div></dl></a>';
			}
			else
			{
				echo '<a href="home"><dl id="left">Accueil<div id="separator"></div></dl></a>';
			}
            if(empty($user->sess_id)) 
            {
                if (isset($_GET['page']) && $_GET['page'] == 'inscription')
                {
                    echo '<a href="inscription"><dl id="menufocus">Inscription<div id="separator"></div></dl></a>';
                }
                else
                {
                    echo '<a href="inscription"><dl>Inscription<div id="separator"></div></dl></a>';
                }
            }
            else
            {
                if (isset($_GET['page']) && $_GET['page'] == 'premium')
                {
                    echo '<a href="premium"><dl id="menufocus">Premium<div id="separator"></div></dl></a>';
                }
                else
                {
                    echo '<a href="premium"><dl>Premium<div id="separator"></div></dl></a>';
                }
            }
            if (isset($_GET['page']) && $_GET['page'] == 'achat')
            {
                echo '<a href="achat"><dl id="menufocus">Achater des RPoint<div id="separator"></div></dl></a>';
            }
            else
            {
                echo '<a href="achat"><dl>Achater des RPoint<div id="separator"></div></dl></a>';
            }
			if (isset($_GET['page']) && $_GET['page'] == 'equipe')
			{
				echo '<a href="equipe"><dl id="menufocus">Equipe<div id="separator"></div></dl></a>';
			}
			else
			{
				echo '<a href="equipe"><dl>Equipe<div id="separator"></div></dl></a>';
			}
			if (isset($_GET['page']) && $_GET['page'] == 'armurerie')
			{
				echo '<a href="armurerie"><dl id="menufocus">Armurerie<div id="separator"></div></dl></a>';
			}
			else
			{
				echo '<a href="armurerie"><dl>Armurerie<div id="separator"></div></dl></a>';
			}
			if (isset($_GET['page']) && $_GET['page'] == 'boutique')
			{
				echo '<a href="boutique"><dl id="menufocusright">Boutique</dl></a>';
			}
			else
			{
				echo '<a href="boutique"><dl id="right">Boutique</dl></a>';
			}
			?>
		</div>
		<a href="home" class="logo"><img src="img/tmpLogo_on.png" alt="logo"/></a>
		<div class="spacer"></div>
	</header>
	
	<div id="rpt-maincontent">
		<h1 class="page" style="padding-top: 2px; background: url('../img/RightMenuTitleCopy.png') no-repeat;height: 17px;width: 200px;border-radius: 7px;font-size: 14px;padding-left: 15px;padding-top: 4px;"><b>set realmlist 198.100.148.75</b></h1>
		<div class="spacer"></div>
		<div id="content">
			<?php
			if (!empty($view))
			{
				if (empty($_SESSION['error']))
				{
					require_once $view;
				}
				else
				{
					echo $_SESSION['error'];
				}
			}
			?>
		</div>
		<!-- MENU DE DROITE -->
		<div id="right-column">
			<div id="votecadre">
				<a href="vote"><img src="img/VoteIMG.png" class="voteimg"></a>
			</div>
            <div id="connexion">
				<?php
				if (empty($user->sess_id))
				{
					?>
						<h1 class="rightmenu">Connexion</h1>
						<div class="relative" style="z-index:5;"><br>
							<?php
							if (!empty($result) && ($result == 'not_activate' || $result == 'wrong_password' || $result == 'wrong_login'))
							{
								echo '
								<div style="color:#EE1010;font-size:12px;">Identifiants incorrects</div>
								';
							}
							?>
							<form method="post">
								<input type="text" id="login" name="login_connexion" placeholder="Nom de compte">
								<br/>
								<input type="password" id="password" name="pass_connexion" placeholder="Mot de passe">
								<button type='submit' id="buttonok" name='Connexion' value='ok'>OK</button>
								<div style="font-size:12px;"><input type="checkbox" name="souvenir" value='1'><label for="souvenir">Connexion automatique</label></div>
							</form>
							<div style="font-size:14px;margin-top:15px;"><a href="pass">Mot de passe perdu?</a></div>
						</div>
						<!--<img src="img/worgen.png" class="worgen">-->
					<?php
				}
				else
				{
					?>
					<h1 class="rightmenu">Bienvenue <?php echo ucfirst($site->get_name_by_id($user->sess_id)); ?></h1>
					<div class="relative" style="z-index:5;">
						<?php
						echo "Points : ".$user->array_points['points']."<br />";
						if ($user->is_banned_by_ip() || $user->is_banned_by_account())
						{
							echo "Etat du compte : <font color='red'>Banni</font>";
						}
						else if (!empty($user->array_account['locked']))
						{
							echo "Etat du compte : <font color='red'>Bloqu�</font>";
						}
						else
						{
							echo "Etat du compte : <font color='green'>Actif</font>";
						}
						echo "
						<br />
						<div id='spacewhite'></div>
						<a href='profil-".$user->sess_id."'>Voir mon profil</a><br />
						";
						if (!empty($privnonlues))
						{
							echo "
							<a href='compte'>Gerer mon compte</a> (".count($privnonlues)." non lus)
							";
						}
						else
						{
							echo "
							<a href='compte'>Gerer mon compte</a>
							";
						}
						?>
                        <div style="position:relative;left:160px;">
						    <form method="post" action='home'>
							    <button type='submit' id="button" name='Deconnexion' value='ok'>Deconnexion</button>
						    </form>
                        </div>
					</div>
					<?php
				}
				?>
			</div>
            <div id="stats">
				<h1 class="rightmenu">Statistiques</h1>
				<div id='statistiques'>
					<a href='statistiques'>Voir les statistiques</a>
				</div>
                
				<div class='popstats'>
					<img src='img/a2_icon.png' id='a2'>
					<div id="pourcent"></div>
					<img src='img/h2_icon.png' id='h2'>
				</div>
				<div class='spacer'></div>
				<div id="total">
				</div>
			</div>
			<div id="stats">
				<h1 class="rightmenu">Top votants</h1>
				<div id="topvotants">
					<div style='font-size:16px;'>
						<?php
						if (!empty($votants))
						{
							echo '
							<table width="100%" style="margin-left:1px;">
							';
							foreach($votants as $votant)
							{
								if (!empty($user->sess_id))
								{
									echo "
									<tr>
										<td align='left'><a href='profil-".$votant['id']."'>".$site->get_name_by_id($votant['id'])."</a></td>
										<td align='right'>".$votant['total']."</td>
									</tr>
									";
								}
								else
								{
									echo "
									<tr>
										<td align='left'>".$site->get_name_by_id($votant['id'])."</td>
										<td align='right'>".$votant['total']."</td>
									</tr>
									";
								}
							}
							?>	
							</table>
							<?php
						}
						?>
					</div>
				</div>
			</div>
			
			<div id="bugstracker">
				<h1 class="rightmenu">Bugstracker</h1>
				<div style='font-size:16px;'>
					<div id='listbug'>
						<?php
						if (!empty($debugs))
						{
							foreach($debugs as $debug)
							{
								echo "
									>> <a href='bug-".$debug['id']."'>".$site->tronquer($debug['title'], 15)."...</a> par ".$site->get_name_by_id($debug['idfix'])."
									<br />
								";
							}
						}
						?>
					</div>
				</div>
				<div style='margin-top:10px;margin-left:20px;'>
					<div id='buttonlistbug'>
						<center><a href='bugstracker'>Voir le bugstracker</a></center>
					</div>
					<div id='buttonaddbug'>
						<center><a href='addbug'>Signaler un bug</a></center>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer"></div>
	</div>
	<div id="footer">
		Design r�alis� par Elmrosth, Naeros, Mathman & Ashk| Cod� Par Mathman & Ashk.<br />
		Reavers-Online, tous droits r�serv�s.
	</div>
</body>
</html>