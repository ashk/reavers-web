<div style='width:650px;margin-left:15px;margin-bottom:5px;margin-top:15px;font-size:40px;'><center>L'�quipe <?php echo $array_site['nom']; ?></center></div>
<div class='cadre2'>
	<div class='newscontent'>
		<div style='font-size:25px;margin-top:5px;margin-bottom:10px;font-weight:bold;'>Administrateurs</div>
		<div id='spacewhite2'></div>
		<i style="font-size: 11px;">Ils supervisent le reste de l'�quipe et prennent ensemble les d�cisions importantes concernant le serveur.</i>
		<br />
		<ul>
			<?php
			if (!empty($admins))
			{
				foreach($admins as $admin)
				{
					echo '
					<li>
						<strong>
							'.$site->get_name_by_id($admin['id']).'
						</strong>
					</li>
					';
				}
			}
			?>
		</ul>
		<br />
		<div style='font-size:25px;margin-top:5px;margin-bottom:10px;font-weight:bold;'>D�veloppeurs</div>
		<div id='spacewhite2'></div>
		<i style="font-size: 11px;">Les D�veloppeurs ont pour r�le de corriger les bugs et am�liorer le contenu du serveur.</i>
		<br />
		<ul>
			<?php
			if (!empty($devs))
			{
				foreach($devs as $dev)
				{
					echo '
					<li>
						<strong>
							'.$site->get_name_by_id($dev['id']).'
						</strong>
					</li>
					';
				}
			}
			?>
		</ul>
		<br />
		<div style='font-size:25px;margin-top:5px;margin-bottom:10px;font-weight:bold;'>Ma�tres de jeu</div>
		<div id='spacewhite2'></div>
		<i style="font-size: 11px;">Les Ma�tres de jeu (ou MJ) se chargent de traiter les requ�tes en jeu et de r�gler les probl�mes des joueurs.</i>
		<br />
		<ul>
			<?php
			if (!empty($mjs))
			{
				foreach($mjs as $mj)
				{
					echo '
					<li>
						<strong>
							'.$site->get_name_by_id($mj['id']).'
						</strong>
					</li>
					';
				}
			}
			?>
		</ul>
		<br />
		<div style='font-size:25px;margin-top:5px;margin-bottom:10px;font-weight:bold;'>Mod�ration</div>
		<div id='spacewhite2'></div>
		<i style="font-size: 11px;">S'occupe de faire respecter les r�gles �l�mentaires de courtoisie et de biens�ance sur le forum et sur le chat, et r�pond � toutes vos question dans la mesure du possible.</i>
		<br />
		<ul>
			<?php
			if (!empty($modos))
			{
				foreach($modos as $modo)
				{
					echo '
					<li>
						<strong>
							'.$site->get_name_by_id($modo['id']).'
						</strong>
					</li>
					';
				}
			}
			?>
		</ul>
	</div>
</div>