<?php
if (!empty($user->array_user['login']))
{
	if (!$user->is_banned_by_ip() && !$user->is_banned_by_account() && empty($user->array_account['locked']))
	{
		if (empty($_POST['categorie']))
		{
			?>
			<div id="titre">
				Signaler un bug : Etape 1
			</div>
			<div class='cadre2'>
				<div class='newscontent'>
					<form action="addbug" id='general' method="post">
						<div style='margin-top:20px;margin-left:50px;'>
							<label for="category">Cat�gorie :</label>
							<select name="categorie" id="category">              
								<option name="categorie" value="1" selected="selected" >Qu�tes</option>
								<option name="categorie" value="2"  >Cr�atures/Pnjs</option>
								<option name="categorie" value="3"  >Talents/Sorts</option>
								<option name="categorie" value="4"  >Objets</option>
								<option name="categorie" value="5"  >Instances</option>
								<option name="categorie" value="6"  >Site</option>
								<option name="categorie" value="7"  >Autre</option>
							</select>
						</div>
						<div style='margin-top:20px;margin-left:520px;margin-bottom:10px;'><input type="submit" name="poster" value="Suivant"/></div>
					</form>
				</div>
			</div>
			<?php
		}
		else if (!empty($_POST['categorie']) && empty($_POST['name']))
		{
			?>
			<div id="titre">
				Signaler un bug : Etape 2
			</div>
			<div class='cadre2'>
				<div class='newscontent'>
					<form action="addbug" id='general' method="post">
						<div style='margin-top:20px;margin-left:50px;'>
							<?php
							if ($_POST['categorie'] == 1)
							{
								?>
								<label for="name">Nom de la qu�te concern�e :</label>
								<?php
							}
							else if ($_POST['categorie'] == 2)
							{
								?>
								<label for="name">Nom de la cr�ature/du pnj concern� :</label>
								<?php
							}
							else if ($_POST['categorie'] == 3)
							{
								?>
								<label for="name">Nom du talent/sort concern� :</label>
								<?php
							}
							else if ($_POST['categorie'] == 4)
							{
								?>
								<label for="name">Nom de l'objet concern� :</label>
								<?php
							}
							else if ($_POST['categorie'] == 5)
							{
								?>
								<label for="name">Nom de l'instance concern�e :</label>
								<?php
							}
							else if ($_POST['categorie'] == 6 || $_POST['categorie'] == 7)
							{
								?>
								<label for="name">Donnez un nom au probl�me :</label>
								<?php
							}
							?>
							<input type="text" name="name" id="name" size='50'/>
						</div>
						<input type="hidden" name="categorie" value="<?php echo  $_POST['categorie']; ?>"/>
						<div style='margin-top:20px;margin-left:520px;margin-bottom:10px;'><input type="submit" name="poster" value="Suivant"/></div>
					</form>
				</div>
			</div>
			<?php
		}
		else if (!empty($_POST['categorie']) && !empty($_POST['name']) && empty($_POST['lienwowhead']))
		{
			?>
			<div id="titre">
				Signaler un bug : Etape 3
			</div>
			<div class='cadre2'>
				<div class='newscontent'>
					<form action="addbug" id='general' method="post">
						<div style='margin-top:20px;margin-left:50px;'>
							<label for="lienwowhead">Liens <a href="http://fr.wowhead.com" target="_blank">Wowhead</a>:</label>
							<input type="text" name="lienwowhead" id="name" size='70'/>
						</div>
						<input type="hidden" name="categorie" value="<?php echo  $_POST['categorie']; ?>"/>
						<input type="hidden" name="name" value="<?php echo  $_POST['name']; ?>"/>
						<div style='margin-top:20px;margin-left:520px;margin-bottom:10px;'><input type="submit" name="poster" value="Suivant"/></div>
					</form>
				</div>
			</div>
			<?php
		}
		else if (!empty($_POST['categorie']) && !empty($_POST['name']) && !empty($_POST['lienwowhead']) && empty($_POST['desc_offi']) && empty($_POST['desc_serv']))
		{
			?>
			<div id="titre">
				Signaler un bug : Etape 4
			</div>
			<div class='cadre2'>
				<div class='newscontent'>
					<form action="<?php echo "bugstracker"; ?>" id='general' method="post">
						<div style='margin-top:20px;margin-left:50px;'>
							<p><label>D�crivez le comportement normal :</label>
							<textarea name="desc_offi" rows="8" cols="65" id="desc_offi"></textarea></p>
							<p><label>D�crivez le comportement sur le serveur :</label><br />
							<textarea name="desc_serv" rows="8" cols="65" id="desc_serv"></textarea></p>
						</div>
						<input type="hidden" name="categorie" value="<?php echo  $_POST['categorie']; ?>"/>
						<input type="hidden" name="name" value="<?php echo  $_POST['name']; ?>"/>
						<input type="hidden" name="lienwowhead" value="<?php echo  $_POST['lienwowhead']; ?>"/>
						<div style='margin-top:20px;margin-left:530px;margin-bottom:10px;'><input type="submit" name="poster" value="poster"/></div>
					</form>
				</div>
			</div>
			<?php
		}
	}
}
?>
<script type="text/JavaScript" src="js/general.js"></script>
