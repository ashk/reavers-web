<div id="titre">
	Statistiques
</div>
<div class='cadre2'>
	<div class='newscontent'>
		<div style='font-size:25px;margin-top:5px;margin-bottom:10px;color:#C60800;font-weight:bold;'>Informations sur les comptes</div>
		<center>
			<div style='font-size:20px;'>Comptes cr��s</div>
			<?php echo $accounts; ?>
		</center>
	</div>
	<div style='font-size:30px;margin-top:40px;margin-bottom:-5px;margin-left:210px;color:#C60800;font-weight:bold;'>Royaume <?php echo $realmlist['name']; ?></div>
	<div class='newscontent'>
		<div style='font-size:25px;margin-top:10px;margin-bottom:10px;color:#C60800;font-weight:bold;'>Informations sur les personnages</div>
		<center>
			<div style='font-size:20px;'>Personnage cr��s</div>
			<?php echo sizeof($alliance) + sizeof($horde); ?>
		</center>
		<div style='font-size:25px;margin-top:10px;margin-bottom:10px;color:#C60800;font-weight:bold;'>Informations sur la population</div>
		<center>
			<div style='font-size:20px;margin-bottom:10px;'>R�partition par factions</div>
			<div style='margin-bottom:20px;'>
				<table width='500px'>
					<tr>
						<td align='center'><div style='font-weight:bold;'>Nombre de joueurs Allianceux</div></td>
						<td align='center'><div style='font-weight:bold;'>Nombre de joueurs Hordeux</div></td>
					</tr>
					<tr>
						<td align='center'><div style='font-weight:bold;'><?php echo sizeof($alliance); ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo sizeof($horde); ?></div></td>
					</tr>
				</table>
			</div>
		</center>
		<center>
			<div style='font-size:20px;margin-bottom:10px;'>R�partition par races</div>
			<div style='margin-bottom:20px;'>
				<table width='500px' cellpadding="4" cellspacing="0">
					<tr>
						<td align='center'><div style='font-weight:bold;'>Humains</div></td>
						<td align='center'><div style='font-weight:bold;'>Nains</div></td>
						<td align='center'><div style='font-weight:bold;'>Elfes de la nuit</div></td>
						<td align='center'><div style='font-weight:bold;'>Gnomes</div></td>
						<td align='center'><div style='font-weight:bold;'>Draene�s</div></td>
					</tr>
					<tr>
						<td align='center'><div style='font-weight:bold;'><?php echo $human; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $nain; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $elfe; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $gnome; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $draenei; ?></div></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td align='center'><div style='font-weight:bold;'>Orcs</div></td>
						<td align='center'><div style='font-weight:bold;'>Morts-vivants</div></td>
						<td align='center'><div style='font-weight:bold;'>Taurens</div></td>
						<td align='center'><div style='font-weight:bold;'>Trolls</div></td>
						<td align='center'><div style='font-weight:bold;'>Elfes de sang</div></td>
					</tr>
					<tr>
						<td align='center'><div style='font-weight:bold;'><?php echo $orc; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $mortvivant; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $tauren; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $troll; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $elfesang; ?></div></td>
					</tr>
				</table>
			</div>
		</center>
		<center>
			<div style='font-size:20px;margin-bottom:10px;'>R�partition par classes</div>
			<div style='margin-bottom:20px;'>
				<table width='500px' cellpadding="4" cellspacing="0">
					<tr>
						<td align='center'><div style='font-weight:bold;'>Guerriers</div></td>
						<td align='center'><div style='font-weight:bold;'>Paladins</div></td>
						<td align='center'><div style='font-weight:bold;'>Chasseurs</div></td>
						<td align='center'><div style='font-weight:bold;'>Voleurs</div></td>
						<td align='center'><div style='font-weight:bold;'>Pr�tres</div></td>
					</tr>
					<tr>
						<td align='center'><div style='font-weight:bold;'><?php echo $guerrier; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $paladin; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $chasseur; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $voleur; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $pretre; ?></div></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td align='center'><div style='font-weight:bold;'>Chevaliers de la mort</div></td>
						<td align='center'><div style='font-weight:bold;'>Chamans</div></td>
						<td align='center'><div style='font-weight:bold;'>Mages</div></td>
						<td align='center'><div style='font-weight:bold;'>D�monistes</div></td>
						<td align='center'><div style='font-weight:bold;'>Druides</div></td>
					</tr>
					<tr>
						<td align='center'><div style='font-weight:bold;'><?php echo $chevaliermort; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $chaman; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $mage; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $demoniste; ?></div></td>
						<td align='center'><div style='font-weight:bold;'><?php echo $druide; ?></div></td>
					</tr>
				</table>
			</div>
		</center>
		<div style='font-size:25px;margin-top:10px;margin-bottom:10px;color:#C60800;font-weight:bold;'>Informations sur l'uptime</div>
		<center>
			<div style='font-size:20px;margin-bottom:10px;'>Uptime serveur</div>
			<div style='font-weight:bold;'>
				Uptime actuel: <?php echo $jours.'j '.$heures.'h '.$minutes.'m'; ?>
				<br />
				Uptime maximum: <?php echo $joursmax.'j '.$heuresmax.'h '.$minutesmax.'m'; ?>
			</div>
		</center>
	</div>
</div>