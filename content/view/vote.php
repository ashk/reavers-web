<div style="width:650px;margin-bottom:5px;margin-left:15px;font-size:40px; text-align: center;">Vote</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class='cadre2'>
	<?php
	if (!empty($user->sess_id))
	{
		if (empty($_POST['site']))
		{
			if (!empty($tab_sites))
			{
				foreach($result as $state)
				{
					if ($state == 1)
					{
						$all = false;
						break;
					}
					else if ($state == 0)
					{
						$all = true;
					}
				}
				if (empty($all))
				{
					echo '
					<div style="text-align: center;">
						<form method="post" action="">
							<label>
								Choisissez un top vote:
								<select name="site">
									<optgroup label="Sites vote">
									';
									foreach($tab_sites as $key => $vote)
									{
										if ($result[$key] == 1)
										{
											echo '
											<option value="'.$key.'">'.$vote[0].'</option>
											';
										}
									}
									echo '
								</select>
							</label>
							<input type="submit" value="Valider">
							<div class="g-recaptcha" data-sitekey="6LcMpQITAAAAAOeadQ2_0Omu5RlIi1oQgcZAz9cF" style="margin-left: 25%"></div>
						</form>
					</div>
					';
				}
				else
				{
					echo '
					<br />
					<div style="text-align: center;">
						<strong>
							Vous avez déjà voté sur tous les sites de vote
							<br />
							Prochain vote disponible dans ' .$heures.'h '.$minutes. 'm
						</strong>
					</div>
					<br />
					';
				}
			}
			else
			{
				?>
				<br />
				<div style="color:#FF0000;">
					<div style="text-align: center;">Aucun site vote de configuré !</div>
				</div>
				<br />
				<?php
			}
		}
		else
		{
            if ($result['error_captcha']) {
                echo '
				<br />
				<div style="text-align: center;"><strong>Erreur captcha</strong></div>
				<br />
				';
            }
			elseif ($result == 1)
			{
				echo "<script language='javascript'>setTimeout(window.open(\"".$tab_sites[$_POST['site']][1]."\"),0);</script>";
				?>
				<br />
				<strong style="text-align: center;">Votre vote a été pris en compte !</strong>
				<br />
				<?php
			}
			else if ($result == -1)
			{
				echo '
				<br />
				<div style="text-align: center;"><strong>Vous pourrez recommencer à voter sur ce site dans ' .$heures.'h '.$minutes. 'm</strong></div>
				<br />
				';
			}
		}
	}
	else
	{
		?>
		<br />
		<div style="color:#FF0000;">
			<div style="text-align: center;">Vous devez étre connecté pour accéder à cette page !</div>
		</div>
		<br />
		<?php
	}
	?>
</div>