<?php

class Session
{
	public $session_time = 7200;//2 heures
	public $session = array();
	private $db;
	
	public function __construct($sql_host, $sql_user, $sql_password, $sql_db)
	{
		$this->host = $sql_host;
		$this->user = $sql_user;
		$this->password = $sql_password;
		$this->dba = $sql_db;
	}
	
	public function open ()//pour l'ouverture
	{
		$this->connect = mysqli_connect($this->host, $this->user, $this->password, $this->dba);

		$error = false;
		// Check connection
        if (mysqli_connect_errno($this->connect))
        {
		    echo "Failed to connect to MySQL: " . mysqli_connect_error();
            $error = true;
        }

		$this->gc();//on appelle la fonction gc		
		return $error;//true ou false selon la r�ussite ou non de la connexion � la bdd
	}
	
	public function read ($sid)//lecture
	{
        $sid = mysqli_real_escape_string($this->connect, $sid);
		$sql = "SELECT sess_datas FROM session
				WHERE sess_id = '$sid' ";

		$query = mysqli_query($this->connect, $sql);	
		$data = mysqli_fetch_array($query);
		
		if(empty($data))
		    return FALSE;
		else
		    return $data['sess_datas'];//on retourne la valeur de sess_datas
	}
	
	public function write ($sid, $data)//�criture
	{
		$expire = intval(time() + $this->session_time);//calcul de l'expiration de la session
		$data = mysqli_real_escape_string($this->connect, $data);//si on veut stocker du code sql 
		
		$sql = "SELECT COUNT(sess_id) AS total
			FROM session
			WHERE sess_id = '$sid' ";
		
		$query = mysqli_query($this->connect, $sql);
		$return = mysqli_fetch_array($query);
		if($return['total'] == 0)//si la session n'existe pas encore
		{
			$sql = "INSERT INTO session
				VALUES('$sid','$data','$expire')";//alors on la cr�e
			
		}
		else//sinon
		{
			$sql = "UPDATE session
				SET sess_datas = '$data',
				sess_expire = '$expire'
				WHERE sess_id = '$sid' ";//on la modifie
		}
		$query = mysqli_query($this->connect, $sql);
		
		return $query;
	}
	
	public function close()//fermeture
	{
		mysqli_close($this->connect);//on ferme la bdd
	}
	
	public function destroy ($sid)//destruction
	{
		$sql = "DELETE FROM session
			WHERE sess_id = '$sid' ";//on supprime la session de la bdd
		$query = mysqli_query($this->connect, $sql);
		return $query;
	}
	
	public function gc ()//nettoyage
	{
		$sql = "DELETE FROM session
				WHERE sess_expire < ".time(); //on supprime les vieilles sessions 
				
		$query = mysqli_query($this->connect, $sql);
		
		return $query;
	}
}
?>