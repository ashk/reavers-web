<?php

class Captcha {

    private $secret;

    function __construct($secret) {

        $this->secret = $secret;

    }

    function checkCode ($code) {

        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $postfields = array(
            'secret' => $this->secret,
            'response' => $code
        );

        if (function_exists('curl_version')) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 2);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            // infos pour vérification
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);

            $response = curl_exec($curl);
            curl_close($curl);
        } else {
            return false;
        }

        if (empty($response) || is_null($response)) {
            return false;
        }


        $json = json_decode($response);
        return $json->success;
    }

}