<?php

Class DatabaseHandler
{
	private $insert_id = 0;
    private $connectionLink = false;
   
    /**
     * @access public
     * @param type $hostname
     * @param type $username
     * @param type $password
     * @param type $database
     */
    public function DatabaseHandler($hostname, $username, $password, $database)
	{
        $this->connectionLink = new mysqli($hostname, $username, $password, $database);
        if($this->connectionLink->connect_error)
		{
            die('Erreur: La connexion au serveur MySQL a �chou� !');
        }
    }

    /**
     * @access public
     * @param type $query
     * @param type $assoc=false
     */
    public function query($query, $assoc=false)
	{
        $sqlQuery = $this->connectionLink->query($query);
        if(!$sqlQuery)
		{
            die('Erreur: L\'ex&eacute;cution de la requ&ecirc;te "' . $query . '" a &eacute;chou&eacute; !Erreur MySQL: ' . $this->connectionLink->error . '.');
        }
        if($assoc == false)
		{
            return $sqlQuery;
        }
		else
		{
            return $sqlQuery->fetch_assoc();
        }
    }
   
    /**
     * @access public
     * @param type $string
     */
    public function escape_string($string)
	{
        return $this->connectionLink->real_escape_string($string);
    }

	public function Get_insert_Id()
	{
		return $this->connectionLink->insert_id;
	}

    /**
     * @access public
     */
    public function __destruct()
	{
        $this->connectionLink->close();
    }
   
}

?>