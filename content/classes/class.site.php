<?php
class site
{
	public $nom_site;
	public $url;
	public $level;
	private $cDB = null;

	public function __construct(&$cDB, $nom, $url)
	{
		$this->array_users = array();
		$this->level = array(0=> "Client",1=> "Mod�rateur",2=> "Administrateur");
		$this->cDB = $cDB;
		$this->nom_site = $nom;
		$this->url = $url;
	}

	function isXMLHttpRequest()
	{
		if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] !== 'XMLHttpRequest')
			exit('!isXMLHttpRequest');
	}

	function mustBeLogin($level)
	{
		if (isset($_SESSION['id']))
		{
			if ($_SESSION['level'] < $level)
			{
				$_SESSION['error'] = "Votre niveau ne permet pas d'acceder � cette page";
			}
		}
		else
		{
			$_SESSION['error'] = "Vous devez �tre connect� pour acceder � cette page";
		}
	}

	function mustBeNotLogin()
	{
		if (isset($_SESSION['id']))
		{
			$_SESSION['error'] = "Vous devez �tre deconnect� pour acceder � cette page";
		}
	}

	function loadNbrNews()
	{
		$Conn = $this->cDB->query("SELECT COUNT(*) FROM `news` WHERE publish = 1");

		$row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadNewsWithLimit($limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->cDB->query("SELECT * FROM `news` WHERE publish = 1 ORDER BY `id` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['id'] = $val['id'];
			$format['title'] = $val['title'];
			$format['content'] = $val['content'];
			$format['publisher'] = $val['publisher'];
			$format['publisherName'] = $this->get_name_by_id($val['publisher']);
			$format['date'] = date("d-m-Y",$val['timestamp']);
			$format['heure'] = date("H:i",$val['timestamp']);
			$Conn2 = $this->cDB->query("SELECT COUNT(*) AS nombre FROM `comment` WHERE newsid = ".$val['id']."");
			$comment = $Conn2->fetch_assoc();
			$format['comment'] = $comment['nombre'];
			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function getNewsById($id)
	{
		$Conn = $this->cDB->query("SELECT * FROM `news` WHERE id = $id");
		$val = $Conn->fetch_array();
		$format['id'] = $val['id'];
		$format['content'] = $val['content'];
		$format['publisher'] = $val['publisher'];
		$format['title'] = $val['title'];
		$format['publisherName'] = $this->get_name_by_id($val['publisher']);
		$format['date'] = date("d-m-Y",$val['timestamp']);
		$format['heure'] = date("H:i",$val['timestamp']);
		return $format;
	}

	function modNewsbyId($news)
	{
		$title = trim(mysql_real_escape_string(strip_tags($news['title'])));
		$content = trim(mysql_real_escape_string(strip_tags($news['content'])));

		$Conn = $this->cDB->query("UPDATE `news` SET title = '".$title."', content = '".$content."' WHERE id = '".$news['id']."'");
	}

	function loadNbrComByNewsId($newsid)
	{
		$Conn = $this->cDB->query("SELECT COUNT(*) FROM `comment` WHERE newsid = ".$newsid."");

		$row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadCommentsWithLimit($newsid, $limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->cDB->query("SELECT * FROM `comment` WHERE newsid = ".$newsid." ORDER BY `id` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['id'] = $val['id'];
			$format['content'] = $val['content'];
			$format['publisher'] = $val['publisher'];
			$format['publisherName'] = $this->get_name_by_id($val['publisher']);
			$format['date'] = date("d-m-Y",$val['timestamp']);
			$format['heure'] = date("H:i",$val['timestamp']);
			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function sha_password($user,$pass)
	{
		$user = strtoupper($user);
		$pass = strtoupper($pass);
		return SHA1($user.':'.$pass);
	}

	function cryptme($nbr)
	{
		$str = "";
		$chaine = "a2bcd3efg6hij4kl5mn78pq9rs0tuv1wxyAZERTYUIOPQSDFGHJKLMWXCVBN";
		srand((double)microtime()*1000000);
		for($i=0; $i<$nbr; $i++)
		{
			$str .= $chaine[rand()%strlen($chaine)];
		}	
		return $str;
	}

	function passgen()
	{
		$chaine ="mnoTUzS5678kVvwxy9WXYZRNCDEFrslq41GtuaHIJKpOPQA23LcdefghiBMbj0";
		srand((double)microtime()*1000000);
		for($i=0; $i<8; $i++)
		{
			@$pass .= $chaine[rand()%strlen($chaine)];
		}
		return $pass;
	}

	function redirect($url,$time)
	{
		echo"<meta http-equiv=\"refresh\" content=\"$time;URL=$url\">";
	}

	function block_account($login, $ip)
	{
		$info = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$Conn = $this->cDB->query("SELECT id FROM ims_false WHERE login = '$login'");
		if ($Conn->num_rows > 0)
		{
			$this->cDB->query("UPDATE ims_false SET ip = '$ip', info = '$info', etat = 1, nbr = nbr + 1 WHERE login = '$login'");
		}
		else
		{
			$this->cDB->query("INSERT INTO ims_false values ('', '$login', '$ip', '$info', '1', '1')");
		}
	}

	function checkmdp($pass, $passconf)
	{
		if (empty($pass) || empty($passconf)) return 'empty';
		if ($pass != $passconf) return 'noconf';
		if(strlen($pass) < MIN_LENGHT) return 'tooshort';
		else if(strlen($pass) > MAX_LENGHT) return 'toolong';
		else if(!preg_match("/^[a-zA-Z0-9-]+$/", $pass)) return 'format';
	}

	function checkpseudo($login)
	{
		if (empty($login)) return 'empty';
		if(strlen($login) < MIN_LENGHT) return 'tooshort';
		else if(strlen($login) > MAX_LENGHT) return 'toolong';
		else if(!preg_match("/^[a-zA-Z0-9-]+$/", $login)) return 'format';

		$Conn = $this->cDB->query("SELECT login FROM users");
		while ($ligne = $Conn->fetch_array())
		{
			$loginsql = $ligne['login'];
			if($login == $loginsql)
			{
				return 'exist';
			}
		}
	}

	function checkmail($mail, $mailconf)
	{
		if (empty($mail) || empty($mailconf)) return 'empty';
		if ($mail != $mailconf) return 'noconf';
		// doit avoir la forme d'un mail !
		if(!preg_match("/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/", $mail))
		{
			return 'format';
		}

		$Conn = $this->cDB->query("SELECT email FROM users");
		while ($ligne = $Conn->fetch_array())
		{
			if (!empty($ligne['mail']))
			{
				$mailsql = $ligne['mail'];
				if($mail == $mailsql)
				{
					return 'exist';
				}
			}
		}
	}

	function sqlDetect($string)
    {
        if (preg_match('#INSERT|SELECT|UNION|FROM|WHERE|DELETE#', $string))
        {
            return true;
        }
		return false;
    }

	function get_user_by_id($id)
	{
		$Conn = $this->cDB->query("SELECT * FROM `users` WHERE id = '".$id."' LIMIT 1");
		$user = $Conn->fetch_array();
		return $user;
	}

	function get_user_by_username($username)
	{
		$this->cDB->query("SELECT * FROM `users` WHERE login = '".$username."'");
		$user = $Conn->fetch_array();
		return $user;
	}

	function get_user_by_mail($mail)
	{
		$this->cDB->query("SELECT * FROM `users` WHERE email = '".$mail."'");
		$user = $Conn->fetch_array();
		return $user;
	}

	function loadvotants($nb)
	{
		$result = array();
		if ($nb > 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `vote` ORDER BY `total` DESC LIMIT ".$nb."");
		}
		else if ($nb == 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `vote` ORDER BY `total` DESC LIMIT 50");
		}
		if (!empty($Conn))
		{
			while ($val = $Conn->fetch_array())
			{
				$result[] = $val;
			}
		}
		return $result;
	}

	function loadbugsWithLimit($cat, $status, $priority)
	{
		$requete = "SELECT * FROM `tracker` WHERE categorie = ".$cat."";
		if ($status > 0 )
		    $requete = $requete." AND `state` = ".$status."";
		if ($priority > 0)
		    $requete = $requete." AND `priority` = ".$priority."";
		$requete = $requete." ORDER BY `id` DESC";
		
		$Conn = $this->cDB->query($requete);

		$result = array();
		while ($val = $Conn->fetch_array())
		{
			$bug['firstdate'] = date("d-m-Y", $val['firstdate']);
			$bug['id'] = $val['id'];
			$bug['title'] = $val['title'];
			$bug['priority'] = $this->get_priority_by_priorityid($val['priority']);
			$bug['state'] = $this->get_state_by_stateid($val['state']);
			$result[] = $bug;
		}
		return $result;
	}

	function loadbugs($limitMin, $limitMax)
	{
		$result = array();
		$Conn = $this->cDB->query("SELECT * FROM `tracker` ORDER BY `id` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$result[] = $val;
		}
		return $result;
	}

	function loadbugsNbr($cat)
	{
		$Conn = $this->cDB->query("SELECT COUNT(*) FROM `tracker` WHERE `categorie` = ".$cat."");

		$row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function get_name_by_id($id)
	{
		$user = array();
		$Conn = $this->cDB->query("SELECT visible_name FROM `users` WHERE id = '".$id."'");
		$user = $Conn->fetch_array();
		return $user['visible_name'];
	}

	function get_bug_by_id($id)
	{
		$bug = array();
		$Conn = $this->cDB->query("SELECT * FROM `tracker` WHERE id = '".$id."'");
		$bug = $Conn->fetch_array();
		return $bug;
	}

	function load_bugcom($bugid)
	{
		$array_comment = array();
		$Conn = $this->cDB->query("SELECT * FROM `trackercomment` WHERE bugid = ".$bugid." ORDER BY timestamp DESC");
		while ($val = $Conn->fetch_array())
		{
			$array_comment[] = $val;
		}
		return $array_comment;
	}

	function loadNbrComByBugId($bugid)
	{
		$Conn = $this->cDB->query("SELECT COUNT(*) FROM `trackercomment` WHERE bugid = ".$bugid."");

		$row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadBugCommentsWithLimit($bugid, $limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->cDB->query("SELECT * FROM `trackercomment` WHERE bugid = ".$bugid." ORDER BY `id` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['id'] = $val['id'];
			$format['content'] = $val['content'];
			$format['publisher'] = $val['publisher'];
			$format['publisherName'] = $this->get_name_by_id($val['publisher']);
			$format['date'] = date("d-m-Y",$val['timestamp']);
			$format['heure'] = date("H:i",$val['timestamp']);
			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function tronquer($texte, $max_caracteres)
	{
		// Test si la longueur du texte d�passe la limite
		if (strlen($texte)>$max_caracteres)
		{
			// S�l�ction du maximum de caract�res
			$texte = substr($texte, 0, $max_caracteres);
		}
		//on retourne le texte
		return $texte;
	}

	function loadbugcomment_by_id($commentid)
	{
		$comment = array();
		$Conn = $this->cDB->query("SELECT * FROM `trackercomment` WHERE id = '".$commentid."'");
		$comment = $Conn->fetch_array();
		return $comment;
	}

	function loadcomment_by_id($commentid)
	{
		$comment = array();
		$Conn = $this->cDB->query("SELECT * FROM `comment` WHERE id = '".$commentid."'");
		$comment = $Conn->fetch_array();
		return $comment;
	}

	function getuser_by_level($level)
	{
		$user_array = array();
		$Conn = $this->cDB->query("SELECT * FROM `users` WHERE level = '".$level."'");
		while ($user = $Conn->fetch_array())
		{
			$user_array[] = $user;
		}
		return $user_array;
	}

	function get_objets_by_cat($cat)
	{
		$objet_array = array();
		$Conn = $this->cDB->query("SELECT * FROM `boutique` WHERE `cat` = '".$cat."'");
		while ($objet = $Conn->fetch_array())
		{
			$objet_array[] = $objet;
		}
		return $objet_array;
	}

	function get_object_by_id($id)
	{
		$objet = array();
		$Conn = $this->cDB->query("SELECT * FROM `boutique` WHERE `id` = '".$id."' LIMIT 1");
		$objet = $Conn->fetch_array();
		return $objet;
	}

	function get_grade_by_level($level)
	{
		$grades = array(0=> "Joueur",1=> "Moderateur", 2 => "Maitre de Jeu", 3=> "Developpeur",4=> "Administrateur");
		$grade = $grades[$level];
		if (!empty($grade))
		{
			return $grade;
		}
		return 'Inconnu';
	}

	function get_priority_by_priorityid($priorityid)
	{
		$priority_array = array(1=> "Non definie",2=> "Faible",3=> "Moyenne",4=> "Haute",5=> "Tres haute");
		$priority = $priority_array[$priorityid];
		if (!empty($priority))
		{
			return $priority;
		}
		return 'Inconnu';
	}

	function get_state_by_stateid($stateid)
	{
		$state_array = array(1=> "Nouveau",2=> "Invalide",3=> "En cours",4=> "Requiert tests",5=> "Resolu");
		$state = $state_array[$stateid];
		if (!empty($state))
		{
			return $state;
		}
		return 'Inconnu';
	}

	function getmembers()
	{
		$members = array();
		$Conn = $this->cDB->query("SELECT * FROM `users`");
		while ($member = $Conn->fetch_array())
		{
			$members[] = $member;
		}
		return $members;
	}

	function is_online($id)
	{
		$result = false;
		$Conn = $this->cDB->query("SELECT sess_id FROM `users` WHERE id = '".$id."'");
		if ($Conn->num_rows > 0)
		{
			$val = $Conn->fetch_array();
			$Conn = $this->cDB->query("SELECT sess_id FROM `session` WHERE sess_id = '".$val['sess_id']."'");
			if ($Conn->num_rows > 0)
			{
				$result = true;
			}
		}
		return $result;
	}
    
    function get_comments_by_userid($userid, $limit)
	{
		$comments = array();
		if ($limit > 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `comment` WHERE `publisher` = '".$userid."' LIMIT ".$limit."");
		}
		else
		{
			$Conn = $this->cDB->query("SELECT * FROM `comment` WHERE `publisher` = '".$userid."'");
		}
		while ($comment = $Conn->fetch_array())
		{
			$comment['date'] = date("d-m-Y",$comment['timestamp']);
			$comment['heure'] = date("H:i",$comment['timestamp']);
			$comments[] = $comment;
		}
		return $comments;
	}

	function get_bugs_by_userid($userid, $limit)
	{
		$bugs = array();
		if ($limit > 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `tracker` WHERE `publisher` = '".$userid."' LIMIT ".$limit."");
		}
		else
		{
			$Conn = $this->cDB->query("SELECT * FROM `tracker` WHERE `publisher` = '".$userid."'");
		}
		while ($bug = $Conn->fetch_array())
		{
			$bug['date'] = date("d-m-Y",$bug['firstdate']);
			$bug['heure'] = date("H:i",$bug['firstdate']);
			$bugs[] = $bug;
		}
		return $bugs;
	}
    
    function get_bugscomments_by_userid($userid, $limit)
	{
		$comments = array();
		if ($limit > 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `trackercomment` WHERE `publisher` = '".$userid."' LIMIT ".$limit."");
		}
		else
		{
			$Conn = $this->cDB->query("SELECT * FROM `trackercomment` WHERE `publisher` = '".$userid."'");
		}
		while ($comment = $Conn->fetch_array())
		{
			$comment['date'] = date("d-m-Y",$comment['timestamp']);
			$comment['heure'] = date("H:i",$comment['timestamp']);
			$comments[] = $comment;
		}
		return $comments;
	}

	function loadNbrMessages($id)
    {
        $Conn = $this->cDB->query("SELECT COUNT(*) FROM `privatemsgdial` WHERE `idobjet` = '".$id."'");

        $row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
    }

	function loadMessagesWithLimit($idObject, $limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->cDB->query("SELECT * FROM `privatemsgdial` WHERE idobjet = ".$idObject." ORDER BY `id` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['id'] = $val['id'];
			$format['content'] = $val['content'];
			$format['sender'] = $val['sender'];
			$format['senderName'] = $this->get_name_by_id($val['sender']);
			$format['date'] = date("d-m-Y",$val['timestamp']);
			$format['heure'] = date("H:i",$val['timestamp']);
			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function loadObjectByObjectId($idObject)
	{
		$Conn = $this->cDB->query("SELECT * FROM `privatemsg` WHERE id = ".$idObject."");

		$object = $Conn->fetch_array();
		return $object;
	}

	function loadObjectSendWithLimit($idUser, $limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->cDB->query("SELECT * FROM `privatemsg` WHERE sender = ".$idUser." ORDER BY `id` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['id'] = $val['id'];
			$format['objet'] = $val['objet'];
			$format['receiver'] = $val['receiver'];
			$format['receiverName'] = $this->get_name_by_id($val['receiver']);
			$format['date'] = date("d-m-Y H:i",$val['timestamp']);
			$format['lus'] = $val['lus'];
			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function loadObjectReceiverWithLimit($idUser, $limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->cDB->query("SELECT * FROM `privatemsg` WHERE receiver = ".$idUser." ORDER BY `id` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['id'] = $val['id'];
			$format['objet'] = $val['objet'];
			$format['sender'] = $val['sender'];
			$format['senderName'] = $this->get_name_by_id($val['sender']);
			$format['date'] = date("d-m-Y H:i",$val['timestamp']);
			$format['lus'] = $val['lus'];
			$formatArray[] = $format;
		}
		return $formatArray;
	}
}
?>
