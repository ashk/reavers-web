<?php
class royaume
{
	private $db_auth = null;
	private $db_characters = null;
	private $db_world = null;

	public function __construct(&$auth, &$characters, &$world)
	{
		$this->db_auth = $auth;
		$this->db_characters = $characters;
		$this->db_world = $world;
	}

	function getrealmlist()
	{
		$Conn = $this->db_auth->query("SELECT * FROM `realmlist` WHERE id = 1");
		$var = $Conn->fetch_array();
		return $var;
	}

	function getAdress()
	{
		$Conn = $this->db_auth->query("SELECT address FROM `realmlist` WHERE id = 1");
		$var = $Conn->fetch_array();
		return $var['address'];
	}

	function getPort()
	{
		$Conn = $this->db_auth->query("SELECT port FROM `realmlist` WHERE id = 1");
		$var = $Conn->fetch_array();
		return $var['port'];
	}

	function getuptime()
	{
		$Conn = $this->db_auth->query("SELECT * FROM `uptime` WHERE realmid = 1 ORDER BY `starttime` DESC LIMIT 1");
		$var = $Conn->fetch_array();
		return $var;
	}

	function getuptimemax()
	{
		$Conn = $this->db_auth->query("SELECT * FROM `uptime` WHERE realmid = 1 ORDER BY `uptime` DESC LIMIT 1");
		$var = $Conn->fetch_array();
		return $var;
	}

	function playermax()
	{
		$Conn = $this->db_auth->query("SELECT * FROM `uptime` WHERE realmid = 1 ORDER BY `maxplayers` DESC LIMIT 1");
		$var = $Conn->fetch_array();
		return $var;
	}

	function isonline()
	{
		$realmlist = $this->getrealmlist();
		if(!$sock = @fsockopen($realmlist['address'], $realmlist['port'], $num, $error, 1))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	function isonline_by_character($guid)
	{
		$Conn = $this->db_characters->query("SELECT online FROM `characters` WHERE `guid` = ".$guid."");
		$var = $Conn->fetch_array();
		if ($var['online'] == '1')
		{
			return true;
		}
		return false;
	}

	function getonline($faction)
	{
		$result = array();
		if ($faction == 0)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE `online` = 1");
		}
		else if ($faction == 1)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE `online` = 1 AND (`race` = 1 OR `race` = 3 OR `race` = 4 OR `race` = 7 OR `race` = 11)");
		}
		else if ($faction == 2)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE `online` = 1 AND (`race` = 2 OR `race` = 5 OR `race` = 6 OR `race` = 8 OR `race` = 10)");
		}
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$Conn2 = $this->db_world->query("SELECT * FROM `zones` WHERE id = ".$var['zone']."");
				$var2 = $Conn2->fetch_array();
				$var['zone_categorie'] = $var2['categorie'];
				$var['zone_zone'] = $var2['zone'];
				$result[] = $var;
			}
		}
		return $result;
	}

	function loadNbrOnline()
	{
		$Conn = $this->db_characters->query("SELECT COUNT(*) FROM `characters` WHERE `online` = 1");

        $row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadOnlineWithLimit($limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE online = 1 ORDER BY `guid` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$Conn2 = $this->db_world->query("SELECT * FROM `zones` WHERE id = ".$val['zone']."");
			$var2 = $Conn2->fetch_array();

			$format['guid'] = $val['guid'];
			$format['name'] = $val['name'];
			$format['level'] = $val['level'];
			$format['race'] = $val['race'];
			$format['gender'] = $val['gender'];
			$format['class'] = $val['class'];
			$format['zone_categorie'] = $var2['categorie'];
			$format['zone_zone'] = $var2['zone'];

			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function getplayers($faction)
	{
		$result = array();
		if ($faction == 0)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters`");
		}
		else if ($faction == 1)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE `race` = 1 OR `race` = 3 OR `race` = 4 OR `race` = 7 OR `race` = 11");
		}
		else if ($faction == 2)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE `race` = 2 OR `race` = 5 OR `race` = 6 OR `race` = 8 OR `race` = 10");
		}
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$result[] = $var;
			}
		}
		return $result;
	}

	function getNbrPlayers()
	{
		$Conn = $this->db_characters->query("SELECT COUNT(*) FROM `characters`");

        $row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadPlayersWithLimit($limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->db_characters->query("SELECT * FROM `characters` ORDER BY `guid` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['guid'] = $val['guid'];
			$format['name'] = $val['name'];
			$format['level'] = $val['level'];
			$format['race'] = $val['race'];
			$format['class'] = $val['class'];
			$format['gender'] = $val['gender'];
			$format['online'] = $val['online'];

			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function getaccount()
	{
		$var = array();
		$var['nombre'] = 0;
		$Conn = $this->db_auth->query("SELECT COUNT(*) AS nombre FROM `account`");
		if ($Conn->num_rows > 0)
		{
			$var = $Conn->fetch_assoc();
		}
		return $var['nombre'];
	}

	function get_account_by_userid($userid)
	{
		$result = array();
		$Conn = $this->db_auth->query("SELECT * FROM `account` WHERE `id` = '".$userid."' LIMIT 1");
		$result = $Conn->fetch_array();
		return $result;
	}

	function is_banned_by_ip($ip)
	{
		$Conn = $this->db_auth->query("SELECT * FROM `ip_banned` WHERE ip = '".$ip."'");
		if ($Conn->num_rows > 0)
		{
			$banned = $Conn->fetch_array();
			return $banned;
		}
		return false;
	}

	function is_banned_by_id($id)
	{
		$Conn = $this->db_auth->query("SELECT * FROM `account_banned` WHERE id = '".$id."'");
		if ($Conn->num_rows > 0)
		{
			$banned = $Conn->fetch_array();
			return $banned;
		}
		return false;
	}

	function loadarenes($nb, $type)
	{
		$result = array();
		if ($nb > 0)
			$Conn = $this->db_characters->query("SELECT * FROM `arena_team` WHERE `type` = ".$type." ORDER BY `rank` ASC LIMIT ".$nb."");
		else if ($nb == 0)
			$Conn = $this->db_characters->query("SELECT * FROM `arena_team` WHERE `type` = ".$type." ORDER BY `rank` ASC LIMIT 50");

		if (!empty($Conn))
		{
			while ($val = $Conn->fetch_array())
			{
				$result[] = $val;
			}
		}
		return $result;
	}

	function getNbrTeams($type)
	{
		$Conn = $this->db_characters->query("SELECT COUNT(*) FROM `arena_team` WHERE `type` = ".$type."");

        $row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadTeamsWithLimit($type, $limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->db_characters->query("SELECT * FROM `arena_team` WHERE `type` = ".$type." ORDER BY `arenaTeamId` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['arenaTeamId'] = $val['arenaTeamId'];
			$format['name'] = $val['name'];
			$format['captainGuid'] = $val['captainGuid'];
			$format['captainName'] = $this->get_name_by_guid($val['captainGuid']);
			$format['rating'] = $val['rating'];

			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function loadhonneur($nb)
	{
		$result = array();
		if ($nb > 0)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters` ORDER BY `totalKills` DESC LIMIT ".$nb."");
		}
		else if ($nb == 0)
		{
			$Conn = $this->db_characters->query("SELECT * FROM `characters` ORDER BY `totalKills` DESC LIMIT 50");
		}
		if (!empty($Conn))
		{
			while ($val = $Conn->fetch_array())
			{
				$result[] = $val;
			}
		}
		return $result;
	}

	function get_guidachievement()
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `character_achievement`");
		while ($val = $Conn->fetch_array())
		{
			$result[$val['guid']][] = $val['achievement'];
		}
		return $result;
	}

	function get_name_by_guid($guid)
	{
		$var['name'] = array();
		$Conn = $this->db_characters->query("SELECT name FROM `characters` WHERE `guid` = ".$guid."");
		if ($Conn->num_rows > 0)
		{
			$var = $Conn->fetch_array();
		}
		return $var['name'];
	}

	function get_character_guid($guid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE `guid` = ".$guid."");
		if ($Conn->num_rows > 0)
		{
			$result = $Conn->fetch_array();
		}
		return $result;
	}

	function getguilds()
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `guild`");
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$result[] = $var;
			}
		}
		return $result;
	}

	function getNbrGuilds()
	{
		$Conn = $this->db_characters->query("SELECT COUNT(*) FROM `guild`");

        $row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadGuildsWithLimit($limitMin, $limitMax)
	{
		$formatArray = array();

		$Conn = $this->db_characters->query("SELECT * FROM `guild` ORDER BY `guildid` DESC LIMIT ".$limitMin.",".$limitMax."");

		while ($val = $Conn->fetch_array())
		{
			$format['guildid'] = $val['guildid'];
			$format['name'] = $val['name'];
			$format['leaderguid'] = $val['leaderguid'];
			$format['leaderName'] = $this->get_name_by_guid($val['leaderguid']);
			$format['date'] = date("j-m-Y", $val['createdate']);

			$formatArray[] = $format;
		}
		return $formatArray;
	}

	function getfaction($race)
	{
		$factionarray = array(1=> "alliance",2=> "horde",3=> "alliance",4=> "alliance",5=> "horde",6=> "horde",7=> "alliance",8=> "horde",10=> "horde",11=> "alliance");
		foreach ($factionarray as $key => $faction)
		{
			if ($key == $race)
			{
				return $faction;
			}
		}
		return false;
	}

	function get_itemcharacter_by_guid($guid)
	{
		for ($i = 0; $i < 19; $i++)
		{
			$row_item[$i] = array();
			$Conn = $this->db_characters->query("SELECT item FROM `character_inventory` WHERE `slot` = '".$i."' AND `bag` = '0' AND `guid` = '".$guid."' LIMIT 1");
			if ($Conn->num_rows > 0)
			{
				$slot = $Conn->fetch_array();
				$Conn = $this->db_characters->query("SELECT * FROM `item_instance` WHERE `guid` = '".$slot['item']."' LIMIT 1");
				$row_item[$i] = $Conn->fetch_array();
				$Conn = $this->db_world->query("SELECT * FROM `item_template` WHERE `entry` = '".$row_item[$i]['itemEntry']."' LIMIT 1");
				$result = $Conn->fetch_array();
				$row_item[$i]['Quality'] = 0;
				$row_item[$i]['displayid'] = 0;
				$row_item[$i]['Quality'] = $result['Quality'];
				$row_item[$i]['displayid'] = $result['displayid'];
				
			}
		}
		return $row_item;
	}

	function getgenderstring($gender)
	{
		if ($gender == 0)
		{
			return 'male';
		}
		elseif ($gender == 1)
		{
			return 'female';
		}
		return 'unknown';
	}

	function getracestring($race)
	{
		if ($race == 1)
		{
			return 'human';
		}
		elseif ($race == 2)
		{
			return 'orc';
		}
		elseif ($race == 3)
		{
			return 'dwarf';
		}
		elseif ($race == 4)
		{
			return 'nightelf';
		}
		elseif ($race == 5)
		{
			return 'undead';
		}
		elseif ($race == 6)
		{
			return 'tauren';
		}
		elseif ($race == 7)
		{
			return 'gnome';
		}
		elseif ($race == 8)
		{
			return 'troll';
		}
		elseif ($race == 10)
		{
			return 'bloodelf';
		}
		elseif ($race == 11)
		{
			return 'draenei';
		}
		return 'unknown';
	}

	function getracestringfr($race)
	{
		if ($race == 1)
		{
			return 'Humain';
		}
		elseif ($race == 2)
		{
			return 'Orc';
		}
		elseif ($race == 3)
		{
			return 'Nain';
		}
		elseif ($race == 4)
		{
			return 'Elfe de la nuit';
		}
		elseif ($race == 5)
		{
			return 'Mort-vivant';
		}
		elseif ($race == 6)
		{
			return 'Tauren';
		}
		elseif ($race == 7)
		{
			return 'Gnome';
		}
		elseif ($race == 8)
		{
			return 'Troll';
		}
		elseif ($race == 10)
		{
			return 'Elfe de sang';
		}
		elseif ($race == 11)
		{
			return 'Draenei';
		}
		return 'unknown';
	}

	function getclassstringfr($class)
	{
		if ($class == 1)
		{
			return 'Guerrier';
		}
		elseif ($class == 2)
		{
			return 'Paladin';
		}
		elseif ($class == 3)
		{
			return 'Chasseur';
		}
		elseif ($class == 4)
		{
			return 'Voleur';
		}
		elseif ($class == 5)
		{
			return 'Pr�tre';
		}
		elseif ($class == 6)
		{
			return 'Chevalier de la mort';
		}
		elseif ($class == 7)
		{
			return 'Chaman';
		}
		elseif ($class == 8)
		{
			return 'Mage';
		}
		elseif ($class == 9)
		{
			return 'D�moniste';
		}
		elseif ($class == 11)
		{
			return 'Druide';
		}
		return 'unknown';
	}

	function get_icon_by_itemguid($itemguid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT itemEntry FROM `item_instance` WHERE `guid` = '".$itemguid."' LIMIT 1");
		$item = $Conn->fetch_array();
		$Conn = $this->db_world->query("SELECT displayid FROM `item_template` WHERE `entry` = '".$item['itemEntry']."' LIMIT 1");
		$display = $Conn->fetch_array();
		if (!empty($display['displayid']))
		{
			$Conn = $this->db_world->query("SELECT * FROM `armory_icons` WHERE `displayid` = ".$display['displayid']." LIMIT 1");
			$result = $Conn->fetch_array();
		}
		return $result;
	}

	function getguild_by_guidcharacter($guid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT guildid FROM `guild_member` WHERE `guid` = ".$guid." LIMIT 1");
		$guildid = $Conn->fetch_array();
		if (!empty($guildid))
		{
			$Conn = $this->db_characters->query("SELECT * FROM `guild` WHERE `guildid` = ".$guildid['guildid']." LIMIT 1");
			$result = $Conn->fetch_array();
		}
		return $result;
	}

	function getguildmember_by_memberguid($guildid, $guid)
	{
		$result = array();
		if (!empty($guildid))
		{
			$Conn = $this->db_characters->query("SELECT * FROM `guild_member` WHERE `guildid` = ".$guildid." AND `guid` = ".$guid." LIMIT 1");
			$result = $Conn->fetch_array();
		}
		return $result;
	}

	function getnamerank_by_rankid($guildid, $rank)
	{
		$result = array();
		if (!empty($guildid))
		{
			$Conn = $this->db_characters->query("SELECT rname FROM `guild_rank` WHERE `rid` = ".$rank." AND `guildid` = ".$guildid." LIMIT 1");
			$result = $Conn->fetch_array();
		}
		return $result['rname'];
	}

	function get_stats_by_guidcharacter($guid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `character_stats` WHERE `guid` = '".$guid."' LIMIT 1");
		$result = $Conn->fetch_array();
		return $result;
	}

	function getstatmember_by_guid($type, $guid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT `arena_team_member`.`weekGames`, `arena_team_member`.`weekWins`, `arena_team_member`.`seasonGames`, `arena_team_member`.`seasonWins`, `arena_team_member`.`personalRating`, `arena_team_member`.`arenaTeamId` 
											FROM `arena_team_member`, `arena_team` 
											WHERE `arena_team_member`.`arenaTeamId` = `arena_team`.`arenaTeamId` AND `arena_team_member`.`guid` = '".$guid."' AND `arena_team`.`type` = ".$type."");
		$result = $Conn->fetch_array();
		return $result;
	}

	function getarenateam_by_arenaTeamId($arenaTeamId)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `arena_team` WHERE `arenaTeamId` = '".$arenaTeamId."' LIMIT 1");
		$result = $Conn->fetch_array();
		return $result;
	}

	function getguild_by_guildid($guildid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `guild` WHERE `guildid` = '".$guildid."' LIMIT 1");
		$result = $Conn->fetch_array();
		return $result;
	}

	function getguildmember_by_guildid($guildid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `guild_member` WHERE `guildid` = '".$guildid."'");
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$result[] = $var;
			}
		}
		return $result;
	}

	function getstatmember_by_arenaTeamId($arenaTeamId)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `arena_team_member` WHERE `arenaTeamId` = '".$arenaTeamId."'");
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$result[] = $var;
			}
		}
		return $result;
	}

	function get_accounts()
	{
		$accounts = array();
		$Conn = $this->db_auth->query("SELECT * FROM `account`");
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$accounts[] = $var;
			}
		}
		return $accounts;
	}
    
    function get_characters_by_userid($userid)
	{
		$result = array();
		$Conn = $this->db_characters->query("SELECT * FROM `characters` WHERE `account` = '".$userid."'");
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$result[] = $var;
			}
		}
		return $result;
	}
}
?>
