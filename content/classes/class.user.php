<?php
class user
{
	var $sess_id;
	var $last_access;
	var $ipaddr;
	var $array_user;
	var $array_points;
	var $array_account;
	private $cDB = null;
	private $auth = null;
    private $characters = null;

	public function __construct(&$cDB, &$auth, &$characters)
	{
		$this->ipaddr = $_SERVER['REMOTE_ADDR'];
		$this->array_user = array();
		$this->array_account = array();
		$this->array_points = array();
		$this->last_access = time();
		$this->cDB = $cDB;
		$this->auth = $auth;
        $this->characters = $characters;
		$this->sess_id = 0;
	}

	function isLogged()
	{
		if (!empty($this->sess_id))
			return 1;

		return 0;
	}

	function load_account()
	{
		$Conn = $this->cDB->query("SELECT * FROM `points` WHERE id = '".$this->sess_id."'");
		$this->array_points = $Conn->fetch_array();

		$Conn = $this->cDB->query("SELECT * FROM `users` WHERE id = '".$this->sess_id."'");
		$this->array_user = $Conn->fetch_array();
		$Conn = $this->auth->query("SELECT * FROM `account` WHERE id = '".$this->sess_id."'");
		$this->array_account = $Conn->fetch_array();
	}

	function unload_account()
	{
		$this->array_user = array();
		$this->array_account = array();
		$this->array_points = array();
		$this->sess_id = 0;
	}
	
	function set_id($id)
	{
		$Conn = $this->cDB->query("SELECT id FROM `users` WHERE id = '".$id."'");
		if ($Conn->num_rows > 0)
		{
			$this->sess_id = $id;
			return true;
		}
		return false;
	}

	function checkbugcomment($commentid)
	{
		$result = false;
		$Conn = $this->cDB->query("SELECT publisher FROM `trackercomment` WHERE id = '".$commentid."'");
		$val = $Conn->fetch_array();
		if ((!empty($this->array_user['login']) && $this->array_user['login'] == $val['publisher']) || (!empty($this->array_user['level']) && $this->array_user['level'] >= 3))
		{
			$result = true;
		}
		return $result;
	}

	function checkcomment($commentid)
	{
		$result = false;
		$Conn = $this->cDB->query("SELECT publisher FROM `comment` WHERE id = '".$commentid."'");
		$val = $Conn->fetch_array();
		if ((!empty($this->array_user['login']) && $this->array_user['login'] == $val['publisher']) || (!empty($this->array_user['level']) && $this->array_user['level'] >= 3))
		{
			$result = true;
		}
		return $result;
	}

	function checkbug($bugid)
	{
		$result = false;
		$Conn = $this->cDB->query("SELECT publisher FROM `tracker` WHERE id = '".$bugid."'");
		$val = $Conn->fetch_array();

		if ((!empty($this->array_user['id']) && $this->array_user['id'] == $val['publisher']) || (!empty($this->array_user['level']) && $this->array_user['level'] >= 3))
		{
			$result = true;
		}
		return $result;
	}

	function checknews($newsid)
	{
		$result = false;
		$Conn = $this->cDB->query("SELECT publisher FROM `news` WHERE id = '".$newsid."'");
		$val = $Conn->fetch_array();
		if (!empty($this->array_user['level']) && $this->array_user['level'] >= 3)
		{
			$result = true;
		}
		return $result;
	}

	function changepoints($points)
	{
		$newpoints = $this->array_points['points'] + $points;
		$this->cDB->query("UPDATE points SET points = '".$newpoints."' WHERE id = '".$this->sess_id."'");
	}

	function is_online()
	{
		$result = false;
		$Conn = $this->cDB->query("SELECT sess_id FROM `users` WHERE id = '".$this->sess_id."'");
		if ($Conn->num_rows > 0)
		{
			$val = $Conn->fetch_array();
			$Conn = $this->cDB->query("SELECT sess_id FROM `session` WHERE sess_id = '".$val['sess_id']."'");
			if ($Conn->num_rows > 0)
			{
				$result = true;
			}
		}
		return $result;
	}

	function is_banned_by_ip()
	{
		$result = array();
		$Conn = $this->auth->query("SELECT ip FROM `ip_banned` WHERE ip = '".$this->array_account['last_ip']."'");
		if ($Conn->num_rows > 0)
		{
			$result = $Conn->fetch_array();
		}
		return $result;
	}

	function is_banned_by_account()
	{
		$result = array();
		$Conn = $this->auth->query("SELECT id FROM `account_banned` WHERE id = '".$this->sess_id."' AND active = 1");
		if ($Conn->num_rows > 0)
		{
			$result = $Conn->fetch_array();
		}
		return $result;
	}

	function getmsgnonlus()
	{
		$msgs = array();
		$Conn = $this->cDB->query("SELECT * FROM `privatemsg` WHERE (receiver = ".$this->sess_id." OR sender = ".$this->sess_id.") AND lus = 0");
		while ($var = $Conn->fetch_array())
		{
			$msgs[] = $var;
		}
		return $msgs;
	}

	function getlevel()
	{
		$level = 'Joueur';
		$Conn = $this->cDB->query("SELECT * FROM `users` WHERE id = '".$this->sess_id."'");
		if ($Conn->num_rows > 0)
		{
			$var = $Conn->fetch_array();
			if ($var['level'] == 1)
			{
				$level = 'Moderateur';
			}
			else if ($var['level'] == 2)
			{
				$level = 'Maitre de Jeu';
			}
            else if ($var['level'] == 3)
			{
				$level = 'Developpeur';
			}
			else if ($var['level'] == 4)
			{
				$level = 'Administrateur';
			}
		}
		return $level;
	}
    
    function savecomment($newsid, $content)
	{
		$content = trim($this->cDB->escape_string(strip_tags($content)));
		$time = time();
		$this->cDB->query("INSERT INTO comment (`newsid`, `publisher`, `content`, `timestamp`) VALUES ('".$newsid."', '".$this->sess_id."', '".$content."', '".$time."')");
	}

	function savebugcomment($bugid, $content)
	{
		$content = trim($this->cDB->escape_string(strip_tags($content)));
		$time = time();
		$this->cDB->query("INSERT INTO trackercomment (`bugid`, `publisher`, `content`, `timestamp`) VALUES ('".$bugid."', '".$this->sess_id."', '".$content."', '".$time."')");
	}
    
    function savebug($content)
	{
		$name = trim($this->cDB->escape_string(strip_tags($content['name'])));
		$categorie = trim($this->cDB->escape_string(strip_tags($content['categorie'])));
		$lienwowhead = trim($this->cDB->escape_string(strip_tags($content['lienwowhead'])));
		$desc_offi = trim($this->cDB->escape_string(strip_tags($content['desc_offi'])));
		$desc_serv = trim($this->cDB->escape_string(strip_tags($content['desc_serv'])));
		$time = time();
		$this->cDB->query("INSERT INTO tracker (`state`, `priority`, `categorie`, `title`, `lienwowhead`, `desc_offi`, `desc_serv`, `firstdate`, `publisher`) VALUES (1, 1, '".$categorie."', '".$name."', '".$lienwowhead."', '".$desc_offi."', '".$desc_serv."', '".$time."', '".$this->sess_id."')");
	}

    function get_comments($limit)
	{
		$comments = array();
		if ($limit > 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `comment` WHERE `publisher` = '".$this->sess_id."' LIMIT ".$limit."");
		}
		else
		{
			$Conn = $this->cDB->query("SELECT * FROM `comment` WHERE `publisher` = '".$this->sess_id."'");
		}
		while ($comment = $Conn->fetch_array())
		{
			$comment['date'] = date("d-m-Y",$comment['timestamp']);
			$comment['heure'] = date("H:i",$comment['timestamp']);
			$comments[] = $comment;
		}
		return $comments;
	}

	function get_bugs($limit)
	{
		$bugs = array();
		if ($limit > 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `tracker` WHERE `publisher` = '".$this->sess_id."' LIMIT ".$limit."");
		}
		else
		{
			$Conn = $this->cDB->query("SELECT * FROM `tracker` WHERE `publisher` = '".$this->sess_id."'");
		}
		while ($bug = $Conn->fetch_array())
		{
			$bug['date'] = date("d-m-Y",$bug['firstdate']);
			$bug['heure'] = date("H:i",$bug['firstdate']);
			$bugs[] = $bug;
		}
		return $bugs;
	}
    
    function get_bugscomments($limit)
	{
		$comments = array();
		if ($limit > 0)
		{
			$Conn = $this->cDB->query("SELECT * FROM `trackercomment` WHERE `publisher` = '".$this->sess_id."' LIMIT ".$limit."");
		}
		else
		{
			$Conn = $this->cDB->query("SELECT * FROM `trackercomment` WHERE `publisher` = '".$this->sess_id."'");
		}
		while ($comment = $Conn->fetch_array())
		{
			$comment['date'] = date("d-m-Y",$comment['timestamp']);
			$comment['heure'] = date("H:i",$comment['timestamp']);
			$comments[] = $comment;
		}
		return $comments;
	}
    
    function get_characters()
	{
		$result = array();
		$Conn = $this->characters->query("SELECT * FROM `characters` WHERE `account` = '".$this->sess_id."'");
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$result[] = $var;
			}
		}
		return $result;
	}

    function get_characters_other() // characters d'un compte (profil)
	{
        $variable = (int) $_GET['variable1'];
		$result = array();
		$Conn = $this->characters->query("SELECT * FROM `characters` WHERE `account` = '".$variable."'");
		if ($Conn->num_rows > 0)
		{
			while($var = $Conn->fetch_array())
			{
				$result[] = $var;
			}
		}
		return $result;
	}
    
	function loadNbrObjectSend()
	{
		$Conn = $this->cDB->query("SELECT COUNT(*) FROM `privatemsg` WHERE sender = '".$this->sess_id."'");

		$row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

	function loadNbrObjectReceive()
	{
		$Conn = $this->cDB->query("SELECT COUNT(*) FROM `privatemsg` WHERE receiver = '".$this->sess_id."'");

		$row = $Conn->fetch_array(MYSQLI_NUM);
		return $row[0];
	}

    function isprivateuser($id)
    {
        $Conn = $this->cDB->query("SELECT * FROM `privatemsg` WHERE id = '".$id."'");
        if ($Conn->num_rows > 0)
		{
			$var = $Conn->fetch_array();
            if ($var['receiver'] == $this->sess_id || $var['sender'] == $this->sess_id)
                return true;
        }

		$_SESSION['error'] = "Erreur d'identification";

        return false;
    }

    function saveprivatemsg($idobjet, $msg)
    {
        $Conn = $this->cDB->query("SELECT * FROM `privatemsg` WHERE id = '".$idobjet."'");
        if ($Conn->num_rows > 0)
		{
			$result = $Conn->fetch_array();
            if ($result['sender'] = $this->sess_id)
                $receiver = $result['receiver'];
            else
                $receiver = $result['sender'];
                
            $content = trim($this->cDB->escape_string(strip_tags($msg)));
		    $time = time();
            
            $Conn = $this->cDB->query("INSERT INTO `privatemsgdial` VALUES ('', '".$idobjet."', '".$content."', '".$this->sess_id."', '".$receiver."', 0, '".$time."')");
		}
    }
    
    function saveobjetmsg($receiver, $objet)
    {
        $objet = trim($this->cDB->escape_string(strip_tags($objet)));
		$time = time();
        $Conn = $this->cDB->query("INSERT INTO `privatemsg` VALUES ('', '".$objet."', '".$this->sess_id."', '".$receiver."', 0, '".$time."')");
        $resid = $this->cDB->Get_insert_Id();
        return $resid;
    }
    
    function setlus($idobjet)
    {
        $Conn = $this->cDB->query("UPDATE `privatemsg` SET lus = 1 WHERE id = '".$idobjet."'");
    }
    
    function ispassword($password)
    {
        $oldpass = $site->sha_password($this->array_user['login'], $password);
		if ($oldpass == $user->array_user['password'])
		{
            return true;
        }
        return false;
    }
    
    function changepassword($newpassword)
    {
        $cryptpass = $site->sha_password($this->array_user['login'], $newpassword);
        $Conn = $this->auth->query("UPDATE account SET v = 0, s = 0, sha_pass_hash = '".$cryptpass."' WHERE id = '".$this->sess_id."'");
        $Conn = $this->cDB->query("UPDATE `users` SET password = '".$cryptpass."' WHERE id = '".$this->sess_id."'");
    }
    
    function ismail($mail)
    {
		if ($mail == $user->array_user['email'])
		{
            return true;
        }
        return false;
    }
    
    function changemail($mail)
    {
        $Conn = $this->auth->query("UPDATE account SET v = 0, s = 0, email = '".$mail."' WHERE id = '".$this->sess_id."'");
        $Conn = $this->cDB->query("UPDATE `users` SET email = '".$mail."' WHERE id = '".$this->sess_id."'");
    }
    
    function changevisiblename($newvisiblename)
    {
        $Conn = $this->cDB->query("UPDATE `users` SET visible_name = '".$newvisiblename."' WHERE id = '".$this->sess_id."'");
    }
    
    function changerace($guid)
    {
        $Conn = $this->characters->query("UPDATE characters SET at_login = at_login | 128 WHERE `guid` = '".$guid."'");
    }
    
    function renommer($guid)
    {
        $Conn = $this->characters->query("UPDATE characters SET at_login = at_login | 1 WHERE `guid` = '".$guid."'");
    }
    
    function customize($guid)
    {
        $Conn = $this->characters->query("UPDATE characters SET at_login = at_login | 8 WHERE `guid` = '".$guid."'");
    }
    
    function faction($guid)
    {
        $Conn = $this->characters->query("UPDATE characters SET at_login = at_login | 64 WHERE `guid` = '".$guid."'");
    }
}
?>
