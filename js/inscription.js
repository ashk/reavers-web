function deactivateTooltips()
{
	var spans = document.getElementsByTagName('span'), spansLength = spans.length;
	for (var i = 0 ; i < spansLength ; i++)
	{
		if (spans[i].className == 'tooltip')
		{
			spans[i].style.display = 'none';
		}
	}
	return spans;
}

function substr_count (haystack, needle, offset, length)
{
    // Returns the number of times a substring occurs in the string
    // 
    // version: 1103.1210
    // discuss at: http://phpjs.org/functions/substr_count    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // *     example 1: substr_count('Kevin van Zonneveld', 'e');
    // *     returns 1: 3
    // *     example 2: substr_count('Kevin van Zonneveld', 'K', 1);    // *     returns 2: 0
    // *     example 3: substr_count('Kevin van Zonneveld', 'Z', 0, 10);
    // *     returns 3: false
    var pos = 0,
        cnt = 0; 
    haystack += '';
    needle += '';
    if (isNaN(offset)) {
        offset = 0;    }
    if (isNaN(length)) {
        length = 0;
    }
    offset--; 
    while ((offset = haystack.indexOf(needle, offset + 1)) != -1) {
        if (length > 0 && (offset + needle.length) > length) {
            return false;
        } else {            cnt++;
        }
    }
    return cnt;
}

function explode (delimiter, string, limit)
{
    // Splits a string on string separator and return array of components. If limit is positive only limit number of components is returned. If limit is negative all components except the last abs(limit) are returned.  
    // 
    // version: 1103.1210
    // discuss at: http://phpjs.org/functions/explode    // +     original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: kenneth
    // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: d3x
    // +     bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)    // *     example 1: explode(' ', 'Kevin van Zonneveld');
    // *     returns 1: {0: 'Kevin', 1: 'van', 2: 'Zonneveld'}
    // *     example 2: explode('=', 'a=bc=d', 2);
    // *     returns 2: ['a', 'bc=d']
    var emptyArray = {        0: ''
    };
 
    // third argument is not required
    if (arguments.length < 2 || typeof arguments[0] == 'undefined' || typeof arguments[1] == 'undefined') {        return null;
    }
 
    if (delimiter === '' || delimiter === false || delimiter === null) {
        return false;    }
 
    if (typeof delimiter == 'function' || typeof delimiter == 'object' || typeof string == 'function' || typeof string == 'object') {
        return emptyArray;
    } 
    if (delimiter === true) {
        delimiter = '1';
    }
     if (!limit) {
        return string.toString().split(delimiter.toString());
    } else {
        // support for limit argument
        var splitted = string.toString().split(delimiter.toString());        var partA = splitted.splice(0, limit - 1);
        var partB = splitted.join(delimiter.toString());
        partA.push(partB);
        return partA;
    }
}

// Fonctions de v�rification du formulaire, elles renvoient "true" si tout est ok

var check = []; // On met toutes nos fonctions dans un tableau associatif

check['login'] = function()
{
	var name = document.getElementById('login'), tooltipStyle, tooltip1, tooltip2, tooltipStyle2, tooltip3, tooltipStyle3, reg = /^[a-zA-Z0-9]+$/, pseudo, result;
	pseudo = name.value;
	tooltip1 = document.getElementById('pseudo1');
	tooltipStyle1 = tooltip1.style;
	tooltip2 = document.getElementById('pseudo2');
	tooltipStyle2 = tooltip2.style;
	tooltip3 = document.getElementById('pseudo3');
	tooltipStyle3 = tooltip3.style;
	result = reg.test(pseudo);
	if (result && pseudo.length >= 6 && pseudo.length < 32)
	{
		name.className = 'correct';
		tooltipStyle1.display = 'none';
		tooltipStyle2.display = 'none';
		tooltipStyle3.display = 'none';
		return true;
	}
	if (pseudo == "")
	{
		name.className = 'incorrect';
		tooltipStyle1.display = 'none';
		tooltipStyle2.display = 'none';
		tooltipStyle3.display = 'none';
		return false;
	}
	else if (!result)
	{
		name.className = 'incorrect';
		tooltipStyle1.display = 'none';
		tooltipStyle2.display = 'none';
		tooltipStyle3.display = 'inline-block';
		return false;
	}
	else if (pseudo.length < 6)
	{
		name.className = 'incorrect';
		tooltipStyle1.display = 'inline-block';
		tooltipStyle2.display = 'none';
		tooltipStyle3.display = 'none';
		return false;
	}
	else if (pseudo.length > 32)
	{
		name.className = 'incorrect';
		tooltipStyle1.display = 'none';
		tooltipStyle2.display = 'inline-block';
		tooltipStyle3.display = 'none';
		return false;
	}
};

check['pass'] = function()
{
	var pwd1 = document.getElementById('pass'), tooltip1, tooltipStyle1, tooltip2, tooltipStyle2, tooltip3, tooltipStyle3, result1, reg1 = /^[a-zA-Z0-9]+$/;
	tooltip1 = document.getElementById('pass1');
	tooltipStyle1 = tooltip1.style;
	tooltip2 = document.getElementById('pass2');
	tooltipStyle2 = tooltip2.style;
	tooltip3 = document.getElementById('pass3');
	tooltipStyle3 = tooltip3.style;
	result1 = reg1.test(pwd1.value);
	if (result1 && pwd1.value.length >= 6 && pwd1.value.length < 32)
	{
		pwd1.className = 'correct';
		tooltipStyle1.display = 'none';
		tooltipStyle2.display = 'none';
		tooltipStyle3.display = 'none';
		return true;
	}
	if (pwd1.value == "")
	{
		pwd1.className = 'incorrect';
		tooltipStyle1.display = 'none';
		tooltipStyle2.display = 'none';
		tooltipStyle3.display = 'none';
		return false;
	}
	else if (!result1)
	{
		pwd1.className = 'incorrect';
		tooltipStyle2.display = 'none';
		tooltipStyle1.display = 'none';
		tooltipStyle3.display = 'inline-block';
		return false;
	}
	else if (pwd1.value.length < 6)
	{
		pwd1.className = 'incorrect';
		tooltipStyle1.display = 'inline-block';
		tooltipStyle2.display = 'none';
		tooltipStyle3.display = 'none';
		return false;
	}
	else if (pwd1.value.length > 32)
	{
		pwd1.className = 'incorrect';
		tooltipStyle2.display = 'inline-block';
		tooltipStyle1.display = 'none';
		tooltipStyle3.display = 'none';
		return false;
	}
};

check['passconf'] = function()
{
	var pwd1 = document.getElementById('pass'), pwd2 = document.getElementById('passconf'), tooltip1, tooltipStyle1;
	tooltip1 = document.getElementById('passconf1');
	tooltipStyle1 = tooltip1.style;
	if (pwd2.value != '' && pwd1.value == pwd2.value)
	{
		pwd2.className = 'correct';
		tooltipStyle1.display = 'none';
		return true;
	}
	if (pwd2.value == "")
	{
		pwd2.className = 'incorrect';
		tooltipStyle1.display = 'none';
		return false;
	}
	else if (pwd1.value != pwd2.value)
	{
		pwd2.className = 'incorrect';
		tooltipStyle1.display = 'inline-block';
		return false;
	}
};

check['mail'] = function()
{
	var mail = document.getElementById('mail'), tooltip1, tooltipStyle1, reg = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/, result;
	tooltip1 = document.getElementById('email1');
	tooltipStyle1 = tooltip1.style;
	result = reg.test(mail.value);
	if (result && mail.value != '')
	{
		mail.className = 'correct';
		tooltipStyle1.display = 'none';
		return true;
	}
	if (mail.value == "")
	{
		mail.className = 'incorrect';
		tooltipStyle1.display = 'none';
		return false;
	}
	else if (!result)
	{
		mail.className = 'incorrect';
		tooltipStyle1.display = 'inline-block';
		return false;
	}
};

check['mailconf'] = function()
{
	var mail = document.getElementById('mail'), emailconf = document.getElementById('mailconf'), tooltip1, tooltipStyle1, tooltip2, tooltipStyle2;
	tooltip1 = document.getElementById('emailconf1');
	tooltipStyle1 = tooltip1.style;
	if (emailconf.value != '' && mail.value == emailconf.value)
	{
		emailconf.className = 'correct';
		tooltipStyle1.display = 'none';
		return true;
	}
	if (emailconf.value == "")
	{
		emailconf.className = 'incorrect';
		tooltipStyle1.display = 'none';
		return false;
	}
	else if (mail.value != emailconf.value)
	{
		emailconf.className = 'incorrect';
		tooltipStyle1.display = 'inline-block';
		return false;
	}
};

check['captcha'] = function()
{
	var captcha = document.getElementById('captcha');
	if (captcha.value != '' )
	{
		captcha.className = 'correct';
		return true;
	}
	else
	{
		captcha.className = 'incorrect';
		return false;
	}
};

(function()
{
	var myForm = document.getElementById('myForm'),
    inputs = document.getElementsByTagName('input'),
    inputsLength = inputs.length;

	if(typeof(myForm)=='object')
        return;

	for (var i = 0 ; i < inputsLength ; i++)
	{
		if (inputs[i].type == 'text' || inputs[i].type == 'password')
		{
			inputs[i].onblur = function()
			{
				check[this.id]();
			};
		}
	}

	myForm.onsubmit = function()
	{
		var result = true;

		for (var i = 0 ; i < inputsLength ; i++)
		{
			if (inputs[i].type == 'text' || inputs[i].type == 'password')
			{
				result = check[inputs[i].id]() && result;
			}
		}

		if (result)
		{
			return true;
		}
		return false;
	};

	myForm.onreset = function()
	{
		for (var i = 0 ; i < inputsLength ; i++)
		{
			if (inputs[i].type == 'text' || inputs[i].type == 'password')
			{
				inputs[i].className = 'none';
			}
		}
		deactivateTooltips();

		return true;
	};

})();

// Maintenant que tout est initialis�, on peut d�sactiver les "tooltips"
deactivateTooltips();