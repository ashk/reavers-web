function commentRequestByBugIdCallback(data, log)
{
	var text = "";

	$.each(data, function(index, element) {
		text = text + "<div class='newscontent'>";

		if (log)
		    text = text + "<div id='newspics'><h1><a href='profil-" + element['publisher'] + "'>" + element['publisherName'] + "</a></h1></div>";
		else
			text = text + "<div id='newspics'><h1>" + element['publisherName'] + "</h1></div>";

		text = text + "<div id='newstext'>" + element['content'] + "</div>";

		text = text + "<div id='newsdate'>Publi� le " + element['date'] + " � " +  element['heure'] + "</div>";

		text = text + "</div><br />";
	});
	$('#block-comment').html(text);
}

function commentRequestByBugId(bugId, ind, log)
{
    var addr = location.href.substring(0, location.href.indexOf('/', 8));
	var url = addr + '/ajaxRequest/bugcomments.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, bug: bugId, index: ind
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			commentRequestByBugIdCallback(data, log);
		}
	});
}
