function deactivateTooltips()
{
	var spans = document.getElementsByTagName('span'), spansLength = spans.length;
	for (var i = 0 ; i < spansLength ; i++)
	{
		if (spans[i].className == 'tooltip')
		{
			spans[i].style.display = 'none';
		}
	}
	return spans;
};

var check = []; // On met toutes nos fonctions dans un tableau associatif

check['name'] = function()
{
	var name = document.getElementById('name');
	if (name.value != '')
	{
		name.className = 'correct';
		return true;
	}
	if (name.value == "")
	{
		name.className = 'incorrect';
		return false;
	}
};

check['desc_offi'] = function()
{
	var name = document.getElementById('desc_offi');
	if (name.value != '')
	{
		name.className = 'correct';
		return true;
	}
	if (name.value == "")
	{
		name.className = 'incorrect';
		return false;
	}
};

check['desc_serv'] = function()
{
	var name = document.getElementById('desc_serv');
	if (name.value != '')
	{
		name.className = 'correct';
		return true;
	}
	if (name.value == "")
	{
		name.className = 'incorrect';
		return false;
	}
};

check['comment'] = function()
{
	var name = document.getElementById('comment');
	if (name.value != '')
	{
		name.className = 'correct';
		return true;
	}
	if (name.value == "")
	{
		name.className = 'incorrect';
		return false;
	}
};

(function()
{
	var myForm = document.getElementById('general'),
    inputs = document.getElementsByTagName('input'),
    inputsLength = inputs.length,
	textareas = document.getElementsByTagName('textarea'),
    textareasLength = textareas.length;

	if(typeof(myForm)=='object')
        return;

	for (var i = 0 ; i < inputsLength ; i++)
	{
		if (inputs[i].type == 'text' || inputs[i].type == 'password')
		{
			inputs[i].onblur = function()
			{
				check[this.id]();
			};
		}
	}

	for (var i = 0 ; i < textareasLength ; i++)
	{
		textareas[i].onblur = function()
		{
			check[this.id]();
		};
	}

	myForm.onsubmit = function()
	{
		var result = true;

		for (var i = 0 ; i < inputsLength ; i++)
		{
			if (inputs[i].type == 'text' || inputs[i].type == 'password')
			{
				result = check[inputs[i].id]() && result;
			}
		}

		for (var i = 0 ; i < textareasLength ; i++)
		{
			result = check[textareas[i].id]() && result;
		}

		if (result)
		{
			return true;
		}
		return false;
	};

	myForm.onreset = function()
	{
		for (var i = 0 ; i < inputsLength ; i++)
		{
			if (inputs[i].type == 'text' || inputs[i].type == 'password')
			{
				inputs[i].className = 'none';
			}
		}
		deactivateTooltips();

		return true;
	};
})();

// Maintenant que tout est initialisé, on peut désactiver les "tooltips"
deactivateTooltips();