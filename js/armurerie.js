function personnageRequestCallback(data)
{
	var text = "";

	$.each(data, function(index, element) {
        text = text + "<tr>";
		text = text + "<td><a href='personnage-" + element['guid'] + "'>" + element['name'] + "</a></td>";
		text = text + "<td align='center'><font color='darkred'>" + element['level'] + "</font></td>";
		text = text + "<td align='center'><img src='img/icons/race/" + element['race'] + "-" + element['gender'] + ".gif'/></td>";
		text = text + "<td align='center'><img src='img/icons/class/" + element['class'] + ".gif'/></td>";
		text = text + "<td align='center'>";
		if (element['online'] == 1)
			text = text + "<div style='color:#34C924;'>En ligne</div>";
		else
			text = text + "<div style='color:#ED0000;'>Hors ligne</div>";
		text = text + "</td>";
        text = text + "</tr>";
	});
	$('#tablepersos').after(text);
}

function personnageRequest(ind)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/personnage.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, index: ind
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			personnageRequestCallback(data);
		}
	});
}

function guildRequestCallback(data)
{
	var text = "";

	$.each(data, function(index, element) {
		text = text + "<td><a href='guild-" + element['guildid'] + "'>" + element['name'] + "</a></td>";
		text = text + "<td align='center'><a href='personnage-" + element['leaderguid'] + "'>" + element['leaderName'] + "</a></td>";
		text = text + "<td align='center'>" + element['date'] + "</td>";
	});
	$('#tableguilds').html(text);
}

function guildRequest(ind)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/guild.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, index: ind
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			guildRequestCallback(data);
		}
	});
}

function teamRequestCallback(data)
{
	var text = "";

	$.each(data, function(index, element) {
		text = text + "<td><a href='arene-" + element['arenaTeamId'] + "'>" + element['name'] + "</a></td>";
		text = text + "<td align='center'><a href='personnage-" + element['captainGuid'] + "'>" + element['captainName'] + "</a></td>";
		text = text + "<td align='center'>" + element['rating'] + "</td>";
	});
	$('#tableteams').html(text);
}

function teamRequest(ind, type)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/team.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, index: ind, type: type
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			teamRequestCallback(data);
		}
	});
}
