function newsRequestCallback(data, log)
{
	var text = "";

	$.each(data, function(index, element) {
		text = text + "<div class='newscontent'>";
		text = text + "<div id='newspics'><h1>" + element['title'] + "</h1></div><div id='newstext'>" + element['content'] + "</div>";
		if (log)
		{
			text = text + "<div id='newsdate'>Publi� par <a href='profil-" + element['publisher'] + "'>" + element['publisherName'] + "</a> le " + element['date'] + " � " +  element['heure'] + "</div>";
			text = text + "<div id='newscom'><a href='news-" + element['id'] + "'>Voir les commentaires(" + element['comment'] + ")</a></div>";
		}
		else
		{
			text = text + "<div id='newsdate'>Publi� par " + element['publisherName'] + " le " + element['date'] + " � " +  element['heure'] + "</div>";
			text = text + "<div id='newscom'>Impossible de voir les commentaires!</div>";
		}
		text = text + "</div><br />";
	});
	$('#allNews').html(text);
}

function newsRequest(ind, log)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/news.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, index: ind
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			newsRequestCallback(data, log);
		}
	});
}

function newsRequestByIdCallback(data)
{
	var text = "";

	text = "<div id='newspics'><div id='title'><h1>" + data['title'] + "</h1></div></div><div id='newstext'>" + data['content'] + "</div>";
	text = text + "<div id='commentdate'>Publi� par <a href='profil-" + data['publisher'] + "'>" + data['publisherName'] + "</a> le " + data['date'] + " � " + data['heure'] + "</div>";

	$('.block-modify').html(text);
}

function newsRequestById(id)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/news.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 2, newsId: id
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			newsRequestByIdCallback(data);
		}
	});
}

function commentRequestByNewsIdCallback(data, log)
{
	var text = "";

	$.each(data, function(index, element) {
		text = text + "<div class='newscontent'>";

		if (log)
		    text = text + "<div id='newspics'><h1><a href='profil-" + element['publisher'] + "'>" + element['publisherName'] + "</a></h1></div>";
		else
			text = text + "<div id='newspics'><h1>" + element['publisherName'] + "</h1></div>";

		text = text + "<div id='newstext'>" + element['content'] + "</div>";

		text = text + "<div id='newsdate'>Publi� le " + element['date'] + " � " +  element['heure'] + "</div>";

		text = text + "</div><br />";
	});
	$('#block-comment').html(text);
}

function commentRequestByNewsId(newsId, ind, log)
{
    var addr = location.href.substring(0, location.href.indexOf('/', 8));
	var url = addr + '/ajaxRequest/comments.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, news: newsId, index: ind
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			commentRequestByNewsIdCallback(data, log);
		}
	});
}
