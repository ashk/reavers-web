function readData(data)
{
    var total = document.getElementById("total");
	var pourcent = document.getElementById("pourcent");

    if (data[0] == -1)
	{
        total.innerHTML = "<div id='progress'><div class='rempli_off'><div id='textprogressoff'>Hors ligne</div></div></div>";
    }
	else
	{
		var str = "<div id='progress'><div class='rempli_on'><div id='textprogresson'><a href='online'>" + data[0];
		if (data[0] <= 1)
			str += " joueur connect�";
		else
			str += " joueurs connect�s";
		str += "</a></div></div></div>";

        total.innerHTML = str;
    }
	pourcent.innerHTML = data[1] + " - " + data[2];
}

function Refresh ()
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/compteur.php';
	$.ajax({
		url: url,
		type: 'GET',
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			readData(data);
		}
	});
}

$(function()
{
	Refresh();
	setInterval("Refresh()", 10000);
});