function deactivateTooltips()
{
	var spans = document.getElementsByTagName('span'), spansLength = spans.length;
	for (var i = 0 ; i < spansLength ; i++)
	{
		if (spans[i].className == 'tooltip')
		{
			spans[i].style.display = 'none';
		}
	}
	return spans;
};

var check = []; // On met toutes nos fonctions dans un tableau associatif

check['mail'] = function()
{
	var mail = document.getElementById('mail'), tooltip1, tooltipStyle1, reg = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/, result;
	tooltip1 = document.getElementById('email1');
	tooltipStyle1 = tooltip1.style;
	result = reg.test(mail.value);
	if (result && mail.value != '')
	{
		mail.className = 'correct';
		tooltipStyle1.display = 'none';
		return true;
	}
	if (mail.value == "")
	{
		mail.className = 'incorrect';
		tooltipStyle1.display = 'none';
		return false;
	}
	else if (!result)
	{
		mail.className = 'incorrect';
		tooltipStyle1.display = 'inline-block';
		return false;
	}
};

(function()
{
	var myForm = document.getElementById('passperdu'),
    inputs = document.getElementsByTagName('input'),
    inputsLength = inputs.length;

	for (var i = 0 ; i < inputsLength ; i++)
	{
		if (inputs[i].type == 'text' || inputs[i].type == 'password')
		{
			inputs[i].onblur = function()
			{
				check[this.id]();
			};
		}
	}

	myForm.onsubmit = function()
	{
		var result = true;

		for (var i = 0 ; i < inputsLength ; i++)
		{
			if (inputs[i].type == 'text' || inputs[i].type == 'password')
			{
				result = check[inputs[i].id]() && result;
			}
		}

		if (result)
		{
			return true;
		}
		return false;
	};

	myForm.onreset = function()
	{
		for (var i = 0 ; i < inputsLength ; i++)
		{
			if (inputs[i].type == 'text' || inputs[i].type == 'password')
			{
				inputs[i].className = 'none';
			}
		}
		deactivateTooltips();

		return true;
	};

})();


// Maintenant que tout est initialisé, on peut désactiver les "tooltips"

deactivateTooltips();