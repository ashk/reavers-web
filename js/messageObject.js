function objectSendRequestCallback(data)
{
	var text = "";

	$.each(data, function(index, element) {

	if (element['lus'] == 0)
		text = text + "<td align='center'><a href='privatesend-" + element['id'] + "'><b>" + element['objet'] + "</b></a></td>";
	else if (element['lus'] == 1)
		text = text + "<td align='center'><a href='privatesend-" + element['id'] + "'>" + element['objet'] + "</a></td>";

	text = text + "<td align='center' width='100px'>" + element['date'] + "</td>";
	text = text + "<td align='center'><a href='profil-" + element['receiver'] + "'>" + element['receiverName'] + "</a></td>";
	});
	$('#tablesend').html(text);
}

function objectSendRequest(idUser, ind)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/messageObject.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, index: ind, user: idUser
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			objectSendRequestCallback(data);
		}
	});
}

function objectReceiveRequestCallback(data)
{
	var text = "";

	$.each(data, function(index, element) {

	if (element['lus'] == 0)
		text = text + "<td align='center'><a href='privatesend-" + element['id'] + "'><b>" + element['objet'] + "</b></a></td>";
	else if (element['lus'] == 1)
		text = text + "<td align='center'><a href='privatesend-" + element['id'] + "'>" + element['objet'] + "</a></td>";

	text = text + "<td align='center' width='100px'>" + element['date'] + "</td>";
	text = text + "<td align='center'><a href='profil-" + element['sender'] + "'>" + element['senderName'] + "</a></td>";
	});
	$('#tablereceive').html(text);
}

function objectReceiveRequest(idUser, ind)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/messageObject.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 2, index: ind, user: idUser
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			objectReceiveRequestCallback(data);
		}
	});
}
