function onlineRequestCallback(data)
{
	var text = "";

	$.each(data, function(index, element) {
		text = text + "<td><a href='personnage-" + element['guid'] + "'>" + element['name'] + "</a></td>";
		text = text + "<td align='center'><font color='darkred'>" + element['level'] + "</font></td>";
		text = text + "<td align='center'><img src='img/icons/race/" + element['race'] + "-" + element['gender'] + ".gif'/></td>";
		text = text + "<td align='center'><img src='img/icons/class/" + element['class'] + ".gif'/></td>";
		text = text + "<td align='right'>" + element['zone_categorie'] + " - " + element['zone_zone'] + "</td>";
	});
	$('#tableonline').html(text);
}

function onlineRequest(ind)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/online.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, index: ind
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			onlineRequestCallback(data);
		}
	});
}
