function bugsRequestCallback(data)
{
	var text = "";

	$.each(data, function(index, element) {
	text = text + "<td width='100px' align='center'>" + element['firstdate'] + "</td>";
	text = text + "<td align='center'><a href='bug-" + element['id'] + "'>" + element['title'] + "</a></td>";
	text = text + "<td width='100px' align='center'>" + element['priority'] + "</td>";
	text = text + "<td width='100px' align='center'>" + element['state'] + "</td>";
	});
	$('#tablebugs').html(text);
}

function bugsRequest(categorie)
{
	var addr = location.href.substring(0, location.href.indexOf('/', 8));
    var url = addr + '/ajaxRequest/bugtracker.php';

	var status = $("select[name='status']").val();
	var priority = $("select[name='priority']").val();

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, cat: categorie, stat: status, prio: priority
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			bugsRequestCallback(data);
		}
	});
}
