function messageRequestCallback(data, log)
{
	var text = "<br />";

	$.each(data, function(index, element) {
		text = text + "<div class='newscontent4'>";

		text = text + "<div id='newstext'>" + element['content'] + "</div>";

		text = text + "<div id='newsdate'>Publi� le " + element['date'] + " � " +  element['heure'] + " par <a href='profil-" + element['sender'] + "'>" + element['senderName'] + "</a></div>";

		text = text + "</div><br />";
	});
	$('#private').html(text);
}

function messageRequest(idObject, ind)
{
    var addr = location.href.substring(0, location.href.indexOf('/', 8));
	var url = addr + '/ajaxRequest/messages.php';

	$.ajax({
		url: url,
		type: 'POST',
		data: {
		    action: 1, idobject: idObject, index: ind
		},
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			messageRequestCallback(data);
		}
	});
}
