<?php

require_once "../content/configuration.php";
require_once "../content/classes/class.mysql.php";
require_once "../content/classes/class.site.php";

$sql = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_site['db_site']);
$site = new site($sql, $array_site['nom'], $array_site['url']);

$site->isXMLHttpRequest();

if (!empty($_POST['action']))
{
	switch ($_POST['action'])
	{
		case 1:
			if (!empty($_POST['index']) && !empty($_POST['idobject']))
			{
			    $idObject = $_POST['idobject'];

				$index = $_POST['index'];
				$index = $index - 1;

				$messages = $site->loadMessagesWithLimit($idObject, ($index * 10), 10);

				exit(json_encode($messages));
			}
			break;
	}
}
?>
