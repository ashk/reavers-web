<?php

require_once "../content/configuration.php";
require_once "../content/classes/class.mysql.php";
require_once "../content/classes/class.site.php";

$sql = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_site['db_site']);
$site = new site($sql, $array_site['nom'], $array_site['url']);

$site->isXMLHttpRequest();

if (!empty($_POST['action']))
{
	switch ($_POST['action'])
	{
		case 1:
			if (!empty($_POST['index']))
			{
				$index = $_POST['index'];
				$index = $index - 1;

				$news = $site->loadNewsWithLimit(($index * 3), 3);

				exit(json_encode($news));
			}
			break;
		case 2:
			if (!empty($_POST['newsId']))
			{
				$newsId = $_POST['newsId'];
				$news = $site->getNewsById($newsId);

				exit(json_encode($news));
			}
			break;
	}
}
?>
