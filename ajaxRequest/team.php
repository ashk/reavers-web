<?php

require_once "../content/configuration.php";
require_once "../content/classes/class.mysql.php";
require_once "../content/classes/class.site.php";
require_once "../content/classes/class.royaume.php";

$sql = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_site['db_site']);
$site = new site($sql, $array_site['nom'], $array_site['url']);

$dbauth = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_auth']);
$dbcharacters = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_characters']);
$dbworld = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_world']);
$royaume = new royaume($dbauth, $dbcharacters, $dbworld);

$site->isXMLHttpRequest();

if (!empty($_POST['action']))
{
	switch ($_POST['action'])
	{
		case 1:
			if (!empty($_POST['index']) && !empty($_POST['type']))
			{
				$index = $_POST['index'];
				$index = $index - 1;

				$type = $_POST['type'];
				$teamsArray = $royaume->loadTeamsWithLimit($type, ($index * 20), 20);

				exit(json_encode($teamsArray));
			}
			break;
	}
}
?>
