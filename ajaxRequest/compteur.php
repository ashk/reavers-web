<?php

require_once "../content/configuration.php";
require_once "../content/classes/class.mysql.php";
require_once "../content/classes/class.royaume.php";
require_once "../content/classes/class.site.php";

$dbauth = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_auth']);
$dbcharacters = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_characters']);
$dbworld = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_royaume['db_world']);
$sql = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_site['db_site']);
$site = new site($sql, $array_site['nom'], $array_site['url']);
$royaume = new royaume($dbauth, $dbcharacters, $dbworld);

$site->isXMLHttpRequest();

if(!$sock = @fsockopen($royaume->getAdress(), $royaume->getPort(), $num, $error, 1))
{
	$response[0] = '-1';
	$response[1] = '0%';
	$response[2] = '0%';

	exit(json_encode($response));
}
else
{
	$ally = 0;
	$horde = 0;
	$Conn = $royaume->getonline(1);
	$ally = count($Conn);

	$Conn = $royaume->getonline(2);
	$horde = count($Conn);

	$total = $ally + $horde;

	$allypourcent = '0%';
	$hordepourcent = '0%';
	if (!empty($ally))
	{
		$allypourcent = ($ally*100)/$total;
		$allypourcent = round($allypourcent, 0, PHP_ROUND_HALF_UP);
		$allypourcent = $allypourcent.'%';
	}

	if(!empty($horde))
	{
		$hordepourcent = ($horde*100)/$total;
		$hordepourcent = round($hordepourcent, 0, PHP_ROUND_HALF_UP);
		$hordepourcent = $hordepourcent.'%';
	}

	$response[0] = $total;
	$response[1] = $allypourcent;
	$response[2] = $hordepourcent;

	exit(json_encode($response));
}
?>