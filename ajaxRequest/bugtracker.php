<?php

require_once "../content/configuration.php";
require_once "../content/classes/class.mysql.php";
require_once "../content/classes/class.site.php";

$sql = new DatabaseHandler($array_db['host'], $array_db['user'], $array_db['pass'], $array_site['db_site']);
$site = new site($sql, $array_site['nom'], $array_site['url']);

$site->isXMLHttpRequest();

if (!empty($_POST['action']))
{
	switch ($_POST['action'])
	{
		case 1:
			if (!empty($_POST['cat']) && isset($_POST['stat']) && isset($_POST['prio']))
			{
				$categorie = $_POST['cat'];
				$status = $_POST['stat'];
				$priority = $_POST['prio'];

				$bugs = $site->loadbugsWithLimit($categorie, $status, $priority);

				exit(json_encode($bugs));
			}
			break;
	}
}
?>
